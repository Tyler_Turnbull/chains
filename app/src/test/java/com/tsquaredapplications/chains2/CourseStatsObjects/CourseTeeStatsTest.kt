package com.tsquaredapplications.chains2.CourseStatsObjects


import com.tsquaredapplications.chains2.Objects.Hole
import com.tsquaredapplications.chains2.Objects.Scorecard
import com.tsquaredapplications.chains2.PlayerStatsObjects.HoleStats
import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerTeeStats
import org.junit.Before

import org.junit.Assert.*
import org.junit.Test


class CourseTeeStatsTest {

//    lateinit var actualMergeStats: CourseTeeStats
//    lateinit var actualFirstTimeStats: CourseTeeStats
//
//    val inputStats = CourseTeeStats(1, 9, 0, 9, mutableListOf(LeaderboardEntry(9, "123", "Test User")),
//            mutableListOf(LeaderboardEntryDouble(9.0, "123", "Test User")), mutableListOf(LeaderboardEntry(-1, "123", "Test User")),
//            mutableListOf(LeaderboardEntry(0, "123", "Test User")), mutableListOf(LeaderboardEntry(0, "123", "Test User")),
//            mutableListOf(LeaderboardEntry(2, "123", "Test User")), mutableListOf(LeaderboardEntry(1, "123", "Test User")),
//            9, 0, 0, 0, 1, 1, 1, 0, 0,
//            mutableListOf(HoleStats(3, 0, 3.0, 3, 0, 0, 0, 0, 1, 0, 0, 0),
//                    HoleStats(4, 1, 4.0, 4, 0 ,0 ,0 , 0, 0, 1, 0, 0),
//                    HoleStats(2, -1, 2.0, 2, 0, 0, 0, 1, 0, 0, 0, 0))
//            )
//
//    val inputScorecard = Scorecard(arrayListOf(3,2,3), arrayListOf(Hole(0, 3, 150), Hole(1, 3, 150), Hole(2, 3, 150)),
//            0, 2, 1, 1, "Test Course", "Red")
//
//    val expectedMergeResults = CourseTeeStats(2, 17, 0, 8, mutableListOf(LeaderboardEntry(8, "123", "Test User")),
//            mutableListOf(LeaderboardEntryDouble(8.5, "123", "Test User")), mutableListOf(LeaderboardEntry(-1, "123", "Test User")),
//            mutableListOf(LeaderboardEntry(0, "123", "Test User")), mutableListOf(LeaderboardEntry(0, "123", "Test User")),
//            mutableListOf(LeaderboardEntry(3, "123", "Test User")), mutableListOf(LeaderboardEntry(2, "123", "Test User")),
//            9, 0, 0, 0, 2, 3, 1, 0, 0,
//            mutableListOf(HoleStats(6, 0, 3.0, 3, 0, 0, 0, 0, 2, 0, 0, 0),
//                    HoleStats(6, 0, 2.0, 4, 0 ,0 ,0 , 1, 0, 1, 0, 0),
//                    HoleStats(5, -1, 2.0, 3, 0, 0, 0, 1, 1, 0, 0, 0))
//    )
//
//    val expectedFirstTimeResults = CourseTeeStats(1, 8, -1, 8,
//            mutableListOf(LeaderboardEntry(8, "123", "Test User")), mutableListOf(LeaderboardEntryDouble(8.0, "123", "Test User")),
//            mutableListOf(LeaderboardEntry(-1, "123", "Test User")), mutableListOf(LeaderboardEntry(0, "123", "Test User")),
//            mutableListOf(LeaderboardEntry(0, "123", "Test User")), mutableListOf(LeaderboardEntry(1, "123", "Test User")),
//            mutableListOf(LeaderboardEntry(1, "123", "Test User")),8, 0,
//            0,0,1,2,0,0,0, mutableListOf())
//
//    val username = "Test User"
//
//
//
//    @Before
//    fun setUp() {
//
//        actualFirstTimeStats = CourseTeeStats.firstTime(inputScorecard, username, "123")
//        actualMergeStats = CourseTeeStats.mergeStats(inputScorecard, inputStats, PlayerTeeStats.mergeStats(inputScorecard, actualFirstTimeStats, username), "123")
//    }
//
//
//    @Test
//    fun `first time sets rounds played as expected`(){
//        assertEquals(expectedFirstTimeResults.roundsPlayed, expectedFirstTimeResults.roundsPlayed)
//    }
//
//    @Test
//    fun `first time sets total strokes as expected`(){
//        assertEquals(expectedFirstTimeResults.totalStrokes, expectedFirstTimeResults.totalStrokes)
//    }
//
//    @Test
//    fun `first time sets lifetime plus minus as expected`(){
//        assertEquals(expectedFirstTimeResults.lifetimePlusMinus, expectedFirstTimeResults.lifetimePlusMinus)
//    }
//
//    @Test
//    fun `first time sets best score as expected`(){
//        assertEquals(expectedFirstTimeResults.bestScore, expectedFirstTimeResults.bestScore)
//    }
//
//    @Test
//    fun `first time sets worst score as expected`(){
//        assertEquals(expectedFirstTimeResults.worstScore, expectedFirstTimeResults.worstScore)
//    }
//
//    @Test
//    fun `first time sets num birdies as expected`(){
//        assertEquals(expectedFirstTimeResults.numBirdies, expectedFirstTimeResults.numBirdies)
//    }
//
//    @Test
//    fun `first time sets num pars as expected`(){
//        assertEquals(expectedFirstTimeResults.numPars, expectedFirstTimeResults.numPars)
//    }
//
//    @Test
//    fun `first time sets num bogeys as expected`(){
//        assertEquals(expectedFirstTimeResults.numBogeys, expectedFirstTimeResults.numBogeys)
//    }
//
//    @Test
//    fun `merge updates rounds played as expected`(){
//        assertEquals(expectedMergeResults.roundsPlayed, actualMergeStats.roundsPlayed)
//    }
//
//    @Test
//    fun `merge updates total strokes as expected`(){
//        assertEquals(expectedMergeResults.totalStrokes, actualMergeStats.totalStrokes)
//    }
//
//    @Test
//    fun `merge updates lifetime plus minus as expected`(){
//        assertEquals(expectedMergeResults.lifetimePlusMinus, actualMergeStats.lifetimePlusMinus)
//    }
//
//    @Test
//    fun `merge updates best score as expected`(){
//        assertEquals(expectedMergeResults.bestScore, actualMergeStats.bestScore)
//    }
//
//    @Test
//    fun `merge updates worst score as expected`(){
//        assertEquals(expectedMergeResults.worstScore, actualMergeStats.worstScore)
//    }
//
//    @Test
//    fun `merge updates num birdies as expected`(){
//        assertEquals(expectedMergeResults.numBirdies, actualMergeStats.numBirdies)
//    }
//
//    @Test
//    fun `merge updates num pars as expected`(){
//        assertEquals(expectedMergeResults.numPars, actualMergeStats.numPars)
//    }
//
//    @Test
//    fun `merge updates num bogeys as expected`(){
//        assertEquals(expectedMergeResults.numBogeys, actualMergeStats.numBogeys)
//    }
}