package com.tsquaredapplications.chains2.CourseStatsObjects

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class LeaderboardEntryTest {

    lateinit var actualFirstTimeLeaderboard: MutableList<LeaderboardEntry>
    lateinit var actualUpdatedLeaderboardSameUser: MutableList<LeaderboardEntry>
    lateinit var actualUpdatedLeaderboardNewUser: MutableList<LeaderboardEntry>
    lateinit var actualAfter11thEntry: MutableList<LeaderboardEntry>



    val entryAdam1 = LeaderboardEntry(50, "1", "Adam")
    val entryAdam2 = LeaderboardEntry(40, "1", "Adam")

    val entryBob1 = LeaderboardEntry(51, "22", "Bob")

    val expectedFirstTimeLeaderboard = mutableListOf(
            entryAdam1
    )

    val expectedLeaderboardSameUser = mutableListOf(
            entryAdam2
    )

    val expectedLeaderboardNewUser = mutableListOf(
           entryAdam2,
            entryBob1
    )

    // Test for over 10 limit
    val size10Leaderboard = mutableListOf(
            LeaderboardEntry(50, "1", "Andy"),
            LeaderboardEntry(52, "2", "bruce"),
            LeaderboardEntry(54, "3", "Chris"),
            LeaderboardEntry(56, "4", "Dave"),
            LeaderboardEntry(58, "5", "Eve"),
            LeaderboardEntry(60, "6", "Frank"),
            LeaderboardEntry(62, "7", "Gale"),
            LeaderboardEntry(64, "8", "Harley"),
            LeaderboardEntry(66, "9", "Ida"),
            LeaderboardEntry(68, "10", "Jasmine")
    )

    val expectedAfter11thEntry = mutableListOf(
            LeaderboardEntry(50, "1", "Andy"),
            LeaderboardEntry(51, "22", "Bob"),
            LeaderboardEntry(52, "2", "bruce"),
            LeaderboardEntry(54, "3", "Chris"),
            LeaderboardEntry(56, "4", "Dave"),
            LeaderboardEntry(58, "5", "Eve"),
            LeaderboardEntry(60, "6", "Frank"),
            LeaderboardEntry(62, "7", "Gale"),
            LeaderboardEntry(64, "8", "Harley"),
            LeaderboardEntry(66, "9", "Ida")

    )


    @Before
    fun setUp() {

        actualFirstTimeLeaderboard = LeaderboardEntry.buildDescendingLeaderboard(null, entryAdam1)
        actualUpdatedLeaderboardSameUser = LeaderboardEntry.buildDescendingLeaderboard(actualFirstTimeLeaderboard, entryAdam2)
        actualUpdatedLeaderboardNewUser = LeaderboardEntry.buildDescendingLeaderboard(actualUpdatedLeaderboardSameUser, entryBob1)
        actualAfter11thEntry = LeaderboardEntry.buildDescendingLeaderboard(size10Leaderboard, entryBob1)
    }

    @Test
    fun `first time leaderboard created as expected`() {
        assertEquals(expectedFirstTimeLeaderboard, actualFirstTimeLeaderboard)
    }

    @Test
    fun `updated leaderboard with same user created as expected`(){
        assertEquals(expectedLeaderboardSameUser, actualUpdatedLeaderboardSameUser)
    }

    @Test
    fun `updated leaderboard with new user created as expected`(){
        assertEquals(expectedLeaderboardNewUser, actualUpdatedLeaderboardNewUser)
    }

    @Test
    fun `updated leaderboard with 11th score created as expected`(){
        assertEquals(expectedAfter11thEntry, actualAfter11thEntry)
    }
}