package com.tsquaredapplications.chains2.CourseStatsObjects

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class LeaderboardEntryDoubleTest {

    lateinit var actualFirstTimeLeaderboard: MutableList<LeaderboardEntryDouble>
    lateinit var actualUpdatedLeaderboardSameUser: MutableList<LeaderboardEntryDouble>
    lateinit var actualUpdatedLeaderboardNewUser: MutableList<LeaderboardEntryDouble>
    lateinit var actualAfter11thEntry: MutableList<LeaderboardEntryDouble>


    val entryAdam1 = LeaderboardEntryDouble(50.3, "1", "Adam")
    val entryAdam2 = LeaderboardEntryDouble(40.1, "1", "Adam")

    val entryBob1 = LeaderboardEntryDouble(51.1, "22", "Bob")

    val expectedFirstTimeLeaderboard = mutableListOf(
            entryAdam1
    )

    val expectedLeaderboardSameUser = mutableListOf(
            entryAdam2
    )

    val expectedLeaderboardNewUser = mutableListOf(
            entryAdam2,
            entryBob1
    )

    // Test for over 10 limit
    val size10Leaderboard = mutableListOf(
            LeaderboardEntryDouble(50.3, "1", "Andy"),
            LeaderboardEntryDouble(52.0, "2", "bruce"),
            LeaderboardEntryDouble(54.9, "3", "Chris"),
            LeaderboardEntryDouble(56.6, "4", "Dave"),
            LeaderboardEntryDouble(58.5, "5", "Eve"),
            LeaderboardEntryDouble(60.5, "6", "Frank"),
            LeaderboardEntryDouble(60.6, "7", "Gale"),
            LeaderboardEntryDouble(64.5, "8", "Harley"),
            LeaderboardEntryDouble(64.57, "9", "Ida"),
            LeaderboardEntryDouble(68.8, "10", "Jasmine")
    )

    val expectedAfter11thEntry = mutableListOf(
            LeaderboardEntryDouble(50.3, "1", "Andy"),
            LeaderboardEntryDouble(51.1, "22", "Bob"),
            LeaderboardEntryDouble(52.0, "2", "bruce"),
            LeaderboardEntryDouble(54.9, "3", "Chris"),
            LeaderboardEntryDouble(56.6, "4", "Dave"),
            LeaderboardEntryDouble(58.5, "5", "Eve"),
            LeaderboardEntryDouble(60.5, "6", "Frank"),
            LeaderboardEntryDouble(60.6, "7", "Gale"),
            LeaderboardEntryDouble(64.5, "8", "Harley"),
            LeaderboardEntryDouble(64.57, "9", "Ida")

    )


    @Before
    fun setUp() {

        actualFirstTimeLeaderboard = LeaderboardEntryDouble.buildDescendingLeaderboard(null, entryAdam1)
        actualUpdatedLeaderboardSameUser = LeaderboardEntryDouble.buildDescendingLeaderboard(actualFirstTimeLeaderboard, entryAdam2)
        actualUpdatedLeaderboardNewUser = LeaderboardEntryDouble.buildDescendingLeaderboard(actualUpdatedLeaderboardSameUser, entryBob1)
        actualAfter11thEntry = LeaderboardEntryDouble.buildDescendingLeaderboard(size10Leaderboard, entryBob1)
    }

    @Test
    fun `first time leaderboard created as expected`() {
        assertEquals(expectedFirstTimeLeaderboard, actualFirstTimeLeaderboard)
    }

    @Test
    fun `updated leaderboard with same user created as expected`() {
        assertEquals(expectedLeaderboardSameUser, actualUpdatedLeaderboardSameUser)
    }

    @Test
    fun `updated leaderboard with new user created as expected`() {
        assertEquals(expectedLeaderboardNewUser, actualUpdatedLeaderboardNewUser)
    }

    @Test
    fun `updated leaderboard with 11th score created as expected`() {
        assertEquals(expectedAfter11thEntry, actualAfter11thEntry)
    }
}