package com.tsquaredapplications.chains2.PlayerStatsObjects

import com.tsquaredapplications.chains2.Objects.Hole
import org.junit.Before

import org.junit.Assert.*
import org.junit.Test

class HoleStatsTest {

    // stuff for first time test
    val ftAce = HoleStats.firstTime(1, 3)
    val ftAlbatross = HoleStats.firstTime(2,5)
    val ftEagle = HoleStats.firstTime(3,5)
    val ftBirdie = HoleStats.firstTime(4,5)
    val ftPar = HoleStats.firstTime(5,5)
    val ftBogey = HoleStats.firstTime(6,5)
    val ftDBogey = HoleStats.firstTime(7,5)
    val ftTBogey = HoleStats.firstTime(8,5)
    val ftGTTBogey = HoleStats.firstTime(10,5)

    val mergeAce = HoleStats.mergeStats(ftAce, 1, 3)
    val mergeEagle = HoleStats.mergeStats(ftEagle, 3, 5)
    val mergeBirdie = HoleStats.mergeStats(ftBirdie, 4, 5)
    val mergePar = HoleStats.mergeStats(ftPar, 5,5)
    val mergeBogey = HoleStats.mergeStats(ftBogey, 6,5)

    val newBestScore = HoleStats.mergeStats(ftPar, 3, 5)
    val newWorstScore = HoleStats.mergeStats(newBestScore, 7, 5)


    @Test
    fun `merge stats updates worst strokes as expected`(){
        assertEquals(7, newWorstScore.worstScore)
    }

    @Test
    fun `merge stats updates best score as expected`(){
        assertEquals(3, newBestScore.bestScore)
    }

    @Test
    fun `merge stats updates total strokes as expected`(){
        assertEquals(2, mergeAce.totalStrokes)
    }

    @Test
    fun `merge stats updates total strokes as expected 2`(){
        assertEquals(6, mergeEagle.totalStrokes)
    }

    @Test
    fun `merge stats with ace created as expected`(){
        assertEquals(2, mergeAce.numAces)
    }

    @Test
    fun `merge stats with eagles created as expected`(){
        assertEquals(2, mergeEagle.numEagles)
    }

    @Test
    fun `merge stats with birdie created as expected`(){
        assertEquals(2, mergeBirdie.numBirdies)
    }

    @Test
    fun `merge stats with par created as expected`(){
        assertEquals(2, mergePar.numPars)
    }

    @Test
    fun `merge stats with bogey created as expected`(){
        assertEquals(2, mergeBogey.numBogeys)
    }

    @Test
    fun `first time with ace created as expected`(){
        assertEquals(1, ftAce.numAces)
    }

    @Test
    fun `first time with albatross created as expected`(){
        assertEquals(1, ftAlbatross.numAlbatross)
    }

    @Test
    fun `first time with eagles created as expected`(){
        assertEquals(1, ftEagle.numEagles)
    }

    @Test
    fun `first time with birdie created as expected`(){
        assertEquals(1, ftBirdie.numBirdies)
    }

    @Test
    fun `first time with par created as expected`(){
        assertEquals(1, ftPar.numPars)
    }

    @Test
    fun `first time with bogey created as expected`(){
        assertEquals(1, ftBogey.numBogeys)
    }

    @Test
    fun `first time with double bogey created as expected`(){
        assertEquals(1, ftDBogey.numDoubleBogeys)
    }


    @Test
    fun `first time with triple bogey created as expected`(){
        assertEquals(1, ftTBogey.numTripleBogeysPlus)
    }

    @Test
    fun `first time with greater than triple bogey created as expected`(){
        assertEquals(1, ftGTTBogey.numTripleBogeysPlus)
    }

    @Test
    fun `first time with ace created with no pars as expected`(){
        assertEquals(0, ftAce.numPars)
    }
}