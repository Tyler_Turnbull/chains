package com.tsquaredapplications.chains2.PlayerStatsObjects

import com.tsquaredapplications.chains2.Objects.Hole
import com.tsquaredapplications.chains2.Objects.Scorecard
import com.tsquaredapplications.chains2.Utilities.TrendUtil
import org.junit.Assert.*
import org.junit.Test

class PlayerStatsTest {
    val uid = "123"

    val scorecard = Scorecard(arrayListOf(3, 3, 3, 4, 5), // 18 strokes
            mutableListOf(Hole(0, 3, 100), // par
                    Hole(1, 4, 200), // birdie
                    Hole(2, 5, 300), // eagle
                    Hole(3, 4, 300), // par
                    Hole(4, 3, 199)), // dbl bogey
            -1, 4, 1, 1331,
            "TestCourse", "")

    val scorecard2 = Scorecard(arrayListOf(4, 4, 2, 4, 5), // 19 strokes
            mutableListOf(Hole(0, 3, 100), // bogey
                    Hole(1, 4, 200), // par
                    Hole(2, 5, 300), // albatross
                    Hole(3, 4, 300), // par
                    Hole(4, 3, 199)), // dbl bogey
            +1, 4, 1, 1331,
            "TestCourse", "")

    val scorecard3 = Scorecard(arrayListOf(6, 5, 4, 8, 5), // 28 strokes
            mutableListOf(Hole(0, 3, 100), // tpl bogey
                    Hole(1, 4, 200), // bogey
                    Hole(2, 5, 300), // birdie
                    Hole(3, 4, 300), // tpl bogey + 1
                    Hole(4, 3, 199)), // dbl bogey
            +8, 4, 1, 1331,
            "TestCourse", "")

    val expectedFirstTime = PlayerStats("123", 18, 1, -1,
            0, 0, 1, 1, 2, 0, 1,
            0, arrayListOf())

    val actualFirstTime = PlayerStats.firstTime(scorecard, uid)

    val expectedMergeSecondRound = PlayerStats("123", 37, 2,
            0,1,0, 1,
            1, 4, 1, 2, 0,
            arrayListOf())

    val actualMergeSecondTime = PlayerStats.mergeStats(scorecard2, actualFirstTime, 10)

   val fourthTime = PlayerStats("123", 75, 4, 2,
           3, 0, 1, 1,
           8,3,4,0,
           arrayListOf())

    val expectedMergeFifthTime = PlayerStats("123", 94, 5,
            3, 4, 0, 1,
            1,10,4,5, 0,
            arrayListOf(0))

    val actualMergeFifthTime = PlayerStats.mergeStats(scorecard2, fourthTime,
            TrendUtil.calculateTrendValue(scorecard2.getTotalStrokes(),
                    fourthTime.getAvgStrokes()))

    val fifthTime = PlayerStats("123", 94, 5,
            3, 4, 0, 1,
            1,10,4,5, 0,
            arrayListOf(0))

    val expectedMergeSixthTime = PlayerStats("123", 122,6,
            11, 4,0,1,
            2,10,5,6,2,
            arrayListOf(0,1))

    val actualMergeSixthTime = PlayerStats.mergeStats(scorecard3, fifthTime,
           TrendUtil.calculateTrendValue(scorecard3.getTotalStrokes(),
                   fifthTime.getAvgStrokes()))

    val sixthTime = PlayerStats("123", 122,6,
            11, 4,0,1,
            2,10,5,6,2,
            arrayListOf(0,1))

    val expectedMergeSeventhTime = PlayerStats("123", 140, 7,
            10,4,0,2,3,
            12,5,7,2,
            arrayListOf(0,1,-1))

    val actualMergeSeventhTime = PlayerStats.mergeStats(scorecard, sixthTime,
            TrendUtil.calculateTrendValue(scorecard.getTotalStrokes(),
                    sixthTime.getAvgStrokes()))

    @Test
    fun `first time computes as expected`() {
        assertEquals(expectedFirstTime, actualFirstTime)
    }

    @Test
    fun `merge second round computes as expected`() {
        assertEquals(expectedMergeSecondRound, actualMergeSecondTime)
    }

    @Test
    fun `merge fifth round computes as expected`(){
        assertEquals(expectedMergeFifthTime, actualMergeFifthTime)
    }

    @Test
    fun `merge sixth round computes as expected`(){
        assertEquals(expectedMergeSixthTime, actualMergeSixthTime)
    }

    @Test
    fun `merge seventh round comutes as expected`(){
        assertEquals(expectedMergeSeventhTime, actualMergeSeventhTime)
    }


}