package com.tsquaredapplications.chains2.Utilities

import org.junit.Assert.*
import org.junit.Test



class GeneralUtilsTest {

    @Test
    fun `rounds double to string with 5_12345`() {
       runTestString(5.12345, "5.12")
    }

    @Test
    fun `rounds double to string with 13_496`(){
        runTestString(13.496, "13.50")
    }

    @Test
    fun `rounds double to string with 100_281`(){
        runTestString(100.281, "100.28")
    }

    @Test
    fun `rounds double to string with 199_999`(){
        runTestString(199.999, "200.00")
    }

    @Test
    fun `rounds double to double with 5_12345`(){
        runTestDouble(5.12345, 5.12)
    }

    @Test
    fun `rounds double to double with 13_496`(){
        runTestDouble(13.496, 13.50)
    }

    @Test
    fun `rounds double to double with 100_281`(){
        runTestDouble(100.281, 100.28)
    }

    @Test
    fun `rounds double to double with 199_999`(){
        runTestDouble(199.999, 200.00)
    }

    private fun runTestString(input: Double, expected: String){
        val actual = GeneralUtils.roundTwoPlacesString(input)
        assertEquals(expected, actual)
    }

    private fun runTestDouble(input: Double, expected: Double){
        val actual = GeneralUtils.roundTwoPlaces(input)
        assertEquals(expected, actual, 0.00001)
    }
}
