package com.tsquaredapplications.chains2.Utilities

import com.github.mikephil.charting.data.Entry
import com.tsquaredapplications.chains2.Objects.Hole
import com.tsquaredapplications.chains2.Objects.Scorecard
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class MPChartUtilTest {

    val scorecard = Scorecard(arrayListOf(3, 3, 3), mutableListOf(
            Hole(0, 3, 100),
            Hole(1, 4, 200),
            Hole(2, 5, 300)),
            -3, 2, 0, 0, "test", "red")

    @Test
    fun createEntriesForScoreLineChartComputes() {
        val actual = MPChartUtil.createEntriesForScoreLineChart(scorecard)
        val expected = listOf(
                Entry(1.toFloat(), 0.toFloat()),
                Entry(2.toFloat(), (-1).toFloat()),
                Entry(3.toFloat(), (-3).toFloat())
        )

        assertEquals(expected.get(0).x, actual.get(0).x)
        assertEquals(expected.get(1).x, actual.get(1).x)
        assertEquals(expected.get(2).x, actual.get(2).x)

        assertEquals(expected.get(0).y, actual.get(0).y)
        assertEquals(expected.get(1).y, actual.get(1).y)
        assertEquals(expected.get(2).y, actual.get(2).y)
    }

}