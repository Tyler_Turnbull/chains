package com.tsquaredapplications.chains2.Utilities

import org.junit.Assert.*
import org.junit.Test

class TrendUtilTest {
    val strokes = 50
    val avgAbove = 52.5
    val avgBelow = 47.5
    val avgMiddle1 = 49.9
    val avgMiddle2 = 48.0
    val avgMiddle3 = 52.0

    @Test
    fun `strokes below avg range computes as expected`() {
        assertEquals(-1, TrendUtil.calculateTrendValue(strokes, avgAbove))
    }

    @Test
    fun `strokes above avg range computes as expected`() {
        assertEquals(1, TrendUtil.calculateTrendValue(strokes, avgBelow))
    }

    @Test
    fun `strokes in range computes as expected`() {
        assertEquals(0, TrendUtil.calculateTrendValue(strokes, avgMiddle1))
    }

    @Test
    fun `strokes in range computes as expected 2`() {
        assertEquals(0, TrendUtil.calculateTrendValue(strokes, avgMiddle2))
    }

    @Test
    fun `strokes in range computes as expected 3`() {
        assertEquals(0, TrendUtil.calculateTrendValue(strokes, avgMiddle3))
    }

}