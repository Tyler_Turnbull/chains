package com.tsquaredapplications.chains2.Utilities;

import com.firebase.ui.auth.User;

import org.junit.Test;

import static org.junit.Assert.*;

public class UsernameUtilsTest {

    @Test
    public void isValidUsername1() {
        String username = "IsAValid_Username1";
        boolean isValid = UsernameUtils.isValidUsername(username);
        assertEquals(true, isValid);
    }

    @Test
    public void isAValidUsername2() {
        String username = "NotA12..ValidUsername";
        boolean isValid = UsernameUtils.isValidUsername(username);
        assertEquals(false, isValid);
    }
}