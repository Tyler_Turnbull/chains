package com.tsquaredapplications.chains2

import android.arch.persistence.room.Room
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.firebase.ui.auth.AuthUI
import com.tsquaredapplications.chains2.Enums.IntentExtraNames
import com.tsquaredapplications.chains2.Enums.PreferenceNames
import com.tsquaredapplications.chains2.Enums.RoomDbNames
import com.tsquaredapplications.chains2.Fragments.HomeFragment
import com.tsquaredapplications.chains2.Fragments.ProfileEditFragment
import com.tsquaredapplications.chains2.Fragments.ProfileFragment
import com.tsquaredapplications.chains2.Fragments.RoundsFragment
import com.tsquaredapplications.chains2.Friends.FriendsFragment
import com.tsquaredapplications.chains2.RoomDB.PlayerOverallStatsDatabase
import com.tsquaredapplications.chains2.RoomDB.PlayerTeeStatsDatabase
import com.tsquaredapplications.chains2.RoomDB.SavedCourseDatabase
import com.tsquaredapplications.chains2.RoomDB.SavedScorecardDatabase
import com.tsquaredapplications.chains2.Utilities.ProfilePicUtil
import com.tsquaredapplications.chains2.stats.StatsFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import org.jetbrains.anko.toast



class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
                                ProfileFragment.EditProfileClickListener{


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_menu)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        val toRoundFragment = intent.getBooleanExtra(IntentExtraNames.MAIN_TO_ROUNDS.toString(), false)
        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        if(toRoundFragment){
            swapFragment(RoundsFragment(), true)

        } else {
            // Display the home fragment
            swapFragment(HomeFragment(), true)
        }

    }

    override fun onBackPressed() {
        when{
            (drawer_layout.isDrawerOpen(GravityCompat.START)) -> drawer_layout.closeDrawer(GravityCompat.START)
            else -> super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.itemId.let {
            when(it){
                android.R.id.home -> drawer_layout.openDrawer(GravityCompat.START)
                R.id.action_sign_out -> {
                    AuthUI.getInstance().signOut(this)
                            .addOnCompleteListener {
                                val prefEditor = PreferenceManager.getDefaultSharedPreferences(applicationContext).edit()
                                prefEditor.putBoolean(PreferenceNames.HAS_USERNAME.toString(), false)
                                prefEditor.remove(PreferenceNames.USERNAME.toString())
                                prefEditor.apply()

                                // Remove Room DB's
                                val playerStatsDB = Room.databaseBuilder(applicationContext,
                                        PlayerOverallStatsDatabase::class.java, RoomDbNames.USER_OVERALL_STATS_DATABASE.toString())
                                        .allowMainThreadQueries()
                                        .build()

                                playerStatsDB.playerOverallStatsDao().deleteAllOverallStats()

                                playerStatsDB.close()

                                val playerTeeStatsDB = Room.databaseBuilder(applicationContext,
                                        PlayerTeeStatsDatabase::class.java, RoomDbNames.USER_TEE_STATS_DATABASE.toString())
                                        .allowMainThreadQueries()
                                        .build()

                                playerTeeStatsDB.playerTeeStatsDao().deleteAllPlayerTeeStats()

                                playerTeeStatsDB.close()

                                val courseDB = Room.databaseBuilder(applicationContext,
                                        SavedCourseDatabase::class.java, RoomDbNames.COURSES.toString())
                                        .allowMainThreadQueries()
                                        .build()

                                courseDB.savedCourseDao().deleteAllCourses()
                                courseDB.close()

                                val recentDB = Room.databaseBuilder(applicationContext,
                                        SavedCourseDatabase::class.java, RoomDbNames.RECENT_COURSES.toString())
                                        .allowMainThreadQueries()
                                        .build()

                                recentDB.savedCourseDao().deleteAllCourses()
                                recentDB.close()


                                val scorecardDB = Room.databaseBuilder(applicationContext,
                                        SavedScorecardDatabase::class.java, RoomDbNames.SAVED_SCORECARDS.toString())
                                        .allowMainThreadQueries()
                                        .build()

                                scorecardDB.savedScorecardDao().deleteAllScorecards()

                                scorecardDB.close()


                                // Remove profile picture
                                ProfilePicUtil.removePhoto(applicationContext)

                                finishAffinity()
                                startActivity(Intent(applicationContext, LoginActivity::class.java))
                            }
                }
                else -> return super.onOptionsItemSelected(item)

            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        item.isChecked = true
        val id = item.itemId

        when(id){
            R.id.nav_home -> swapFragment(HomeFragment(), true)
            R.id.nav_rounds -> swapFragment(RoundsFragment(), true)
            R.id.nav_friends -> swapFragment(FriendsFragment(), true)
            R.id.nav_stats -> swapFragment(StatsFragment(), true)
            R.id.nav_profile -> swapFragment(ProfileFragment(), true)
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    fun swapFragment(fragment: Fragment, addToBackStack: Boolean) {
        when (addToBackStack) {
            true -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.content_frame, fragment)
                        .addToBackStack(null)
                        .commit()
            }
            false -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.content_frame, fragment)
                        .disallowAddToBackStack()
                        .commit()
            }
        }
    }


    override fun onClickEditProfile() {
        swapFragment(ProfileEditFragment(), true)
    }
}
