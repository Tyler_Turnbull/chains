package com.tsquaredapplications.chains2.Friends;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.tsquaredapplications.chains2.Enums.FirebaseStorageNames;
import com.tsquaredapplications.chains2.R;
import com.tsquaredapplications.chains2.Utilities.GlideApp;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Tyler on 2/28/2018.
 */

public class FriendFeedItemAdapter extends RecyclerView.Adapter<FriendFeedItemAdapter.FriendFeedItemViewHolder> {

    private ArrayList<FriendFeedItem> feedList;
    private Context context;

    public FriendFeedItemAdapter(ArrayList<FriendFeedItem> feedList){
        this.feedList = feedList;
    }

    @Override
    public FriendFeedItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.friend_feed_list_item, parent, false);
        return new FriendFeedItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FriendFeedItemViewHolder holder, int position) {
        FriendFeedItem feedItem = feedList.get(position);
        holder.courseNameTextView.setText(feedItem.getCourseName());
        holder.dateTextView.setText(feedItem.getDate());
        holder.nameTextView.setText(feedItem.getUsername());

        // profile picture
        StorageReference storageRef = FirebaseStorage.getInstance().getReference()
                .child(FirebaseStorageNames.PROFILE_PICTURES.toString())
                .child(feedItem.getUid());

        GlideApp.with(context)
                .load(storageRef)
                .into(holder.pictureImageView);

        if(feedItem.getScore() == 0) {
            String text = "Even";
            holder.scoreTextView.setText(text);
        }
        else if(feedItem.getScore() > 0) {
            String text = "+" + String.format(Locale.getDefault(), "%d", feedItem.getScore());
            holder.scoreTextView.setText(text);
        }
        else
            holder.scoreTextView.setText(String.format(Locale.getDefault(), "%d", feedItem.getScore()));

        holder.strokesTextView.setText(String.format(Locale.getDefault(),"%d", feedItem.getStrokes()));


    }

    @Override
    public int getItemCount() {
        return feedList.size();
    }


    public class FriendFeedItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.friend_feed_list_item_tv_name)
        TextView nameTextView;

        @BindView(R.id.friend_feed_list_item_tv_course_name)
        TextView courseNameTextView;

        @BindView(R.id.friend_feed_list_item_civ_profile_pic)
        CircleImageView pictureImageView;

        @BindView(R.id.friend_feed_list_item_tv_date)
        TextView dateTextView;

        @BindView(R.id.friend_feed_list_item_tv_score_amount)
        TextView scoreTextView;

        @BindView(R.id.friend_feed_list_item_tv_threw_amount)
        TextView strokesTextView;

        public FriendFeedItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
