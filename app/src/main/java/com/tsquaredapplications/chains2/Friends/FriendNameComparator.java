package com.tsquaredapplications.chains2.Friends;

import java.util.Comparator;

/**
 * Created by Tyler on 3/2/2018.
 */

public class FriendNameComparator implements Comparator<Friend> {
    @Override
    public int compare(Friend friend1, Friend friend2) {
        return friend1.getUsername().compareToIgnoreCase(friend2.getUsername());
    }
}
