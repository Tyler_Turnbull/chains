package com.tsquaredapplications.chains2.Friends;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Tyler on 3/2/2018.
 *
 * Object for containing information on a friend request
 */

public class Friend implements Parcelable {
    private String uid;
    private String username;

    public Friend() {

    }

    public Friend(String uid, String username) {
        this.uid = uid;
        this.username = username;
    }

    protected Friend(Parcel in) {
        uid = in.readString();
        username = in.readString();
    }

    public static final Creator<Friend> CREATOR = new Creator<Friend>() {
        @Override
        public Friend createFromParcel(Parcel in) {
            return new Friend(in);
        }

        @Override
        public Friend[] newArray(int size) {
            return new Friend[size];
        }
    };

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(uid);
        parcel.writeString(username);
    }
}
