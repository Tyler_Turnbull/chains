package com.tsquaredapplications.chains2.Friends;

/**
 * Created by Tyler on 2/28/2018.
 *
 * Models a list item object for friend feed
 */

public class FriendFeedItem {
    private String username;
    private String uid; // for retrieving profile picture
    private String courseName;
    private int strokes;
    private int score;
    private String date;

    public FriendFeedItem() {
    }

    public FriendFeedItem(String username, String uid, String courseName, int strokes, int score, String date) {
        this.username = username;
        this.uid = uid;
        this.courseName = courseName;
        this.strokes = strokes;
        this.score = score;
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getStrokes() {
        return strokes;
    }

    public void setStrokes(int strokes) {
        this.strokes = strokes;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "FriendFeedItem{" +
                "username='" + username + '\'' +
                ", uid='" + uid + '\'' +
                ", courseName='" + courseName + '\'' +
                ", strokes=" + strokes +
                ", score=" + score +
                ", date='" + date + '\'' +
                '}';
    }
}
