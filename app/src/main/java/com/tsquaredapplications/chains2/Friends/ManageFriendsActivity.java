package com.tsquaredapplications.chains2.Friends;

import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tsquaredapplications.chains2.Dialogs.FriendSearchDialog;
import com.tsquaredapplications.chains2.Enums.FirebaseDbNames;
import com.tsquaredapplications.chains2.Enums.PreferenceNames;
import com.tsquaredapplications.chains2.R;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ManageFriendsActivity extends AppCompatActivity
        implements FriendSearchDialog.FriendSearchDialogListener,
                   ManageFriendsRecyclerAdapter.FriendOnClickHandler{

    private static final String LOG_TAG = ManageFriendsActivity.class.getName();

    private ArrayList<Friend> mRequestList;
    private ArrayList<Friend> mFriendList;
    private LinearLayoutManager mLinearLayoutManager;
    private ManageFriendsRecyclerAdapter mAdapter;

    @BindView(R.id.activity_manage_friends_rv)
    RecyclerView mRecyclerView;

    @BindView(R.id.activity_manage_friends_pb)
    ProgressBar mProgressBar;

    @BindView(R.id.manage_friends_no_friends_text_view)
    TextView mNoFriendsTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_friends);
        ButterKnife.bind(this);
        mLinearLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);

        getFriendData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.find_friends, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id){
            case R.id.action_find_friend:
                DialogFragment dialog = new FriendSearchDialog();
                dialog.show(getFragmentManager(), "FriendSearchDialogFragment");
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDialogPositiveClick(final String username) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if(prefs.getString(PreferenceNames.USERNAME.toString(), "null").equals(username)){
            Toast.makeText(this, "That is your username", Toast.LENGTH_SHORT).show();
            return;
        }
        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference().child(FirebaseDbNames.USER_DIRECTORY.toString()).child(username);
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    String uid = dataSnapshot.getValue(String.class);
                    Log.i(LOG_TAG, uid);
                    makeFriendRequest(uid, username);

                } else {
                    // no user found with that username
                    Toast.makeText(ManageFriendsActivity.this,
                            "No user found with that username", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void makeFriendRequest(String uid, final String username){

        // add to friend_requests
        // get this users username
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final String thisUsersUsername = prefs.getString(PreferenceNames.USERNAME.toString(), "null");

        // Prepare request data
        final Friend request = new Friend(FirebaseAuth.getInstance().getUid(), thisUsersUsername);
        final DatabaseReference requestDatabase = FirebaseDatabase.getInstance().getReference()
                .child(FirebaseDbNames.FRIEND_REQUESTS.toString())
                .child(uid);

        requestDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<Friend> requestList = new ArrayList<>();
                Iterable<DataSnapshot> data = dataSnapshot.getChildren();
                for(DataSnapshot d: data){
                    Friend f = d.getValue(Friend.class);
                    if(f.getUsername().equals(thisUsersUsername)){
                        Toast.makeText(ManageFriendsActivity.this,
                                "You have already sent a request to " + username, Toast.LENGTH_SHORT).show();
                    }
                    requestList.add(f);
                }

                requestList.add(request);
                Collections.sort(requestList, new FriendNameComparator());
                requestDatabase.setValue(requestList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Toast.makeText(this, "Request sent", Toast.LENGTH_SHORT).show();

    }


    public void getFriendData(){

        mFriendList = new ArrayList<>();
        mRequestList = new ArrayList<>();

        DatabaseReference requestDB = FirebaseDatabase.getInstance().getReference()
                .child(FirebaseDbNames.FRIEND_REQUESTS.toString())
                .child(FirebaseAuth.getInstance().getUid());

        requestDB.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> data = dataSnapshot.getChildren();
                for(DataSnapshot d: data){
                    Friend request = d.getValue(Friend.class);
                    mRequestList.add(request);
                }

                DatabaseReference friendDB = FirebaseDatabase.getInstance().getReference()
                        .child(FirebaseDbNames.FRIENDS.toString())
                        .child(FirebaseAuth.getInstance().getUid());

                friendDB.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Iterable<DataSnapshot> data = dataSnapshot.getChildren();
                        for(DataSnapshot d: data){
                            Friend friend = d.getValue(Friend.class);
                            mFriendList.add(friend);
                        }


                        if(mFriendList.isEmpty() && mRequestList.isEmpty()){
                            showNoFriends();
                        } else {
                            createList();
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void createList(){
        mAdapter = new ManageFriendsRecyclerAdapter(getApplicationContext(), mRequestList, mFriendList, this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mProgressBar.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    public void showNoFriends(){
        mProgressBar.setVisibility(View.GONE);
        mNoFriendsTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(Friend friend) {
        // Click on friend in friend list, not on request
    }
}
