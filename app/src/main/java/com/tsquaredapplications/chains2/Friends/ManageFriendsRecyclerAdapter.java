package com.tsquaredapplications.chains2.Friends;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tsquaredapplications.chains2.Enums.FirebaseDbNames;
import com.tsquaredapplications.chains2.Enums.PreferenceNames;
import com.tsquaredapplications.chains2.R;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Tyler on 3/2/2018.
 */

public class ManageFriendsRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private static final String LOG_TAG = ManageFriendsRecyclerAdapter.class.getName();

    private ArrayList<Friend> mRequestList;
    private ArrayList<Friend> mFriendList;
    private final FriendOnClickHandler mHandler;

    private static final int TYPE_REQUEST = 0;
    private static final int TYPE_FRIEND = 1;
    private static final int TYPE_HEADER = 2;

    private int numRequests;
    private int numFriends;

    private Context mContext;


    public interface FriendOnClickHandler {
        void onClick(Friend friend);
    }

    public ManageFriendsRecyclerAdapter(Context context, ArrayList<Friend> requestList, ArrayList<Friend> friendList,
                                        FriendOnClickHandler handler){
        mContext = context;
        mRequestList = requestList;
        mFriendList = friendList;
        mHandler = handler;

        numRequests = mRequestList.size();
        numFriends = mFriendList.size();


    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_REQUEST){
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.friend_request_list_item, parent,false);
            return new FriendRequestViewHolder(view);
        } else if(viewType == TYPE_FRIEND) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.friend_list_item, parent, false);
            return new FriendViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()
                    ).inflate(R.layout.friend_list_header_item, parent, false);
            return new FriendHeaderViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if(getItemViewType(position) == TYPE_REQUEST){
            final Friend friendRequest = mRequestList.get(holder.getAdapterPosition() - 1);
            FriendRequestViewHolder viewHolder = (FriendRequestViewHolder)holder;
            viewHolder.usernameTV.setText(friendRequest.getUsername());
            viewHolder.acceptReqeustFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    mFriendList.add(friendRequest);
                    mRequestList.remove(holder.getAdapterPosition() -1);
                    notifyDataSetChanged();

                    Collections.sort(mFriendList, new FriendNameComparator());

                    DatabaseReference friendDB = FirebaseDatabase.getInstance().getReference()
                            .child(FirebaseDbNames.FRIENDS.toString())
                            .child(FirebaseAuth.getInstance().getUid());

                    friendDB.setValue(mFriendList);

                    DatabaseReference requestDB = FirebaseDatabase.getInstance().getReference()
                            .child(FirebaseDbNames.FRIEND_REQUESTS.toString())
                            .child(FirebaseAuth.getInstance().getUid());


                    requestDB.setValue(mRequestList);

                    // Now update the requesters friend list to also add the frienship to their
                    // friends list
                    final DatabaseReference requesterDB = FirebaseDatabase.getInstance().getReference()
                            .child(FirebaseDbNames.FRIENDS.toString())
                            .child(friendRequest.getUid());

                    requesterDB.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
                            ArrayList<Friend> requesterFriendList = new ArrayList<>();
                            Iterable<DataSnapshot> data = dataSnapshot.getChildren();
                            for(DataSnapshot d: data){
                                requesterFriendList.add(d.getValue(Friend.class));
                            }
                            String username = prefs.getString(PreferenceNames.USERNAME.toString(), "null");

                            requesterFriendList.add(new Friend(FirebaseAuth.getInstance().getUid(),
                                    username));

                            Collections.sort(requesterFriendList, new FriendNameComparator());
                            requesterDB.setValue(requesterFriendList);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });




                }
            });
            viewHolder.declineRequestFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mRequestList.remove(holder.getAdapterPosition() -1);
                    notifyDataSetChanged();

                    DatabaseReference requestDB = FirebaseDatabase.getInstance().getReference()
                            .child(FirebaseDbNames.FRIEND_REQUESTS.toString())
                            .child(FirebaseAuth.getInstance().getUid());


                    requestDB.setValue(mRequestList);



                }
            });

        } else if (getItemViewType(position) == TYPE_FRIEND){
            Log.i(LOG_TAG, "TYPE FRIEND");
            int requestOffset = 0;
            if(mRequestList.size() > 0){
                requestOffset += 1 + mRequestList.size();
            }
            Friend friend = mFriendList.get(holder.getAdapterPosition() - 1 - requestOffset);
            Log.i(LOG_TAG,  mFriendList.get(holder.getAdapterPosition() - 1 - requestOffset).toString());
            FriendViewHolder viewHolder = (FriendViewHolder)holder;
            viewHolder.usernameTV.setText(friend.getUsername());


        } else {
            FriendHeaderViewHolder viewHolder = (FriendHeaderViewHolder) holder;
            if (!mRequestList.isEmpty() && position == 0)
                viewHolder.headerTV.setText(mContext.getString(R.string.requests));
            else
                viewHolder.headerTV.setText(mContext.getString(R.string.friends));
        }
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if(mRequestList.size() > 0)
            size += mRequestList.size() + 1; // +1 for header

        if(mFriendList.size() > 0)
            size += mFriendList.size() + 1;

        return size;
    }

    @Override
    public int getItemViewType(int position) {

        if(position == 0)
            return TYPE_HEADER;
        else if (position <= mRequestList.size())
            return TYPE_REQUEST;
        else if(position == mRequestList.size() + 1 && mRequestList.size() != 0)
            return TYPE_HEADER;
        else
            return TYPE_FRIEND;

    }

    public class FriendRequestViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.friend_request_list_item_fab_accept)
        Button acceptReqeustFab;

        @BindView(R.id.friend_request_list_item_fab_decline)
        Button declineRequestFab;

        @BindView(R.id.friend_request_list_item_tv_username)
        TextView usernameTV;

        public FriendRequestViewHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this,itemView);


        }

    }

    public class FriendViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.friend_list_item_tv_username)
        TextView usernameTV;

        @BindView(R.id.friend_list_item_view_line_break)
        View friendDividerLineView;

        public FriendViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            // TODO: Open user details screen / user stats
            int adapterPosition = getAdapterPosition();
        }
    }

    public class FriendHeaderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.friend_list_item_header_tv)
        TextView headerTV;

        public FriendHeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        @Override
        public void onClick(View view) {

        }
    }
}
