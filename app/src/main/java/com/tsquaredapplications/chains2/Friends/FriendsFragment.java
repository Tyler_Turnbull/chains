package com.tsquaredapplications.chains2.Friends;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tsquaredapplications.chains2.Enums.FirebaseDbNames;
import com.tsquaredapplications.chains2.R;
import com.tsquaredapplications.chains2.Utilities.NetworkUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FriendsFragment extends Fragment {

    @BindView(R.id.friends_fragment_rv)
    RecyclerView mRecyclerView;

    @BindView(R.id.friends_fragment_btn_manage_friends)
    Button manageFriendsButton;

    @BindView(R.id.friends_fragment_progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.friends_fragment_no_items_text_view)
    TextView noItemsTextView;



    ArrayList<FriendFeedItem> friendFeedList;
    FriendFeedItemAdapter mAdapter;

    public FriendsFragment() {
        // Required empty public constructor
    }

    /* TODO: Need to implement
    1) adding friends
    2) removing friends
    3) displaying friends in manage friends
    4) adding FriendFeedItems to database
    5) display feed items here

    intermix friends list with friend requests
    top of list is requests items
    second part of list is friends items

    progress::
    able to retrieve UID from firebase in ManageFriendsActivity.

    Next Step: add friend request to firebase DB. check FirebaseDbNames for notes on that.
     */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friends, container, false);

        ButterKnife.bind(this, view);

        if(!NetworkUtils.isNetworkAvailable(getContext())){
            displayNoConnection();
        } else {
            manageFriendFeed();

            manageFriendsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), ManageFriendsActivity.class);
                    startActivity(intent);
                }
            });
        }
        return view;
    }

    private void manageFriendFeed(){
        DatabaseReference db = FirebaseDatabase.getInstance().getReference()
                .child(FirebaseDbNames.FRIEND_FEED.toString())
                .child(FirebaseAuth.getInstance().getUid());

        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                friendFeedList = new ArrayList<>();
                Iterable<DataSnapshot> data = dataSnapshot.getChildren();
                for(DataSnapshot d: data){
                    friendFeedList.add(d.getValue(FriendFeedItem.class));
                }

                if(friendFeedList.size() == 0){
                    displayNoItems();
                }

                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(),
                        LinearLayoutManager.VERTICAL, false);

                mRecyclerView.setLayoutManager(layoutManager);
                mRecyclerView.setHasFixedSize(true);
                mAdapter = new FriendFeedItemAdapter(friendFeedList);
                mRecyclerView.setAdapter(mAdapter);

                displayItems();

            }



            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void displayNoItems(){
        progressBar.setVisibility(View.GONE);
        noItemsTextView.setVisibility(View.VISIBLE);

    }

    private void displayItems(){
        progressBar.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void displayNoConnection(){
        progressBar.setVisibility(View.GONE);
        noItemsTextView.setVisibility(View.VISIBLE);
        noItemsTextView.setText(getString(R.string.no_connection_message));
        manageFriendsButton.setActivated(false);
    }

}
