package com.tsquaredapplications.chains2

import android.arch.persistence.room.Room
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.tsquaredapplications.chains2.Adapters.CourseInformationAdapter
import com.tsquaredapplications.chains2.Adapters.CourseToInformationAdapter
import com.tsquaredapplications.chains2.Enums.IntentExtraNames
import com.tsquaredapplications.chains2.Enums.RoomDbNames
import com.tsquaredapplications.chains2.Objects.Course
import com.tsquaredapplications.chains2.Objects.CourseInformation
import com.tsquaredapplications.chains2.RoomDB.SavedCourseDatabase
import kotlinx.android.synthetic.main.activity_course_selection.*
import java.util.*

class CourseSelectionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_course_selection)

        supportActionBar?.hide()

        val isSavedCourses = intent.getBooleanExtra(IntentExtraNames.IS_SAVED_COURSE.toString(), false)
        val isRecentCourses = intent.getBooleanExtra(IntentExtraNames.IS_RECENT_COURSES.toString(), false)
        var courseList: MutableList<Course> = mutableListOf()
        var courseInformationList: MutableList<CourseInformation> = mutableListOf()
        if(isSavedCourses){
            val db = Room.databaseBuilder(this, SavedCourseDatabase::class.java,
                    RoomDbNames.COURSES.toString())
                    .allowMainThreadQueries()
                    .build()

            courseList = db.savedCourseDao().loadAllCourses().toMutableList()
            Collections.sort(courseList, kotlin.Comparator { o1, o2 ->
                 o1.name.compareTo(o2.name)
            })

            db.close()


        } else if (isRecentCourses) {
            val db = Room.databaseBuilder(this, SavedCourseDatabase::class.java,
                    RoomDbNames.RECENT_COURSES.toString())
                    .allowMainThreadQueries()
                    .build()

            courseList = db.savedCourseDao().loadAllCourses().toMutableList()
            Collections.sort(courseList, kotlin.Comparator { o1, o2 ->
                o1.name.compareTo(o2.name)
            })

            db.close()
        } else {
            // Not saved or recent courses
            courseInformationList = intent.getParcelableArrayListExtra(IntentExtraNames.COURSE_LIST.toString())
        }

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false)

        course_list_recycler_view.layoutManager = layoutManager
        course_list_recycler_view.setHasFixedSize(true)
        val courseListAdapter = if (isSavedCourses || isRecentCourses) CourseToInformationAdapter(courseList, isSavedCourses)
        else CourseInformationAdapter(courseInformationList)
        course_list_recycler_view.adapter = courseListAdapter

        if(courseList.isEmpty() && isSavedCourses){
            course_selection_error_text_view.text = getString(R.string.saved_course_error_message)
            course_selection_error_text_view.visibility = View.VISIBLE
        } else if (courseList.isEmpty() && isRecentCourses){
            course_selection_error_text_view.text = getString(R.string.recent_course_error_message)
            course_selection_error_text_view.visibility = View.VISIBLE
        }


    }
}
