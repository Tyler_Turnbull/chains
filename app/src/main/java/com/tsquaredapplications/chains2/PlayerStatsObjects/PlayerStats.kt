package com.tsquaredapplications.chains2.PlayerStatsObjects

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.tsquaredapplications.chains2.Enums.ScoreNames
import com.tsquaredapplications.chains2.Objects.Scorecard
import com.tsquaredapplications.chains2.Utilities.GolfUtils
import com.tsquaredapplications.chains2.Utilities.TrendUtil

@Entity(tableName = "user_overall_stats")
data class PlayerStats(
        @PrimaryKey var uid: String = "",
        var totalStrokes: Int = 0,
        var numRoundsPlayed: Int = 0,
        var lifetimePlusMinus: Int = 0,
        var numAlbatross: Int = 0,
        var numAces: Int = 0,
        var numEagles: Int = 0,
        var numBirdies: Int = 0,
        var numPars: Int = 0,
        var numBogeys: Int = 0,
        var numDoubleBogeys: Int = 0,
        var numTripleBogeyPlus: Int = 0,
        var trendList: ArrayList<Int> = arrayListOf()
        ) {

    fun getAvgStrokes(): Double{
        return totalStrokes.toDouble() / numRoundsPlayed.toDouble()
    }

    companion object {
        fun firstTime(scorecard: Scorecard, uid: String): PlayerStats {
            val numHoles = scorecard.getNumHoles()
            var totalStrokes = 0
            var numAces = 0
            var numAlbatross = 0
            var numEagles = 0
            var numBirdies = 0
            var numPars = 0
            var numBogeys = 0
            var numDoubleBogeys = 0
            var numTripleBogeysPlus = 0

            for(i in 0 until numHoles){
                totalStrokes += scorecard.getHoleScore(i)

                when(GolfUtils.determineScoreName(scorecard.getHoleScore(i), scorecard.getHolePar(i))){
                    ScoreNames.ACE -> numAces++
                    ScoreNames.ALBATROSS -> numAlbatross++
                    ScoreNames.EAGLE -> numEagles++
                    ScoreNames.BIRDIE -> numBirdies++
                    ScoreNames.PAR -> numPars++
                    ScoreNames.BOGEY -> numBogeys++
                    ScoreNames.DBOGEY -> numDoubleBogeys++
                    else -> numTripleBogeysPlus++
                }
            }

            // Let trend list be assigned by default value as we wont worry about trend
            // until after five rounds have been played
            return PlayerStats(uid, totalStrokes, 1, scorecard.plusMinus,
                    numAlbatross, numAces, numEagles, numBirdies, numPars, numBogeys, numDoubleBogeys,
                    numTripleBogeysPlus, arrayListOf())

        }


        /***
         * Creates an updated PlayerStats object from a scorecard and old PlayerStats object.
         * TrendValue is the -1, 0, or 1 value based on how the player performed on the played course
         * in comparison to their course average. -1 for 2 strokes over avg. 1 for 2 strokes less than
         * average else 0.
         *
         * The trend list will only hold the last 10 trend values. AND trendValues are only added if the
         * player has played that course 5 times or more. If player has not played 5 or more rounds on
         * the course 10 will be passed into trendValue
         * @param scorecard
         * @param oldStats
         * @param trendValue
         * @return
         */
        fun mergeStats(scorecard: Scorecard, oldStats: PlayerStats, trendValue: Int): PlayerStats {
            val numHoles = scorecard.getNumHoles()
            var totalStrokes = oldStats.totalStrokes
            var numAces = oldStats.numAces
            var numAlbatross = oldStats.numAlbatross
            var numEagles = oldStats.numEagles
            var numBirdies = oldStats.numBirdies
            var numPars = oldStats.numPars
            var numBogeys = oldStats.numBogeys
            var numDoubleBogeys = oldStats.numDoubleBogeys
            var numTripleBogeysPlus = oldStats.numTripleBogeyPlus

            for(i in 0 until numHoles){
                totalStrokes += scorecard.getHoleScore(i)

                when(GolfUtils.determineScoreName(scorecard.getHoleScore(i), scorecard.getHolePar(i))){
                    ScoreNames.ACE -> numAces++
                    ScoreNames.ALBATROSS -> numAlbatross++
                    ScoreNames.EAGLE -> numEagles++
                    ScoreNames.BIRDIE -> numBirdies++
                    ScoreNames.PAR -> numPars++
                    ScoreNames.BOGEY -> numBogeys++
                    ScoreNames.DBOGEY -> numDoubleBogeys++
                    else -> numTripleBogeysPlus++
                }
            }


            var trendList = oldStats.trendList
            if(oldStats.numRoundsPlayed >= 4 && trendValue != 10){
                trendList.add(trendValue)
                if(trendList.size > 10){
                    trendList.removeAt(0)
                }
            }

            return PlayerStats(oldStats.uid, totalStrokes,
                    oldStats.numRoundsPlayed + 1,
                    oldStats.lifetimePlusMinus + scorecard.plusMinus,
                    numAlbatross, numAces, numEagles, numBirdies, numPars, numBogeys, numDoubleBogeys,
                    numTripleBogeysPlus, trendList)
        }
    }




}