package com.tsquaredapplications.chains2.PlayerStatsObjects

import android.arch.persistence.room.Entity
import android.os.Parcel
import android.os.Parcelable
import com.tsquaredapplications.chains2.Enums.ScoreNames
import com.tsquaredapplications.chains2.Objects.Scorecard
import com.tsquaredapplications.chains2.Utilities.GolfUtils
import java.util.*
import kotlin.collections.ArrayList

@Entity(tableName = "user_course_tee_stats",
        primaryKeys = ["courseId", "teeNumber"])
class PlayerTeeStats(
        var courseId: Int = 0,
        var teeNumber: Int = 0,
        var roundsPlayed: Int = 0,
        var totalStrokes: Int = 0,
        var avgStrokes: Double = 0.0,
        var lifetimePlusMinus: Int = 0,
        var bestScore: Int = 0,
        var worstScore: Int = 0,
        var numAlbatross: Int = 0,
        var numAces: Int = 0,
        var numEagles: Int = 0,
        var numBirdies: Int = 0,
        var numPars: Int = 0,
        var numBogeys: Int = 0,
        var numDoubleBogeys: Int = 0,
        var numTripleBogeyPlus: Int = 0,
        var scoresHistory: ArrayList<RoundStats> = arrayListOf(),
        var holeStats: ArrayList<HoleStats> = arrayListOf(),
        var username: String = "")
    : Parcelable {

    constructor(parcel: Parcel) : this() {
        this.courseId = parcel.readInt()
        this.teeNumber = parcel.readInt()
        this.roundsPlayed = parcel.readInt()
        this.totalStrokes = parcel.readInt()
        this.avgStrokes = parcel.readDouble()
        this.lifetimePlusMinus = parcel.readInt()
        this.bestScore = parcel.readInt()
        this.worstScore = parcel.readInt()
        this.numAlbatross = parcel.readInt()
        this.numAces = parcel.readInt()
        this.numEagles = parcel.readInt()
        this.numBirdies = parcel.readInt()
        this.numPars = parcel.readInt()
        this.numBogeys = parcel.readInt()
        this.numDoubleBogeys = parcel.readInt()
        this.numTripleBogeyPlus = parcel.readInt()
        val scoresHistory = mutableListOf<RoundStats>()
        parcel.readTypedList(scoresHistory, RoundStats.CREATOR)
        val holeStats = mutableListOf<HoleStats>()
        parcel.readTypedList(holeStats, HoleStats.CREATOR)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(courseId)
        parcel.writeInt(teeNumber)
        parcel.writeInt(roundsPlayed)
        parcel.writeInt(totalStrokes)
        parcel.writeDouble(avgStrokes)
        parcel.writeInt(lifetimePlusMinus)
        parcel.writeInt(bestScore)
        parcel.writeInt(worstScore)
        parcel.writeInt(numAlbatross)
        parcel.writeInt(numAces)
        parcel.writeInt(numEagles)
        parcel.writeInt(numBirdies)
        parcel.writeInt(numPars)
        parcel.writeInt(numBogeys)
        parcel.writeInt(numDoubleBogeys)
        parcel.writeInt(numTripleBogeyPlus)
        parcel.writeString(username)
    }


    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "PlayerTeeStats(courseId=$courseId, teeNumber=$teeNumber, roundsPlayed=$roundsPlayed, totalStrokes=$totalStrokes, avgStrokes=$avgStrokes, lifetimePlusMinus=$lifetimePlusMinus, bestScore=$bestScore, worstScore=$worstScore, numAlbatross=$numAlbatross, numAces=$numAces, numEagles=$numEagles, numBirdies=$numBirdies, numPars=$numPars, numBogeys=$numBogeys, numDoubleBogeys=$numDoubleBogeys, numTripleBogeyPlus=$numTripleBogeyPlus, scoresHistory=$scoresHistory, holeStats=$holeStats, username='$username')"
    }

    companion object CREATOR : Parcelable.Creator<PlayerTeeStats> {
        override fun createFromParcel(parcel: Parcel): PlayerTeeStats {
            return PlayerTeeStats(parcel)
        }

        override fun newArray(size: Int): Array<PlayerTeeStats?> {
            return arrayOfNulls(size)
        }


        fun firstTime(scorecard: Scorecard, courseId: Int,
                      username: String): PlayerTeeStats{

            val numHoles = scorecard.getNumHoles()
            var totalStrokes = 0
            var numAces = 0
            var numAlbatross = 0
            var numEagles = 0
            var numBirdies = 0
            var numPars = 0
            var numBogeys = 0
            var numDoubleBogeys = 0
            var numTripleBogeysPlus = 0

            var holeStatsList = arrayListOf<HoleStats>()

            for(i in 0 until numHoles){
                totalStrokes += scorecard.getHoleScore(i)

                when(GolfUtils.determineScoreName(scorecard.getHoleScore(i),
                        scorecard.getHolePar(i))){
                    ScoreNames.ACE -> numAces++
                    ScoreNames.ALBATROSS -> numAlbatross++
                    ScoreNames.EAGLE -> numEagles++
                    ScoreNames.BIRDIE -> numBirdies++
                    ScoreNames.PAR -> numPars++
                    ScoreNames.BOGEY -> numBogeys++
                    ScoreNames.DBOGEY -> numDoubleBogeys++
                    else -> numTripleBogeysPlus++
                }

                holeStatsList.add(HoleStats.CREATOR.firstTime(scorecard.getHoleScore(i),
                        scorecard.getHolePar(i)))
            }

            return PlayerTeeStats(courseId, scorecard.teeChoice,
                    1,totalStrokes, totalStrokes.toDouble(), scorecard.plusMinus, totalStrokes, totalStrokes,
                    numAlbatross, numAces, numEagles, numBirdies, numPars,
                    numBogeys, numDoubleBogeys, numTripleBogeysPlus,
                    arrayListOf(RoundStats(scorecard.scores)),
                    holeStatsList, username)
        }

        fun mergeStats(scorecard: Scorecard, oldStats: PlayerTeeStats,
                       username: String) : PlayerTeeStats {

            val numHoles = scorecard.getNumHoles()
            var totalStrokes = 0
            var numAces = oldStats.numAces
            var numAlbatross = oldStats.numAlbatross
            var numEagles = oldStats.numEagles
            var numBirdies = oldStats.numBirdies
            var numPars = oldStats.numPars
            var numBogeys = oldStats.numBogeys
            var numDoubleBogeys = oldStats.numDoubleBogeys
            var numTripleBogeysPlus = oldStats.numTripleBogeyPlus

            val oldHoleStats = oldStats.holeStats
            val newHoleStats = arrayListOf<HoleStats>()

            for(i in 0 until numHoles){
                totalStrokes += scorecard.getHoleScore(i)

                when(GolfUtils.determineScoreName(scorecard.getHoleScore(i),
                        scorecard.getHolePar(i))){
                    ScoreNames.ACE -> numAces++
                    ScoreNames.ALBATROSS -> numAlbatross++
                    ScoreNames.EAGLE -> numEagles++
                    ScoreNames.BIRDIE -> numBirdies++
                    ScoreNames.PAR -> numPars++
                    ScoreNames.BOGEY -> numBogeys++
                    ScoreNames.DBOGEY -> numDoubleBogeys++
                    else -> numTripleBogeysPlus++
                }

                val oldCurrentHoleStat = oldHoleStats[i]
                newHoleStats.add(HoleStats.mergeStats(oldCurrentHoleStat,
                        scorecard.getHoleScore(i),
                        scorecard.getHolePar(i)))

            }

            var bestScore = if(oldStats.bestScore > totalStrokes) totalStrokes else oldStats.bestScore
            var worstScore = if(oldStats.worstScore < totalStrokes) totalStrokes else oldStats.worstScore
            val roundStatHistory = oldStats.scoresHistory
            roundStatHistory.add(RoundStats(scorecard.scores))

            return PlayerTeeStats(scorecard.courseId, scorecard.teeChoice,
                    oldStats.roundsPlayed + 1, oldStats.totalStrokes + totalStrokes,
                    (oldStats.totalStrokes + totalStrokes).toDouble() / (oldStats.roundsPlayed + 1).toDouble(),
                    oldStats.lifetimePlusMinus + scorecard.plusMinus,
                    bestScore, worstScore, numAlbatross, numAces, numEagles, numBirdies,
                    numPars, numBogeys, numDoubleBogeys, numTripleBogeysPlus,
                    roundStatHistory, newHoleStats, username)
        }

    }


}