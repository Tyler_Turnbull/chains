package com.tsquaredapplications.chains2.PlayerStatsObjects

import android.os.Parcel
import android.os.Parcelable
import com.tsquaredapplications.chains2.Enums.ScoreNames
import com.tsquaredapplications.chains2.Utilities.GolfUtils

data class HoleStats(
        var timesPlayed: Int = 0,
        var totalStrokes: Int = 0,
        var avgStrokes: Double = 0.0,
        var lifetimePlusMinus: Int = 0,
        var bestScore: Int = 0,
        var worstScore:Int = 0,
        var numAlbatross: Int = 0,
        var numAces: Int = 0,
        var numEagles: Int = 0,
        var numBirdies: Int = 0,
        var numPars: Int = 0,
        var numBogeys: Int = 0,
        var numDoubleBogeys: Int = 0,
        var numTripleBogeysPlus: Int = 0
): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readDouble(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(timesPlayed)
        parcel.writeInt(totalStrokes)
        parcel.writeDouble(avgStrokes)
        parcel.writeInt(lifetimePlusMinus)
        parcel.writeInt(bestScore)
        parcel.writeInt(worstScore)
        parcel.writeInt(numAlbatross)
        parcel.writeInt(numAces)
        parcel.writeInt(numEagles)
        parcel.writeInt(numBirdies)
        parcel.writeInt(numPars)
        parcel.writeInt(numBogeys)
        parcel.writeInt(numDoubleBogeys)
        parcel.writeInt(numTripleBogeysPlus)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HoleStats> {
        override fun createFromParcel(parcel: Parcel): HoleStats {
            return HoleStats(parcel)
        }

        override fun newArray(size: Int): Array<HoleStats?> {
            return arrayOfNulls(size)
        }


        fun firstTime(score: Int, par: Int): HoleStats {
            var numAlbatross  = 0
            var numAces = 0
            var numEagles = 0
            var numBirdies = 0
            var numPars = 0
            var numBogeys = 0
            var numDoubleBogeys  = 0
            var numTripleBogeysPlus = 0

            when(GolfUtils.determineScoreName(score, par)){
                ScoreNames.ACE -> numAces++
                ScoreNames.ALBATROSS -> numAlbatross++
                ScoreNames.EAGLE -> numEagles++
                ScoreNames.BIRDIE -> numBirdies++
                ScoreNames.PAR -> numPars++
                ScoreNames.BOGEY -> numBogeys++
                ScoreNames.DBOGEY -> numDoubleBogeys++
                else -> numTripleBogeysPlus++
            }

            return HoleStats(1,score, score.toDouble(),
                    score - par, score, score,
                    numAlbatross, numAces, numEagles, numBirdies, numPars,
                    numBogeys, numDoubleBogeys, numTripleBogeysPlus)
        }


        fun mergeStats(oldStats: HoleStats, score: Int, par: Int): HoleStats{

            var numAces = oldStats.numAces
            var numAlbatross = oldStats.numAlbatross
            var numEagles = oldStats.numEagles
            var numBirdies = oldStats.numBirdies
            var numPars = oldStats.numPars
            var numBogeys = oldStats.numBogeys
            var numDoubleBogeys = oldStats.numDoubleBogeys
            var numTripleBogeysPlus = oldStats.numTripleBogeysPlus


            when(GolfUtils.determineScoreName(score, par)){
                ScoreNames.ACE -> numAces++
                ScoreNames.ALBATROSS -> numAlbatross++
                ScoreNames.EAGLE -> numEagles++
                ScoreNames.BIRDIE -> numBirdies++
                ScoreNames.PAR -> numPars++
                ScoreNames.BOGEY -> numBogeys++
                ScoreNames.DBOGEY -> numDoubleBogeys++
                else -> numTripleBogeysPlus++
            }

            val bestScore = if(oldStats.bestScore < score) oldStats.bestScore else score


            var worstScore = if(oldStats.worstScore < score) score else oldStats.worstScore


            return HoleStats(oldStats.timesPlayed + 1,
                    oldStats.totalStrokes + score,
                    (oldStats.totalStrokes + score).toDouble() / (oldStats.timesPlayed + 1).toDouble(),
                    oldStats.lifetimePlusMinus + (score - par),
                    bestScore, worstScore, numAlbatross, numAces, numEagles, numBirdies,
                    numPars, numBogeys, numDoubleBogeys, numTripleBogeysPlus)
        }
    }

}