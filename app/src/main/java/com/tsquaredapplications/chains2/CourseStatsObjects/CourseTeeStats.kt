package com.tsquaredapplications.chains2.CourseStatsObjects

import com.tsquaredapplications.chains2.Enums.ScoreNames
import com.tsquaredapplications.chains2.Objects.Scorecard
import com.tsquaredapplications.chains2.PlayerStatsObjects.HoleStats
import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerTeeStats
import com.tsquaredapplications.chains2.Utilities.GolfUtils

data class CourseTeeStats(
        var roundsPlayed: Int = 0,
        var totalStrokes: Int = 0,
        var lifetimePlusMinus: Int = 0,
        var bestScore: Int = 0,
        var topTenScore: MutableList<LeaderboardEntry> = mutableListOf(),
        var topTenAvgStrokes: MutableList<LeaderboardEntryDouble> = mutableListOf(),
        var topTenLPM: MutableList<LeaderboardEntry> = mutableListOf(),
        var topTenAces: MutableList<LeaderboardEntry> = mutableListOf(),
        var topTenEagles: MutableList<LeaderboardEntry> = mutableListOf(),
        var topTenBirdies: MutableList<LeaderboardEntry> = mutableListOf(),
        var topTenRoundsPlayed: MutableList<LeaderboardEntry> = mutableListOf(),
        var worstScore: Int = 0,
        var numAlbatross: Int = 0,
        var numAces: Int = 0,
        var numEagles: Int = 0,
        var numBirdies: Int = 0,
        var numPars: Int = 0,
        var numBogeys: Int = 0,
        var numDoubleBogeys: Int = 0,
        var numTripleBogeyPlus: Int = 0,
        var holeStats: MutableList<HoleStats> = mutableListOf()) {


    companion object {
        fun firstTime(scorecard: Scorecard, username: String, uid: String): CourseTeeStats {
            val numHoles = scorecard.getNumHoles()
            var totalStrokes = 0
            var aces = 0
            var albatross = 0
            var eagles = 0
            var birdies = 0
            var pars = 0
            var bogeys = 0
            var doubleBogeys = 0
            var tripleBogeysPlus = 0

            var holeStatsList: MutableList<HoleStats> = mutableListOf()

            for (i in 0 until numHoles) {
                totalStrokes += scorecard.getHoleScore(i)

                when (GolfUtils.determineScoreName(scorecard.getHoleScore(i), scorecard.getHolePar(i))) {
                    ScoreNames.ACE -> aces++
                    ScoreNames.ALBATROSS -> albatross++
                    ScoreNames.EAGLE -> eagles++
                    ScoreNames.BIRDIE -> birdies++
                    ScoreNames.PAR -> pars++
                    ScoreNames.BOGEY -> bogeys++
                    ScoreNames.DBOGEY -> doubleBogeys++
                    else -> tripleBogeysPlus++
                }

                holeStatsList.add(HoleStats.firstTime(scorecard.getHoleScore(i), scorecard.getHolePar(i)))
            }

            val bestScoreLeaderboardEntry = LeaderboardEntry(totalStrokes, uid, username)
            val bestScoreLeaderboard = LeaderboardEntry.buildDescendingLeaderboard(null, bestScoreLeaderboardEntry)

            val avgStrokesLeaderboardEntry = LeaderboardEntryDouble(totalStrokes.toDouble(), uid, username)
            val avgStrokesLeaderboard = LeaderboardEntryDouble.buildDescendingLeaderboard(null, avgStrokesLeaderboardEntry)

            val lpmLeaderboardEntry = LeaderboardEntry(scorecard.plusMinus, uid, username)
            val lpmLeaderboard = LeaderboardEntry.buildDescendingLeaderboard(null, lpmLeaderboardEntry)

            val acesLeaderboardEntry = LeaderboardEntry(aces, uid, username)
            val acesLeaderboard = LeaderboardEntry.buildAscendingLeaderboard(null, acesLeaderboardEntry)

            val eaglesLeaderboardEntry = LeaderboardEntry(eagles, uid, username)
            val eaglesLeaderboard = LeaderboardEntry.buildAscendingLeaderboard(null, eaglesLeaderboardEntry)

            val birdiesLeaderboardEntry = LeaderboardEntry(birdies, uid, username)
            val birdiesLeaderboard = LeaderboardEntry.buildAscendingLeaderboard(null, birdiesLeaderboardEntry)

            val roundsPlayedLeaderboardEntry = LeaderboardEntry(1, uid, username)
            val roundsPlayedLeaderboard  = LeaderboardEntry.buildAscendingLeaderboard(null, roundsPlayedLeaderboardEntry)


            return CourseTeeStats(1, totalStrokes, scorecard.plusMinus,
                    totalStrokes, bestScoreLeaderboard, avgStrokesLeaderboard,
                    lpmLeaderboard, acesLeaderboard, eaglesLeaderboard, birdiesLeaderboard,
                    roundsPlayedLeaderboard, totalStrokes, albatross, aces, eagles,
                    birdies, pars, bogeys, doubleBogeys, tripleBogeysPlus, holeStatsList)
        }


        fun mergeStats(scorecard: Scorecard, oldStats: CourseTeeStats, playerTeeStatsAfter: PlayerTeeStats, uid: String): CourseTeeStats {
            val username = playerTeeStatsAfter.username
            val numHoles = scorecard.getNumHoles()
            var totalStrokes = 0
            var aces = oldStats.numAces
            var albatross = oldStats.numAlbatross
            var eagles = oldStats.numEagles
            var birdies = oldStats.numBirdies
            var pars = oldStats.numPars
            var bogeys = oldStats.numBogeys
            var doubleBogeys = oldStats.numDoubleBogeys
            var tripleBogeysPlus = oldStats.numTripleBogeyPlus
            var newHoleStats: MutableList<HoleStats> = mutableListOf()

            for (i in 0 until numHoles) {
                totalStrokes += scorecard.getHoleScore(i)

                when (GolfUtils.determineScoreName(scorecard.getHoleScore(i), scorecard.getHolePar(i))) {
                    ScoreNames.ACE -> aces++
                    ScoreNames.ALBATROSS -> albatross++
                    ScoreNames.EAGLE -> eagles++
                    ScoreNames.BIRDIE -> birdies++
                    ScoreNames.PAR -> pars++
                    ScoreNames.BOGEY -> bogeys++
                    ScoreNames.DBOGEY -> doubleBogeys++
                    else -> tripleBogeysPlus++
                }

                newHoleStats.add(HoleStats.mergeStats(oldStats.holeStats[i],
                        scorecard.getHoleScore(i),
                        scorecard.getHolePar(i)))

            }
            val bestScore = if (oldStats.bestScore <= totalStrokes) oldStats.bestScore else totalStrokes
            val worstScore = if (oldStats.worstScore >= totalStrokes) oldStats.worstScore else totalStrokes

            val bestScoreLeaderboardEntry = LeaderboardEntry(totalStrokes, uid, username)
            val bestScoreLeaderboard = LeaderboardEntry.buildDescendingLeaderboard(
                    oldStats.topTenScore, bestScoreLeaderboardEntry)

            val avgStrokesLeaderboardEntry = LeaderboardEntryDouble(playerTeeStatsAfter.avgStrokes, uid, username)
            val avgStrokesLeaderboard = LeaderboardEntryDouble.buildDescendingLeaderboard(
                    oldStats.topTenAvgStrokes, avgStrokesLeaderboardEntry)

            val lpmLeaderboardEntry = LeaderboardEntry(playerTeeStatsAfter.lifetimePlusMinus, uid, username)
            val lpmLeaderboard = LeaderboardEntry.buildDescendingLeaderboard(
                    oldStats.topTenLPM, lpmLeaderboardEntry)

            val acesLeaderboardEntry = LeaderboardEntry(playerTeeStatsAfter.numAces, uid, username)
            val acesLeaderboard = LeaderboardEntry.buildAscendingLeaderboard(
                    oldStats.topTenAces, acesLeaderboardEntry)

            val eaglesLeaderboardEntry = LeaderboardEntry(playerTeeStatsAfter.numEagles, uid, username)
            val eaglesLeaderboard = LeaderboardEntry.buildAscendingLeaderboard(
                    oldStats.topTenEagles, eaglesLeaderboardEntry)

            val birdiesLeaderboardEntry = LeaderboardEntry(playerTeeStatsAfter.numBirdies, uid, username)
            val birdiesLeaderboard = LeaderboardEntry.buildAscendingLeaderboard(
                    oldStats.topTenBirdies, birdiesLeaderboardEntry)

            val roundsPlayedLeaderboardEntry = LeaderboardEntry(playerTeeStatsAfter.roundsPlayed, uid, username)
            val roundsPlayedLeaderboard  = LeaderboardEntry.buildAscendingLeaderboard(
                    oldStats.topTenRoundsPlayed, roundsPlayedLeaderboardEntry)

            return CourseTeeStats(oldStats.roundsPlayed + 1,
                    oldStats.totalStrokes + totalStrokes,
                    oldStats.lifetimePlusMinus + scorecard.plusMinus,
                    bestScore, bestScoreLeaderboard, avgStrokesLeaderboard,
                    lpmLeaderboard, acesLeaderboard, eaglesLeaderboard, birdiesLeaderboard,
                    roundsPlayedLeaderboard, worstScore, albatross, aces, eagles,
                    birdies, pars, bogeys, doubleBogeys, tripleBogeysPlus, newHoleStats)
        }

        fun getLeaderboardString(score: Int, username: String): String {
            return "$score $username"
        }

        fun getLeaderboardStringDouble(score: Double, username: String): String {
            return "$score $username"
        }

        fun getLeaderboardStringLpm(score: Int, username: String): String {
            if(score != 0)
                return "$score $username"
            else
                return "Even $username"
        }
    }
}