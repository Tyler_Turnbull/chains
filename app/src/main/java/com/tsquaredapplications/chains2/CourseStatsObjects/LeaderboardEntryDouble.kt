package com.tsquaredapplications.chains2.CourseStatsObjects

import android.os.Parcel
import android.os.Parcelable
import java.util.*

data class LeaderboardEntryDouble(val score: Double = 0.0, val uid: String? = null, val username: String = "default") : Parcelable {


    constructor(parcel: Parcel) : this(
            parcel.readDouble(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(score)
        parcel.writeString(uid)
        parcel.writeString(username)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LeaderboardEntryDouble> {
        override fun createFromParcel(parcel: Parcel): LeaderboardEntryDouble {
            return LeaderboardEntryDouble(parcel)
        }

        override fun newArray(size: Int): Array<LeaderboardEntryDouble?> {
            return arrayOfNulls(size)
        }

        // Used for sorting leaderboards that are descending, such as avg strokes
        fun buildDescendingLeaderboard(inputLeaderboard: MutableList<LeaderboardEntryDouble>?,
                                       newEntry: LeaderboardEntryDouble): MutableList<LeaderboardEntryDouble> {

            var userSeen = false
            if (inputLeaderboard == null) {
                // This would be the first round played at this course
                return mutableListOf(newEntry)
            } else {


                // Not first round played here
                for (entry in inputLeaderboard) {
                    if (entry.uid == newEntry.uid) {
                        // user already in leaderboard
                        // since avg strokes is a growing stat unlike best score, swap stat
                        inputLeaderboard.remove(entry)
                        inputLeaderboard.add(newEntry)
                        userSeen = true
                        break
                    }
                }
            }

            if (!userSeen) inputLeaderboard.add(newEntry)


            Collections.sort(inputLeaderboard, kotlin.Comparator { o1, o2 ->
                if (o1.score > o2.score) 1 else -1
            })

            if (inputLeaderboard.size > 10) inputLeaderboard.removeAt(inputLeaderboard.lastIndex)
            return inputLeaderboard
        }
    }
}
