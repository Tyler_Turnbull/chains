package com.tsquaredapplications.chains2.CourseStatsObjects

import android.os.Parcel
import android.os.Parcelable
import java.util.*


/**
 * LeaderboardEntry is used to contain entries in a leaderboard.
 * Each course/tee combo will have its own leaderboard.
 *
 * Its current implementation is to hold only the top ten scores for a course / tee
 * combo, however this can simply be changed by modifying the value inside of buildDescendingLeaderboard
 * function.
 */
data class LeaderboardEntry(val score: Int = 100, val uid: String? = null, val username: String = "default") : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(score)
        parcel.writeString(uid)
        parcel.writeString(username)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LeaderboardEntry> {
        override fun createFromParcel(parcel: Parcel): LeaderboardEntry {
            return LeaderboardEntry(parcel)
        }

        override fun newArray(size: Int): Array<LeaderboardEntry?> {
            return arrayOfNulls(size)
        }

        // Used for sorting leaderboards that are descending, such as best score
        fun buildDescendingLeaderboard(inputLeaderboard: MutableList<LeaderboardEntry>?,
                                       newEntry: LeaderboardEntry): MutableList<LeaderboardEntry> {

            if (inputLeaderboard == null) {
                // This would be the first round played at this course
                return mutableListOf(newEntry)
            } else {

                var newLeaderboard = mutableListOf<LeaderboardEntry>()
                for (entry in inputLeaderboard) {
                    newLeaderboard.add(entry)
                }
                // Not first round played here
                var userSeen = false
                for (entry in newLeaderboard) {
                    if (entry.uid == newEntry.uid) {
                        // user already in leaderboard
                        if (entry.score >= newEntry.score) {
                            // new score is better
                            newLeaderboard.remove(entry)
                            newLeaderboard.add(newEntry)
                            userSeen = true
                            break
                        } else {
                            userSeen = true
                            break
                        }
                    }
                }

                if (!userSeen) newLeaderboard.add(newEntry)


                Collections.sort(newLeaderboard, kotlin.Comparator { o1, o2 ->
                    if (o1.score > o2.score) 1 else -1
                })

                if (newLeaderboard.size > 10) newLeaderboard.removeAt(newLeaderboard.lastIndex)
                return newLeaderboard
            }
        }

        // Used for sorting ascending leaderboards such as numBirdies, or times played
        fun buildAscendingLeaderboard(inputLeaderboard: MutableList<LeaderboardEntry>?,
                                      newEntry: LeaderboardEntry): MutableList<LeaderboardEntry> {

            if (inputLeaderboard == null) {
                // This would be the first round played at this course
                return mutableListOf(newEntry)
            } else {
                // Not first round played here
                var newLeaderboard = mutableListOf<LeaderboardEntry>()
                for (entry in inputLeaderboard) {
                    newLeaderboard.add(entry)
                }

                var userSeen = false
                for (entry in newLeaderboard) {
                    if (entry.uid == newEntry.uid) {
                        // user already in leaderboard
                        if (entry.score <= newEntry.score) {
                            // new score is better
                            newLeaderboard.remove(entry)
                            newLeaderboard.add(newEntry)
                            userSeen = true
                            break
                        } else {
                            userSeen = true
                            break
                        }
                    }
                }

                if (!userSeen) newLeaderboard.add(newEntry)


                Collections.sort(newLeaderboard, kotlin.Comparator { o1, o2 ->
                    if (o1.score < o2.score) 1 else -1
                })

                if (newLeaderboard.size > 10) newLeaderboard.removeAt(newLeaderboard.lastIndex)
                return newLeaderboard
            }
        }
    }
}