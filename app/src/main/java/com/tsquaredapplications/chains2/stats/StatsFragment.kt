package com.tsquaredapplications.chains2.stats


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tsquaredapplications.chains2.MainActivity

import com.tsquaredapplications.chains2.R
import kotlinx.android.synthetic.main.fragment_stats.*


class StatsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_stats, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setClickListeners()
    }

    private fun setClickListeners() {
        user_stats_btn.setOnClickListener {
            (activity as MainActivity).swapFragment(UserStatsSelectionFragment(), true)
        }

        course_stats_btn.setOnClickListener {

        }

        friends_stats_btn.setOnClickListener {

        }


    }

}
