package com.tsquaredapplications.chains2.stats


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tsquaredapplications.chains2.MainActivity

import com.tsquaredapplications.chains2.R
import kotlinx.android.synthetic.main.fragment_user_stats_selection.*


class UserStatsSelectionFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_stats_selection, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setClickListeners()
    }

    private fun setClickListeners() {
        overall_btn.setOnClickListener {
            (activity as MainActivity).swapFragment(UserOverallStatsFragment(), true)
        }

        by_course_btn.setOnClickListener {

        }
    }


}
