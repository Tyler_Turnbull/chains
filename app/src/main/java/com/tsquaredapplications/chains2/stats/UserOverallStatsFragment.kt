package com.tsquaredapplications.chains2.stats


import android.arch.persistence.room.Room
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tsquaredapplications.chains2.Enums.RoomDbNames
import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerStats

import com.tsquaredapplications.chains2.R
import com.tsquaredapplications.chains2.RoomDB.PlayerOverallStatsDatabase
import com.tsquaredapplications.chains2.Utilities.GeneralUtils
import com.tsquaredapplications.chains2.Utilities.GolfUtils
import kotlinx.android.synthetic.main.fragment_user_overall_stats.*



class UserOverallStatsFragment : Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_overall_stats, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val db = Room.databaseBuilder(context!!,
                PlayerOverallStatsDatabase::class.java,
                RoomDbNames.USER_OVERALL_STATS_DATABASE.toString())
                .allowMainThreadQueries()
                .build()

        updateUI(db.playerOverallStatsDao().loadPlayerStats()[0])
    }

    private fun updateUI(stats: PlayerStats) {
        user_overall_stats_pb.visibility = View.GONE
        user_overall_stats_cl.visibility = View.VISIBLE
        user_overall_rounds_played_val_tv.text = stats.numRoundsPlayed.toString()
        user_overall_total_strokes_val_tv.text = stats.totalStrokes.toString()
        user_overall_lpm_val_tv.text = GolfUtils.formatScore(stats.lifetimePlusMinus)
        val avgPlusMinus = stats.lifetimePlusMinus.toDouble() / stats.numRoundsPlayed.toDouble()
        user_overall_avg_pm_val_tv.text = GolfUtils.formatScore(GeneralUtils.roundTwoPlaces(avgPlusMinus))
        user_overall_aces_val_tv.text = stats.numAces.toString()
        user_overall_albatross_val_tv.text = stats.numAlbatross.toString()
        user_overall_eagles_val_tv.text = stats.numEagles.toString()
        user_overall_birdies_val_tv.text = stats.numBirdies.toString()
        user_overall_pars_val_tv.text = stats.numPars.toString()
        user_overall_bogeys_val_tv.text = stats.numBogeys.toString()
        user_overall_dbl_bogeys_val_tv.text = stats.numDoubleBogeys.toString()
        user_overall_tri_bogey_val_tv.text = stats.numTripleBogeyPlus.toString()

    }
}
