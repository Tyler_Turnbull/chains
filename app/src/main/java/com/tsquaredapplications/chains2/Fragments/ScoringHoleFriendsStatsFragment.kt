package com.tsquaredapplications.chains2.Fragments


import android.arch.lifecycle.ViewModelProviders
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tsquaredapplications.chains2.R
import com.tsquaredapplications.chains2.SpScoringActivity
import com.tsquaredapplications.chains2.SpScoringViewModel
import com.tsquaredapplications.chains2.Utilities.GeneralUtils
import kotlinx.android.synthetic.main.fragment_scoring_friends_hole_stats.*


class ScoringHoleFriendsStatsFragment : Fragment() {

    private val viewModel by lazy { ViewModelProviders.of(activity!!).get(SpScoringViewModel::class.java) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_scoring_friends_hole_stats, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val activity = activity as SpScoringActivity

        if (!viewModel.isNetworkAvailable) {
            displayNoConnectionUI()
        } else if (viewModel.friendsList.isEmpty()) {
            displayNoFriendsUI()
        } else if (viewModel.friendsStatList.isEmpty()) {
            displyNoFriendsPlayedUI()
        } else {
            displayFriendsStatsUI()
        }
    }

    fun displayFriendsStatsUI() {
        val holeNumber = viewModel.scorecard.currentHole
        var displayList = mutableListOf<String>()
        var userIndex = 0
        var userDisplayed = false

        var statsList = viewModel.friendsStatList
        if(viewModel.playerTeeStats != null)
            statsList.add(viewModel.playerTeeStats!!)


        val numberToDisplay = if (statsList.size > 3) 3 else statsList.size

        // Avg Strokes
        var views = mutableListOf<TextView>(scoring_friend_stats_avg_stroke_1_text_view,
                scoring_friend_stats_avg_stroke_2_text_view,
                scoring_friend_stats_avg_stroke_3_text_view,
                scoring_friend_stats_avg_strokes_user_text_view)

        statsList.sortBy { it.holeStats[holeNumber].avgStrokes }
        for (i in 0 until numberToDisplay) {
            if (statsList[i].username == viewModel.playerTeeStats?.username) {
                displayList.add("${GeneralUtils.roundTwoPlacesString(statsList[i].holeStats[holeNumber].avgStrokes)} You")
                userDisplayed = true
                userIndex = i
            } else {
                // This is a friend
                displayList.add(("${GeneralUtils.roundTwoPlacesString(statsList[i].holeStats[holeNumber].avgStrokes)} " +
                        statsList[i].username))

            }
        }

        if (!userDisplayed && viewModel.playerTeeStats != null) {
            displayList.add("${GeneralUtils.roundTwoPlacesString(viewModel.playerTeeStats!!.holeStats[holeNumber].avgStrokes)} You")
            userIndex = 3
        }

        populateList(views, displayList, userIndex)

        // Lifetime Plus Minus
        views = mutableListOf(scoring_friend_stats_bpm_1_text_view,
                scoring_friend_stats_bpm_2_text_view,
                scoring_friend_stats_bpm_3_text_view,
                scoring_friend_stats_bpm_user_text_view)

        statsList.sortBy { it.holeStats[holeNumber].lifetimePlusMinus }
        displayList = mutableListOf()
        userDisplayed = false
        for (i in 0 until numberToDisplay) {
            val lpm = statsList[i].holeStats[holeNumber].lifetimePlusMinus
            if (statsList[i].username == viewModel.playerTeeStats?.username) {
                // this is the user
                if (lpm > 0)
                    displayList.add("+" + Integer.toString(lpm) + " You")
                else if (lpm == 0)
                    displayList.add("Even You")
                else
                    displayList.add(Integer.toString(lpm) + " You")


                userDisplayed = true
                userIndex = i
            } else {
                // this is a friend
                if (lpm > 0) {
                    displayList.add("+${Integer.toString(lpm)} ${statsList[i].username}")
                } else if (lpm == 0) {
                    displayList.add("Even ${statsList[i].username}")
                } else {
                    displayList.add("${Integer.toString(lpm)} ${statsList[i].username}")
                }

            }
        }

        if (!userDisplayed && viewModel.playerTeeStats != null) {
            val lpm = viewModel.playerTeeStats!!.lifetimePlusMinus
            if (lpm > 0)
                displayList.add("+${Integer.toString(viewModel.playerTeeStats!!.holeStats[holeNumber].lifetimePlusMinus)} You")
            else if (lpm == 0)
                displayList.add("Even You")
            else
                displayList.add("${Integer.toString(viewModel.playerTeeStats!!.holeStats[holeNumber].lifetimePlusMinus)} You")
            userIndex = 3
        }

        populateList(views, displayList, userIndex)

        // Best Scores
        views = mutableListOf(scoring_friend_stats_best_score_1_text_view,
                scoring_friend_stats_best_score_2_text_view,
                scoring_friend_stats_best_score_3_text_view,
                scoring_friend_stats_best_score_user_text_view)

        statsList.sortBy { it.holeStats[holeNumber].bestScore }
        displayList = mutableListOf()
        userDisplayed = false
        for (i in 0 until numberToDisplay) {
            if (statsList[i].username == viewModel.playerTeeStats?.username) {
                // this is the user
                displayList.add(Integer.toString(statsList[i]
                        .holeStats[holeNumber].bestScore) + " You")

                userDisplayed = true
                userIndex = i
            } else {
                // this is a friend
                displayList.add(Integer.toString(statsList[i]
                        .holeStats[holeNumber].bestScore) +
                        " " + statsList[i].username)


            }
        }

        if (!userDisplayed && viewModel.playerTeeStats != null) {
            displayList.add("${Integer.toString(viewModel.playerTeeStats!!.holeStats[holeNumber].bestScore)} You")
            userIndex = 3
        }

        populateList(views, displayList, userIndex)


        // Worst Scores
        views = mutableListOf(scoring_friend_stats_worst_score_1_text_view,
                scoring_friend_stats_worst_score_2_text_view,
                scoring_friend_stats_worst_scrore_3_text_view,
                scoring_friend_stats_worst_score_user_text_view)

        statsList.sortBy { it.holeStats[holeNumber].worstScore }
        statsList.reverse()
        displayList = mutableListOf()
        userDisplayed = false
        for (i in 0 until numberToDisplay) {
            if (statsList[i].username == viewModel.playerTeeStats?.username) {
                // this is the user
                displayList.add(Integer.toString(statsList[i]
                        .holeStats[holeNumber].worstScore) + " You")

                userDisplayed = true
                userIndex = i
            } else {
                // this is a friend
                displayList.add(Integer.toString(statsList[i]
                        .holeStats[holeNumber].worstScore) +
                        " " + statsList[i].username)


            }
        }

        if (!userDisplayed && viewModel.playerTeeStats != null) {
            displayList.add("${Integer.toString(viewModel.playerTeeStats!!.holeStats[holeNumber].worstScore)} You")
            userIndex = 3
        }

        populateList(views, displayList, userIndex)


        // Times Played
        views = mutableListOf(scoring_friend_stats_times_played_1_text_view,
                scoring_friend_stats_times_played_2_text_view,
                scoring_friend_stats_times_played_3_text_view,
                scoring_friend_stats_times_played_user_text_view)

        statsList.sortBy { it.holeStats[holeNumber].timesPlayed }
        statsList.reverse()
        displayList = mutableListOf()
        userDisplayed = false
        for (i in 0 until numberToDisplay) {
            if (statsList[i].username == viewModel.playerTeeStats?.username) {
                // this is the user
                displayList.add(Integer.toString(statsList[i]
                        .holeStats[holeNumber].timesPlayed) + " You")

                userDisplayed = true
                userIndex = i
            } else {
                // this is a friend
                displayList.add(Integer.toString(statsList[i]
                        .holeStats[holeNumber].timesPlayed) +
                        " " + statsList[i].username)


            }
        }

        if (!userDisplayed && viewModel.playerTeeStats != null) {
            displayList.add("${Integer.toString(viewModel.playerTeeStats!!.holeStats[holeNumber].timesPlayed)} You")
            userIndex = 3
        }

        populateList(views, displayList, userIndex)

        // Birdie Count
        views = mutableListOf(scoring_friend_stats_birdies_1_text_view,
                scoring_friend_stats_birdies_2_text_view,
                scoring_friend_stats_birdies_3_text_view,
                scoring_friend_stats_birdie_count_user_text_view)

        statsList.sortBy { it.holeStats[holeNumber].numBirdies }
        statsList.reverse()
        displayList = mutableListOf()
        userDisplayed = false
        for (i in 0 until numberToDisplay) {
            if (statsList[i].username == viewModel.playerTeeStats?.username) {
                // this is the user
                displayList.add(Integer.toString(statsList[i]
                        .holeStats[holeNumber].numBirdies) + " You")

                userDisplayed = true
                userIndex = i
            } else {
                // this is a friend
                displayList.add(Integer.toString(statsList[i]
                        .holeStats[holeNumber].numBirdies) +
                        " " + statsList[i].username)


            }
        }

        if (!userDisplayed && viewModel.playerTeeStats != null) {
            displayList.add("${Integer.toString(viewModel.playerTeeStats!!.holeStats[holeNumber].numBirdies)} You")
            userIndex = 3
        }

        populateList(views, displayList, userIndex)

        scoring_friend_stats_scroll_view.visibility = View.VISIBLE

    }

    fun populateList(views: MutableList<TextView>, textList: MutableList<String>, userIndex: Int) {
        for (i in 0..3) {
            if (i < textList.size) {
                if (i == userIndex) {
                    views[i].text = textList[i]
                    views[i].setTypeface(null, Typeface.BOLD)
                } else {
                    views[i].text = textList[i]
                }
            } else {
                // noone left to display
                views[i].visibility = View.INVISIBLE
            }
        }
    }

    fun displayNoFriendsUI() {
        scoring_friend_stats_no_stats_message.text = getString(R.string.no_friends_message)
        scoring_friend_stats_no_stats_message.visibility = View.VISIBLE
    }

    fun displyNoFriendsPlayedUI() {
        scoring_friend_stats_no_stats_message.text = getString(R.string.no_friends_have_played_message)
        scoring_friend_stats_no_stats_message.visibility = View.VISIBLE
    }

    fun displayNoConnectionUI() {
        scoring_friend_stats_no_stats_message.text = getString(R.string.no_connection_message)
        scoring_friend_stats_no_stats_message.visibility = View.VISIBLE
    }


}
