package com.tsquaredapplications.chains2.Fragments


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tsquaredapplications.chains2.R
import com.tsquaredapplications.chains2.Utilities.GeneralUtils
import com.tsquaredapplications.chains2.Utilities.ProfilePicUtil
import kotlinx.android.synthetic.main.fragment_profile.*


class ProfileFragment : Fragment() {

    lateinit var callbackListener: EditProfileClickListener

    interface EditProfileClickListener {
        fun onClickEditProfile()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        username_text_view.text = GeneralUtils.getUsername(context!!)
        edit_profile_button.setOnClickListener { callbackListener.onClickEditProfile() }
        val bitmap = ProfilePicUtil.loadPhotoFromInternalStorage(context!!)
        if (bitmap != null)
            profile_image_view.setImageBitmap(bitmap)

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            callbackListener = activity as EditProfileClickListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString() + " must implement EditProfileClickListener")
        }
    }

}
