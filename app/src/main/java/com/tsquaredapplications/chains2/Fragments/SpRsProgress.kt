package com.tsquaredapplications.chains2.Fragments


import android.arch.lifecycle.ViewModelProviders
import android.graphics.Color
import android.os.Bundle
import android.support.constraint.ConstraintSet
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerTeeStats
import com.tsquaredapplications.chains2.R
import com.tsquaredapplications.chains2.SpRsViewModel
import com.tsquaredapplications.chains2.Utilities.GeneralUtils
import com.tsquaredapplications.chains2.Utilities.GolfUtils
import kotlinx.android.synthetic.main.fragment_rs_progress.*


class SpRsProgress : Fragment() {


    lateinit var viewModel: SpRsViewModel
    val green = "#3dc666"
    val red = "#c63d3d"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        viewModel = ViewModelProviders.of(activity!!).get(SpRsViewModel::class.java)
       return inflater.inflate(R.layout.fragment_rs_progress, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        if(viewModel.playerTeeStatsAfter != null && viewModel.playerTeeStatsBefore != null)
            updateUI(viewModel.playerTeeStatsBefore!!, viewModel.playerTeeStatsAfter!!)

    }

    fun updateUI(statsBefore: PlayerTeeStats,statsAfter: PlayerTeeStats){

        // Best Score
        sprs_progress_best_before_text_view.text = "${statsBefore.bestScore}"
        sprs_progress_best_after_text_view.text = "${statsAfter.bestScore}"
        sprs_progress_best_diff_text_view.text = "${statsBefore.bestScore - statsAfter.bestScore}"
        if(statsAfter.bestScore < statsBefore.bestScore){
            // new best score
            sprs_progress_best_diff_text_view.setTextColor(Color.parseColor(green))
        }


        // Worst Score
        sprs_progress_worst_before_text_view.text = "${statsBefore.worstScore}"
        sprs_progress_worst_after_text_view.text = "${statsAfter.worstScore}"
        val worstDiff = statsAfter.worstScore - statsBefore.worstScore
        val worstDiffString: String
        when(worstDiff > 0){
            true -> worstDiffString = "+$worstDiff"
            false -> worstDiffString = "$worstDiff"
        }

        sprs_progress_worst_diff_header.text = worstDiffString

        if(statsAfter.worstScore > statsBefore.worstScore){
            // new worst score
            sprs_progress_worst_diff_header.setTextColor(Color.parseColor(red))
        }

        // Avg Strokes
        sprs_progress_avg_strokes_before_text_view.text = GeneralUtils.roundTwoPlacesString(statsBefore.avgStrokes)
        sprs_progress_avg_strokes_after_text_view.text = GeneralUtils.roundTwoPlacesString(statsAfter.avgStrokes)
        val avgDiff = statsAfter.avgStrokes - statsBefore.avgStrokes
        val avgDiffString: String
        if(avgDiff > 0) avgDiffString = "+${GeneralUtils.roundTwoPlacesString(avgDiff)}"
        else avgDiffString = GeneralUtils.roundTwoPlacesString(avgDiff)

        sprs_progress_avg_strokes_diff_text_view.text = avgDiffString

        if(statsAfter.avgStrokes < statsBefore.avgStrokes){
            // avg strokes improved
            sprs_progress_avg_strokes_diff_text_view.setTextColor(Color.parseColor(green))
        } else if (statsAfter.avgStrokes > statsBefore.avgStrokes){
            // avg strokes got worse
            sprs_progress_avg_strokes_diff_text_view.setTextColor(Color.RED)
        }


        // Total Strokes
        sprs_progress_total_strokes_before_text_view.text = statsBefore.totalStrokes.toString()
        sprs_progress_total_strokes_after_text_view.text = statsAfter.totalStrokes.toString()
        val totalStrokeDiff = "+${statsAfter.totalStrokes - statsBefore.totalStrokes}"
        sprs_progress_total_strokes_diff_text_view.text = totalStrokeDiff


        // LPM
        sprs_progress_lpm_before_text_view.text = GolfUtils.formatScore(statsBefore.lifetimePlusMinus)
        sprs_progress_lpm_after_text_view.text = GolfUtils.formatScore(statsAfter.lifetimePlusMinus)
        val lpmDiff = statsAfter.lifetimePlusMinus - statsBefore.lifetimePlusMinus
        var lpmDiffString: String
        if(lpmDiff > 0)  {
            lpmDiffString = "+$lpmDiff"
            sprs_progress_lpm_diff_text_view.setTextColor(Color.parseColor(red))
        }
        else {
            lpmDiffString = "$lpmDiff"
            sprs_progress_lpm_diff_text_view.setTextColor(Color.parseColor(green))
        }
        sprs_progress_lpm_diff_text_view.text = lpmDiffString

        // Aces
        sprs_progress_aces_before_text_view.text = "${statsBefore.numAces}"
        sprs_progress_laces_after_text_view.text = "${statsAfter.numAces}"
        if(statsAfter.numAces > statsBefore.numAces){
            sprs_progress_aces_diff_text_view.setTextColor(Color.GREEN)
            val aceDiff = "+${statsAfter.numAces - statsBefore.numAces}"
            sprs_progress_aces_diff_text_view.text = aceDiff
        } else{
            sprs_progress_aces_diff_text_view.text = "0"
        }

        if(statsAfter.numAlbatross == 0){
            // none, just remove albatross row
            sprs_progress_albatross_after_text_view.visibility = View.GONE
            sprs_progress_albatross_before_text_view.visibility = View.GONE
            sprs_progress_albatross_diff_text_view.visibility = View.GONE
            sprs_progress_albatross_header_text_view.visibility = View.GONE

            val constraintSet = ConstraintSet()
            constraintSet.clone(sprs_progress_constraint_layout)
            constraintSet.connect(sprs_progress_eagles_header_text_view.id, ConstraintSet.TOP,
                    sprs_progress_aces_header_text_view.id, ConstraintSet.BOTTOM)

            constraintSet.applyTo(sprs_progress_constraint_layout)
        }else {
            sprs_progress_albatross_before_text_view.text = "${statsBefore.numAlbatross}"
            sprs_progress_albatross_after_text_view.text = "${statsAfter.numAlbatross}"
            val albaDiff = statsAfter.numAlbatross - statsBefore.numAlbatross
            sprs_progress_albatross_diff_text_view.text = "+$albaDiff"
            if(albaDiff > 0){
                sprs_progress_albatross_diff_text_view.setTextColor(Color.parseColor(green))
            }
        }


        // Eagles
        sprs_progress_eagles_before_text_view.text = "${statsBefore.numEagles}"
        sprs_progress_eagles_after_text_view.text = "${statsAfter.numEagles}"
        if(statsAfter.numEagles > statsBefore.numEagles){
            sprs_progress_eagles_diff_text_view.setTextColor(Color.parseColor(green))
            val eaglesDiff = "+${statsAfter.numEagles - statsBefore.numEagles}"
            sprs_progress_eagles_diff_text_view.text = eaglesDiff
        } else{
            sprs_progress_eagles_diff_text_view.text = "0"
        }


        // Birdies
        sprs_progress_birdies_before_text_view.text = "${statsBefore.numBirdies}"
        sprs_progress_birdies_after_text_view.text = "${statsAfter.numBirdies}"
        if(statsAfter.numBirdies > statsBefore.numBirdies){
            sprs_progress_birdies_diff_text_view.setTextColor(Color.parseColor(green))
            val birdDiff = "+${statsAfter.numBirdies - statsBefore.numBirdies}"
            sprs_progress_birdies_diff_text_view.text = birdDiff
        } else{
            sprs_progress_birdies_diff_text_view.text = "0"
        }


        // pars
        sprs_progress_pars_before_text_view.text = "${statsBefore.numPars}"
        sprs_progress_pars_after_text_view.text = "${statsAfter.numPars}"
        if(statsAfter.numPars > statsBefore.numPars){
            val parDiff = "+${statsAfter.numPars - statsBefore.numPars}"
            sprs_progress_pars_diff_text_view.text = parDiff
        } else{
            sprs_progress_pars_diff_text_view.text = "0"
        }


        // bogeys
        sprs_progress_bogeys_before_text_view.text = "${statsBefore.numBogeys}"
        sprs_progress_bogeys_after_text_view.text = "${statsAfter.numBogeys}"
        if(statsAfter.numBogeys > statsBefore.numBogeys){
            sprs_progress_bogeys_diff_text_view.setTextColor(Color.parseColor(red))
            val bogeyDiff = "+${statsAfter.numBogeys - statsBefore.numBogeys}"
            sprs_progress_bogeys_diff_text_view.text = bogeyDiff
        } else{
            sprs_progress_bogeys_diff_text_view.text = "0"
        }


        // 2x bogeys
        sprs_progress_2x_bogey_before_text_view.text = "${statsBefore.numDoubleBogeys}"
        sprs_progress_2x_bogey_after_text_view.text = "${statsAfter.numDoubleBogeys}"
        if(statsAfter.numDoubleBogeys > statsBefore.numDoubleBogeys){
            sprs_progress_2x_bogeys_diff_text_view.setTextColor(Color.parseColor(red))
            val dblBogeyDiff = "+${statsAfter.numDoubleBogeys - statsBefore.numDoubleBogeys}"
            sprs_progress_2x_bogeys_diff_text_view.text = dblBogeyDiff
        } else{
            sprs_progress_2x_bogeys_diff_text_view.text = "0"
        }

        // 3x bogeys
        sprs_progress_3x_bogey_before_text_view.text = "${statsBefore.numTripleBogeyPlus}"
        sprs_progress_3x_bogey_after_text_view.text = "${statsAfter.numTripleBogeyPlus}"
        if(statsAfter.numTripleBogeyPlus > statsBefore.numTripleBogeyPlus){
            sprs_progress_3x_bogeys_diff_text_view.setTextColor(Color.parseColor(red))
            val triBogeyDiff = "+${statsAfter.numTripleBogeyPlus - statsBefore.numTripleBogeyPlus}"
            sprs_progress_3x_bogeys_diff_text_view.text = triBogeyDiff
        } else{
            sprs_progress_3x_bogeys_diff_text_view.text = "0"
        }

    }
}
