package com.tsquaredapplications.chains2.Fragments


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tsquaredapplications.chains2.Enums.GameMode
import com.tsquaredapplications.chains2.R
import kotlinx.android.synthetic.main.fragment_game_style_selection.*


/**
 * A simple [Fragment] subclass.
 *
 */
class GameModeSelectionFragment : Fragment() {

    lateinit var mCallback: OnGameModeSelectedListener

    public interface OnGameModeSelectedListener {
        fun onModeSelected(mode: GameMode)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.fragment_game_style_selection,
                container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setClickListeners()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        try{
          mCallback = activity as OnGameModeSelectedListener
        } catch (e: ClassCastException){
           throw ClassCastException(context.toString() +
                    "Must implement OnGameModeSelectedListener")
        }
    }

    private fun setClickListeners(){
        single_player_button.setOnClickListener {
            mCallback.onModeSelected(GameMode.Single)
        }

        multi_player_button.setOnClickListener {
            mCallback.onModeSelected(GameMode.Multi)
        }
    }
}
