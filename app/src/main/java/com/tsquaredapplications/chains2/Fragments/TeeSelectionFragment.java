package com.tsquaredapplications.chains2.Fragments;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tsquaredapplications.chains2.Enums.IntentExtraNames;

import com.tsquaredapplications.chains2.Objects.Course;
import com.tsquaredapplications.chains2.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class TeeSelectionFragment extends Fragment {

    private static final String LOG_TAG = TeeSelectionFragment.class.getName();

    @BindView(R.id.tee_one_button)
    Button teeOneButton;

    @BindView(R.id.tee_two_button)
    Button teeTwoButton;

    @BindView(R.id.tee_three_button)
    Button teeThreeButton;

    @BindView(R.id.tee_four_button)
    Button teeFourButton;

    private Course mCourse;

    onTeeSelectedListener mCallback;

    public interface onTeeSelectedListener {
         void onTeeSelected(int teeChoice);
    }

    public TeeSelectionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tee_selection, container, false);
        // Inflate the layout for this fragment



        ButterKnife.bind(this, view);

        Bundle bundle = getArguments();
        mCourse = bundle.getParcelable(IntentExtraNames.COURSE.toString());
        setupButtons();
        return view;
    }

    private void setupButtons(){
        if(mCourse.getTeeOneExists()){
            teeOneButton.setVisibility(View.VISIBLE);
            teeOneButton.setBackgroundColor(Color.parseColor(mCourse.getTeeOneColor()));
            teeOneButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.onTeeSelected(1);
                }
            });
        }

        if(mCourse.getTeeTwoExists()){
            teeTwoButton.setVisibility(View.VISIBLE);
            teeTwoButton.setBackgroundColor(Color.parseColor(mCourse.getTeeTwoColor()));
            teeTwoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.onTeeSelected(2);
                }
            });
        }

        if(mCourse.getTeeThreeExists()){
            teeThreeButton.setVisibility(View.VISIBLE);
            teeThreeButton.setBackgroundColor(Color.parseColor(mCourse.getTeeThreeColor()));
            teeThreeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.onTeeSelected(3);
                }
            });
        }

        if(mCourse.getTeeFourExists()){
            teeFourButton.setVisibility(View.VISIBLE);
            teeFourButton.setBackgroundColor(Color.parseColor(mCourse.getTeeFourColor()));
            teeFourButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.onTeeSelected(4);
                }
            });
        }


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (onTeeSelectedListener)context;
        } catch (ClassCastException e){
            throw new ClassCastException(context.toString() +
                    "Must implement OnTeeSelectedListener");
        }
    }
}
