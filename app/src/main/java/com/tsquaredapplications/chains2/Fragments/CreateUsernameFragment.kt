package com.tsquaredapplications.chains2.Fragments


import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.tsquaredapplications.chains2.Enums.FirebaseDbNames
import com.tsquaredapplications.chains2.Enums.PreferenceNames
import com.tsquaredapplications.chains2.MainActivity

import com.tsquaredapplications.chains2.R
import com.tsquaredapplications.chains2.Utilities.UsernameUtils
import kotlinx.android.synthetic.main.fragment_create_username.*


/**
 * A simple [Fragment] subclass.
 */
class CreateUsernameFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater!!.inflate(R.layout.fragment_create_username, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button_submit.setOnClickListener {
            val username: String = username_edit_text.text.toString()

            if(!UsernameUtils.isValidUsername(username)){
                username_edit_text.setError("Username can only contain A-Z, a-z, 0-9, -, and _")

            } else if(UsernameUtils.isUsernameToLong(username)){
                Toast.makeText(context, "Username is too long, can only be 20 characters long",
                        Toast.LENGTH_LONG).show()
            } else if(UsernameUtils.isUsernameToShort(username)){
                Toast.makeText(context, "Username is too short, must be at least 3 characters long",
                        Toast.LENGTH_LONG).show()
            } else {
                // Check that username is not taken
                 FirebaseDatabase.getInstance().getReference()
                        .child(FirebaseDbNames.USER_DIRECTORY.toString())
                        .child(username)
                         .addListenerForSingleValueEvent(object: ValueEventListener{
                             override fun onCancelled(p0: DatabaseError?) {

                             }

                             override fun onDataChange(datasnapshot: DataSnapshot?) {
                                    datasnapshot?.let {
                                        if(it.exists()){
                                            // Username is taken
                                            Toast.makeText(context, "Username already taken",
                                                    Toast.LENGTH_LONG).show()
                                        } else {
                                            val prefEditor = PreferenceManager.getDefaultSharedPreferences(context)
                                                    .edit()

                                            prefEditor.apply {
                                                putBoolean(PreferenceNames.HAS_USERNAME.toString(), true)
                                            }

                                            prefEditor.apply {
                                                putString(PreferenceNames.USERNAME.toString(), username)
                                            }

                                            prefEditor.apply()


                                            // Update Firebase
                                            FirebaseDatabase.getInstance().getReference()
                                                    .child(FirebaseDbNames.USER_DIRECTORY.toString())
                                                    .child(username)
                                                    .setValue(FirebaseAuth.getInstance().uid)

                                            FirebaseDatabase.getInstance().getReference()
                                                    .child(FirebaseDbNames.USERNAMES.toString())
                                                    .child(FirebaseAuth.getInstance().uid)
                                                    .setValue(username)

                                            startActivity(Intent(context, MainActivity::class.java))
                                        }
                                    }
                             }

                         })


            }
        }
    }
}
