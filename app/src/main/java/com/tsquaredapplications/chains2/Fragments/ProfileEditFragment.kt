package com.tsquaredapplications.chains2.Fragments


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tsquaredapplications.chains2.Enums.PreferenceNames

import com.tsquaredapplications.chains2.R
import com.tsquaredapplications.chains2.Utilities.FirebaseProfilePicUploadUtil
import com.tsquaredapplications.chains2.Utilities.FirebaseUtils
import com.tsquaredapplications.chains2.Utilities.GeneralUtils
import com.tsquaredapplications.chains2.Utilities.ProfilePicUtil
import kotlinx.android.synthetic.main.fragment_profile_edit.*
import org.jetbrains.anko.toast
import java.io.File
import java.io.IOException


class ProfileEditFragment : Fragment() {

    val PICK_IMAGE_REQUEST_CODE = 2
    lateinit var filePath: Uri

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        username_edit_text.setText(GeneralUtils.getUsername(context!!))
        erase_stats_button.setOnClickListener { FirebaseUtils.eraseUserStats() }
        change_picture_button.setOnClickListener { chooseImage() }
        val bitmap = ProfilePicUtil.loadPhotoFromInternalStorage(context!!)
        if (bitmap != null)
            profile_image_view.setImageBitmap(bitmap)
    }

    fun chooseImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture)), PICK_IMAGE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST_CODE && data != null && data.data != null) {
            filePath = data.data

            val photoUploader = FirebasePhotoUploader()

            photoUploader.uploadPhoto(filePath, context!!)

        }

    }

    inner class FirebasePhotoUploader : FirebaseProfilePicUploadUtil() {
        override fun onPhotoUploadSuccess() {
            photo_upload_progress_bar.visibility = View.INVISIBLE
            activity?.toast("Photo Uploaded")

            try {
                val bitmap = MediaStore.Images.Media.getBitmap(activity?.contentResolver, filePath)
                // display on screen
                profile_image_view.setImageBitmap(bitmap)


                // save image to storage
                val imagePath = ProfilePicUtil.savePhoto(context!!, bitmap)
                val prefs = PreferenceManager.getDefaultSharedPreferences(context).edit()
                prefs.putString(PreferenceNames.PROFILE_PICTURE.toString(), imagePath).apply()


            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        override fun onPhotoUploadFailure() {
            activity?.toast("Problem uploading file, try again")
        }

        override fun photoUploadProgress(progress: Double) {
            if (photo_upload_progress_bar.visibility == View.INVISIBLE)
                photo_upload_progress_bar.visibility = View.VISIBLE

            photo_upload_progress_bar.progress = progress.toInt()
        }

    }

}
