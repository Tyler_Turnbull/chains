package com.tsquaredapplications.chains2.Fragments

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.tsquaredapplications.chains2.R
import com.tsquaredapplications.chains2.SpScoringViewModel
import com.tsquaredapplications.chains2.Utilities.GeneralUtils
import com.tsquaredapplications.chains2.Utilities.GolfUtils
import kotlinx.android.synthetic.main.fragment_scoring_stats.*



/**
 * A simple [Fragment] subclass.
 */


class ScoringHoleStatsFragment : Fragment() {



    private val viewModel by lazy { ViewModelProviders.of(activity!!).get(SpScoringViewModel::class.java) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_scoring_stats, container, false)

    }

    override fun onResume() {
        super.onResume()
        Log.i(this.toString(), "HI")
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        if (!viewModel.isFirstTime) {
            stats_layout.visibility = View.VISIBLE
            val holeNumber = viewModel.scorecard.currentHole
            val par = viewModel.scorecard.getHolePar(holeNumber)

            updateUI(holeNumber)

        } else {
            firstTimePlayingDisplay()
        }

    }


    fun firstTimePlayingDisplay() {
        no_stats_text_view.visibility = View.VISIBLE
    }

    fun updateUI(holeNumber: Int) {
        viewModel.playerTeeStats.let {
            if (it != null) {
                var text = getString(R.string.hole) + " " + (holeNumber + 1)
                hole_number_text_view.text = text

                text = getString(R.string.par) + " " + viewModel.scorecard.getHolePar(holeNumber)
                hole_par_text_view.text = text

                scoring_lpm_text_view.text = GolfUtils.formatScore(it.holeStats[holeNumber].lifetimePlusMinus)
                scoring_times_played_text_view.text = "${it.roundsPlayed}"
                scoring_best_text_view.text = "${it.holeStats[holeNumber].bestScore}"
                scoring_worst_text_view.text = "${it.holeStats[holeNumber].worstScore}"
                scoring_total_strokes_text_view.text = "${it.holeStats[holeNumber].totalStrokes}"
                scoring_avg_strokes_text_view.text = "${GeneralUtils.roundTwoPlacesString(it.holeStats[holeNumber].avgStrokes)}"
                scoring_aces_text_view.text = "${it.holeStats[holeNumber].numAces}"

                if (viewModel.scorecard.getHolePar(holeNumber) < 5) {
                    scoring_albatross_header_text_view.visibility = View.GONE
                    scoring_albatross_text_view.visibility = View.GONE
                } else {
                    scoring_albatross_text_view.text = "${it.holeStats[holeNumber].numAlbatross}"
                }

                scoring_eagles_text_view.text = "${it.holeStats[holeNumber].numEagles}"
                scoring_birdies_text_view.text = "${it.holeStats[holeNumber].numBirdies}"
                scoring_pars_text_view.text = "${it.holeStats[holeNumber].numPars}"
                scoring_bogeys_text_view.text = "${it.holeStats[holeNumber].numBogeys}"
                scoring_dbl_bogeys_text_view.text = "${it.holeStats[holeNumber].numDoubleBogeys}"
                scoring_tri_bogeys_text_view.text = "${it.holeStats[holeNumber].numTripleBogeysPlus}"
            }
        }
    }
}

