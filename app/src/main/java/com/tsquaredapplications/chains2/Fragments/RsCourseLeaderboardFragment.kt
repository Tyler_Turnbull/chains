package com.tsquaredapplications.chains2.Fragments


import android.arch.lifecycle.ViewModelProviders
import android.graphics.Typeface
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import com.tsquaredapplications.chains2.CourseStatsObjects.CourseTeeStats
import com.tsquaredapplications.chains2.CourseStatsObjects.LeaderboardEntry
import com.tsquaredapplications.chains2.CourseStatsObjects.LeaderboardEntryDouble
import com.tsquaredapplications.chains2.Enums.PreferenceNames
import com.tsquaredapplications.chains2.R
import com.tsquaredapplications.chains2.SpRsActivity
import com.tsquaredapplications.chains2.SpRsViewModel
import com.tsquaredapplications.chains2.Utilities.GeneralUtils
import com.tsquaredapplications.chains2.Utilities.GolfUtils
import kotlinx.android.synthetic.main.fragment_rs_course_leaderboard.*

class RsCourseLeaderboardFragment : Fragment(), AdapterView.OnItemSelectedListener {

    lateinit var viewModel: SpRsViewModel
    var leaderboardList = mutableListOf<LeaderboardEntry>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        viewModel = ViewModelProviders.of(activity!!).get(SpRsViewModel::class.java)
        return inflater.inflate(R.layout.fragment_rs_course_leaderboard,
                container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (viewModel.courseTeeStatsAfter != null)
            leaderboardList = viewModel.courseTeeStatsAfter!!.topTenScore
        if (!viewModel.isNetworkAvailable)
            displayNoConnectionUI()
        else if (leaderboardList.isEmpty())
            displayEmptyLeaderboardUI()
        else {
            displayLeaderboardUI(leaderboardList, viewModel.playerTeeStatsAfter!!.bestScore)
            val adapter = ArrayAdapter.createFromResource(context,
                    R.array.leaderboard_spinner, android.R.layout.simple_spinner_item)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            RS_course_leaderboard_spinner.adapter = adapter
            RS_course_leaderboard_spinner.visibility = View.VISIBLE
            RS_course_leaderboard_spinner.onItemSelectedListener = this
        }

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        val activity = activity as SpRsActivity
        when (position) {
            0 -> displayLeaderboardUI(viewModel.courseTeeStatsAfter!!.topTenScore, viewModel.playerTeeStatsAfter!!.bestScore)
            1 -> displayAvgStrokesLeaderboardUI(activity.viewModel.courseTeeStatsAfter!!.topTenAvgStrokes, viewModel.playerTeeStatsAfter!!.avgStrokes)
            2 -> displayLpmLeaderboardUI(activity.viewModel.courseTeeStatsAfter!!.topTenLPM, viewModel.playerTeeStatsAfter!!.lifetimePlusMinus)
            3 -> displayLeaderboardUI(activity.viewModel.courseTeeStatsAfter!!.topTenAces, viewModel.playerTeeStatsAfter!!.numAces)
            4 -> displayLeaderboardUI(activity.viewModel.courseTeeStatsAfter!!.topTenEagles, viewModel.playerTeeStatsAfter!!.numEagles)
            5 -> displayLeaderboardUI(activity.viewModel.courseTeeStatsAfter!!.topTenBirdies, viewModel.playerTeeStatsAfter!!.numBirdies)
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun displayLeaderboardUI(leaderboardList: MutableList<LeaderboardEntry>, bestScore: Int) {
        val textViewList = mutableListOf<TextView>(
                RS_course_leaderboard_1_text_view,
                RS_course_leaderboard_2_text_view,
                RS_course_leaderboard_3_text_view,
                RS_course_leaderboard_4_text_view,
                RS_course_leaderboard_5_text_view,
                RS_course_leaderboard_6_text_view,
                RS_course_leaderboard_7_text_view,
                RS_course_leaderboard_8_text_view,
                RS_course_leaderboard_9_text_view,
                RS_course_leaderboard_10_text_view
        )

        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val username = prefs.getString(PreferenceNames.USERNAME.toString(), "null")
        var userDisplayed = false

        for (i in 0 until 10) {
            if (leaderboardList.size > i) {
                val currentEntry = leaderboardList[i]
                if (currentEntry.username == username) {
                    // This is the users entry
                    val text = "${currentEntry.score} You"
                    textViewList[i].text = text
                    textViewList[i].setTypeface(null, Typeface.BOLD)
                    userDisplayed = true
                    textViewList[i].visibility = View.VISIBLE
                } else {
                    // Another players entry
                    textViewList[i].text = CourseTeeStats.Companion.getLeaderboardString(
                            currentEntry.score, currentEntry.username)
                    textViewList[i].visibility = View.VISIBLE
                }
            } else if (!userDisplayed) {
                val text = "$bestScore You"
                textViewList[i].text = text
                textViewList[i].setTypeface(null, Typeface.BOLD)
                userDisplayed = true
                textViewList[i].visibility = View.VISIBLE
            }
        }

        if (!userDisplayed) {
            val text = "$bestScore You"
            RS_course_leaderboard_user_text_view.text = text
            RS_course_leaderboard_user_text_view.setTypeface(null, Typeface.BOLD)
            RS_course_leaderboard_user_text_view.visibility = View.VISIBLE
        }

        RS_course_leaderboard_progress_bar.visibility = View.GONE
        RS_course_leaderboard_scroll_view.visibility = View.VISIBLE
    }

    fun displayLpmLeaderboardUI(leaderboardList: MutableList<LeaderboardEntry>, bestScore: Int) {
        val textViewList = mutableListOf<TextView>(
                RS_course_leaderboard_1_text_view,
                RS_course_leaderboard_2_text_view,
                RS_course_leaderboard_3_text_view,
                RS_course_leaderboard_4_text_view,
                RS_course_leaderboard_5_text_view,
                RS_course_leaderboard_6_text_view,
                RS_course_leaderboard_7_text_view,
                RS_course_leaderboard_8_text_view,
                RS_course_leaderboard_9_text_view,
                RS_course_leaderboard_10_text_view
        )

        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val username = prefs.getString(PreferenceNames.USERNAME.toString(), "null")
        var userDisplayed = false

        for (i in 0 until 10) {
            if (leaderboardList.size > i) {
                val currentEntry = leaderboardList[i]
                if (currentEntry.username == username) {
                    val text = "${GolfUtils.formatScore(currentEntry.score)} You"
                    textViewList[i].text = text
                    textViewList[i].setTypeface(null, Typeface.BOLD)
                    userDisplayed = true
                    textViewList[i].visibility = View.VISIBLE
                } else {
                    // Another players entry
                    val text = "${GolfUtils.formatScore(currentEntry.score)} ${currentEntry.username}"
                    textViewList[i].text = text
                    textViewList[i].visibility = View.VISIBLE
                }
            } else if (!userDisplayed) {
                val text = "${GolfUtils.formatScore(bestScore)} You"
                textViewList[i].text = text
                textViewList[i].setTypeface(null, Typeface.BOLD)
                userDisplayed = true
                textViewList[i].visibility = View.VISIBLE
            }
        }

        if (!userDisplayed) {
            val text = "$bestScore You"
            RS_course_leaderboard_user_text_view.text = text
            RS_course_leaderboard_user_text_view.setTypeface(null, Typeface.BOLD)
            RS_course_leaderboard_user_text_view.visibility = View.VISIBLE
        }

        RS_course_leaderboard_progress_bar.visibility = View.GONE
        RS_course_leaderboard_scroll_view.visibility = View.VISIBLE
    }

    fun displayAvgStrokesLeaderboardUI(leaderboardList: MutableList<LeaderboardEntryDouble>, avgScore: Double) {
        val textViewList = mutableListOf<TextView>(
                RS_course_leaderboard_1_text_view,
                RS_course_leaderboard_2_text_view,
                RS_course_leaderboard_3_text_view,
                RS_course_leaderboard_4_text_view,
                RS_course_leaderboard_5_text_view,
                RS_course_leaderboard_6_text_view,
                RS_course_leaderboard_7_text_view,
                RS_course_leaderboard_8_text_view,
                RS_course_leaderboard_9_text_view,
                RS_course_leaderboard_10_text_view
        )

        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val username = prefs.getString(PreferenceNames.USERNAME.toString(), "null")
        var userDisplayed = false

        for (i in 0 until 10) {
            if (leaderboardList.size > i) {
                val currentEntry = leaderboardList[i]
                if (currentEntry.username == username) {
                    // This is the users entry
                    val text = "${GeneralUtils.roundTwoPlacesString(currentEntry.score)} You"
                    textViewList[i].text = text
                    textViewList[i].setTypeface(null, Typeface.BOLD)
                    userDisplayed = true
                    textViewList[i].visibility = View.VISIBLE
                } else {
                    // Another players entry
                    textViewList[i].text = CourseTeeStats.Companion.getLeaderboardStringDouble(
                            currentEntry.score, currentEntry.username)
                    textViewList[i].visibility = View.VISIBLE
                }
            } else if (!userDisplayed) {
                val text = "${GeneralUtils.roundTwoPlacesString(avgScore)} You"
                textViewList[i].text = text
                textViewList[i].setTypeface(null, Typeface.BOLD)
                userDisplayed = true
                textViewList[i].visibility = View.VISIBLE
            }
        }

        if (!userDisplayed) {
            val text = "$avgScore You"
            RS_course_leaderboard_user_text_view.text = text
            RS_course_leaderboard_user_text_view.setTypeface(null, Typeface.BOLD)
            RS_course_leaderboard_user_text_view.visibility = View.VISIBLE
        }

        RS_course_leaderboard_progress_bar.visibility = View.GONE
        RS_course_leaderboard_scroll_view.visibility = View.VISIBLE
    }

    fun displayNoConnectionUI() {
        RS_course_leaderboard_message_text_view.text = getString(R.string.no_connection_message)
        RS_course_leaderboard_progress_bar.visibility = View.GONE
        RS_course_leaderboard_message_text_view.visibility = View.VISIBLE
    }

    fun displayEmptyLeaderboardUI() {
        RS_course_leaderboard_message_text_view.text = getString(R.string.no_stats_found)
        RS_course_leaderboard_progress_bar.visibility = View.GONE
        RS_course_leaderboard_message_text_view.visibility = View.VISIBLE
    }


}
