package com.tsquaredapplications.chains2.Fragments


import android.arch.persistence.room.Room
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tsquaredapplications.chains2.Adapters.UnfinishedRoundAdapter
import com.tsquaredapplications.chains2.Enums.RoomDbNames
import com.tsquaredapplications.chains2.FindCourseActivity
import com.tsquaredapplications.chains2.R
import com.tsquaredapplications.chains2.RoomDB.SavedScorecardDatabase
import kotlinx.android.synthetic.main.fragment_rounds.*
import java.util.*

class RoundsFragment : Fragment() {



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_rounds, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {


        // get ongoing rounds
        val db = Room.databaseBuilder(context!!,
                SavedScorecardDatabase::class.java, RoomDbNames.SAVED_SCORECARDS.toString())
                .allowMainThreadQueries()
                .build()

        val ongoingRoundsList = db.savedScorecardDao().loadAllScorecards().toMutableList()

        db.close()

        if (ongoingRoundsList.isEmpty()) {
            empty_list_text_view.visibility = View.VISIBLE
        } else {
           ongoingRoundsList.sortBy { it.courseName }

            val layoutManager = LinearLayoutManager(context,
                    LinearLayoutManager.VERTICAL, false)
            unfinished_rounds_recycler_view.layoutManager = layoutManager
            unfinished_rounds_recycler_view.setHasFixedSize(true)
            val unfinishedRoundAdapter = UnfinishedRoundAdapter(ongoingRoundsList, context!!)
            unfinished_rounds_recycler_view.adapter = unfinishedRoundAdapter
            unfinished_rounds_recycler_view.visibility = View.VISIBLE
            ongoing_rounds_text_view.visibility = View.VISIBLE

        }

        new_round_button.setOnClickListener {
            startActivity(Intent(context, FindCourseActivity::class.java))
        }
    }


}
