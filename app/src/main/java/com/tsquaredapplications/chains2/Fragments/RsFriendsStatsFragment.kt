package com.tsquaredapplications.chains2.Fragments


import android.arch.lifecycle.ViewModelProviders
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerTeeStats
import com.tsquaredapplications.chains2.R
import com.tsquaredapplications.chains2.SpRsViewModel
import com.tsquaredapplications.chains2.Utilities.GeneralUtils
import kotlinx.android.synthetic.main.fragment_rs_friend_stats.*

class RsFriendsStatsFragment : Fragment() {

    val viewModel by lazy { ViewModelProviders.of(activity!!).get(SpRsViewModel::class.java) }
    val username by lazy { GeneralUtils.getUsername(context!!) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_rs_friend_stats, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        if(!viewModel.isNetworkAvailable)
            displayNoConnectionUI()
        else if ( viewModel.friendsStatList.isEmpty())
            displayNoFriendsPlayedUI()
        else {
            displayFriendStatsUI()

        }
    }

    private fun displayFriendStatsUI() {
        var displayList = mutableListOf<String>()
        var userIndex = 0
        var userDisplayed = false
        val statsList = mutableListOf(viewModel.playerTeeStatsAfter!!)
        for(stats: PlayerTeeStats in viewModel.friendsStatList)
            statsList.add(stats)


        val numberToDisplay = if (viewModel.friendsStatList.size > 3) 3 else statsList.size

        // Avg Strokes
        var views = mutableListOf<TextView>(RS_friends_stats_avg_strk_1_text_view,
                RS_friends_stats_avg_strk_2_text_view,
                RS_friends_stats_avg_strk_3_text_view,
                RS_friends_stats_avg_strk_user_text_view)

        viewModel.friendsStatList.sortBy { it.avgStrokes }
        for(i in 0 until numberToDisplay){
            if (statsList[i].username == username){
                displayList.add("${GeneralUtils.roundTwoPlacesString(statsList[i].avgStrokes)} You")
                userDisplayed = true
                userIndex = i
            } else {
                // This is a friend
                displayList.add(("${GeneralUtils.roundTwoPlacesString(statsList[i].avgStrokes)} " +
                        statsList[i].username))

            }
        }

        if(!userDisplayed){
            displayList.add("${GeneralUtils.roundTwoPlacesString(viewModel.playerTeeStatsAfter!!.avgStrokes)} You")
            userIndex = 3
        }

        populateList(views, displayList, userIndex)

        // Lifetime Plus Minus
        views = mutableListOf(RS_friends_stats_bpm1_text_view,
                RS_friends_stats_bpm2_text_view,
                RS_friends_stats_bpm3_text_view,
                RS_friends_stats_bpm_user_text_view)

        viewModel.friendsStatList.sortBy { it.lifetimePlusMinus }
        displayList = mutableListOf()
        userDisplayed = false
        for(i in 0 until numberToDisplay){
            val lpm = statsList[i]!!.lifetimePlusMinus
            if(statsList[i].username == username){
                // this is the user
                if (lpm > 0)
                    displayList.add( "+" + Integer.toString(lpm) + " You")
                else if (lpm == 0)
                    displayList.add("Even You")
                else
                    displayList.add(Integer.toString(lpm) + " You")


                userDisplayed = true
                userIndex = i
            } else {
                // this is a friend
                if (lpm > 0) {
                    displayList.add("+${Integer.toString(lpm)} ${statsList[i].username}")
                } else if (lpm == 0) {
                    displayList.add("Even ${statsList[i].username}")
                } else {
                    displayList.add("${Integer.toString(lpm)} ${statsList[i].username}")
                }

            }
        }

        if(!userDisplayed){
            val lpm = viewModel.playerTeeStatsAfter!!.lifetimePlusMinus
            if (lpm > 0)
                displayList.add("+${Integer.toString(viewModel.playerTeeStatsAfter!!.lifetimePlusMinus)} You")
            else if (lpm == 0)
                displayList.add("Even You")
            else
                displayList.add("${Integer.toString(viewModel.playerTeeStatsAfter!!.lifetimePlusMinus)} You")
            userIndex = 3
        }

        populateList(views, displayList, userIndex)

        // Best Scores
        views = mutableListOf(RS_friends_stats_best_score_1_text_view,
                RS_friends_stats_best_score_2_text_view,
                RS_friends_stats_best_score_3_text_view,
                RS_friends_stats_best_score_user_text_view)

        statsList.sortBy { it?.bestScore }
        displayList = mutableListOf()
        userDisplayed = false
        for(i in 0 until numberToDisplay){
            if (statsList[i]?.username == username) {
                // this is the user
                displayList.add(Integer.toString(statsList[i]!!
                        .bestScore) + " You")

                userDisplayed = true
                userIndex = i
            } else {
                // this is a friend
                displayList.add( Integer.toString(statsList[i]!!
                        .bestScore) +
                        " " + statsList[i]?.username)


            }
        }

        if(!userDisplayed){
            displayList.add("${Integer.toString(viewModel.playerTeeStatsAfter!!.bestScore)} You")
            userIndex = 3
        }

        populateList(views, displayList, userIndex)


        // Worst Scores
        views = mutableListOf(RS_friends_stats_worst_score_1_text_view,
                RS_friends_stats_worst_score_2_text_view,
                RS_friends_stats_worst_score_3_text_view,
                RS_friends_stats_worst_score_user_text_view)

        statsList.sortBy { it?.worstScore }
        statsList.reverse()
        displayList = mutableListOf()
        userDisplayed = false
        for(i in 0 until numberToDisplay){
            if (statsList[i]?.username == username) {
                // this is the user
                displayList.add(Integer.toString(statsList[i]!!
                        .worstScore) + " You")

                userDisplayed = true
                userIndex = i
            } else {
                // this is a friend
                displayList.add( Integer.toString(statsList[i]!!
                        .worstScore) +
                        " " + statsList[i]?.username)


            }
        }

        if(!userDisplayed){
            displayList.add("${Integer.toString(viewModel.playerTeeStatsAfter!!.worstScore)} You")
            userIndex = 3
        }

        populateList(views, displayList, userIndex)


        // Times Played
        views = mutableListOf(RS_friends_stats_times_played_1_text_view,
                RS_friends_stats_times_played_2_text_view,
                RS_friends_stats_times_played_3_text_view,
                RS_friends_stats_times_played_user_text_view)

        statsList.sortBy { it?.roundsPlayed }
        statsList.reverse()
        displayList = mutableListOf()
        userDisplayed = false
        for(i in 0 until numberToDisplay){
            if (statsList[i]?.username == username) {
                // this is the user
                displayList.add(Integer.toString(statsList[i]!!
                        .roundsPlayed) + " You")

                userDisplayed = true
                userIndex = i
            } else {
                // this is a friend
                displayList.add( Integer.toString(statsList[i]!!
                        .roundsPlayed) +
                        " " + statsList[i]?.username)


            }
        }

        if(!userDisplayed){
            displayList.add("${Integer.toString(viewModel.playerTeeStatsAfter!!.roundsPlayed)} You")
            userIndex = 3
        }

        populateList(views, displayList, userIndex)

        // Birdie Count
        views = mutableListOf(RS_friends_stats_birdies_1_text_view,
                RS_friends_stats_birdies_2_text_view,
                RS_friends_stats_birdies_3_text_view,
                RS_friends_stats_birdies_text_view)

        statsList.sortBy { it?.numBirdies }
        statsList.reverse()
        displayList = mutableListOf()
        userDisplayed = false
        for(i in 0 until numberToDisplay){
            if (statsList[i]?.username == username) {
                // this is the user
                displayList.add(Integer.toString(statsList[i]!!
                        .numBirdies) + " You")

                userDisplayed = true
                userIndex = i
            } else {
                // this is a friend
                displayList.add( Integer.toString(statsList[i]!!
                        .numBirdies) +
                        " " + statsList[i]?.username)


            }
        }

        if(!userDisplayed){
            displayList.add("${Integer.toString(viewModel.playerTeeStatsAfter!!.numBirdies)} You")
            userIndex = 3
        }

        populateList(views, displayList, userIndex)

        RS_friends_stats_scroll_view.visibility = View.VISIBLE




    }

    fun populateList(views: MutableList<TextView>, textList: MutableList<String>, userIndex: Int){
        for(i in 0..3){
            if(i < textList.size){
                if(i == userIndex){
                    views[i].text = textList[i]
                    views[i].setTypeface(null, Typeface.BOLD)
                } else {
                    views[i].text = textList[i]
                }
            } else {
                // noone left to display
                views[i].visibility = View.INVISIBLE
            }
        }
    }
    private fun displayNoConnectionUI(){
        RS_friends_stats_no_stats_message_text_view.text = getString(R.string.no_connection_message)
        RS_friends_stats_progress_bar.visibility = View.GONE
        RS_friends_stats_no_stats_message_text_view.visibility = View.VISIBLE
    }

    private fun displayNoFriendsPlayedUI(){
        RS_friends_stats_no_stats_message_text_view.text = getString(R.string.no_friends_have_played_message)
        RS_friends_stats_progress_bar.visibility = View.GONE
        RS_friends_stats_no_stats_message_text_view.visibility = View.VISIBLE
    }

}
