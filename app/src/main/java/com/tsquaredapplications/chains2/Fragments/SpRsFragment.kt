package com.tsquaredapplications.chains2.Fragments


import android.arch.lifecycle.ViewModelProviders
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tsquaredapplications.chains2.Enums.IntentExtraNames
import com.tsquaredapplications.chains2.Objects.Scorecard
import com.tsquaredapplications.chains2.R
import com.tsquaredapplications.chains2.SpRsViewModel
import com.tsquaredapplications.chains2.Utilities.GolfUtils
import kotlinx.android.synthetic.main.fragment_rs_single_player_summary.*


class SpRsFragment : Fragment() {

    val viewModel by lazy { ViewModelProviders.of(activity!!).get(SpRsViewModel::class.java) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
       return inflater.inflate(R.layout.fragment_rs_single_player_summary, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        updateUI()
    }

    fun updateUI(){



            sprs_course_name_text_view.text = viewModel.scorecard.courseName
            val drawable = resources.getDrawable(R.drawable.tee_indicator)
            val gradientDrawable = drawable as GradientDrawable
            gradientDrawable.colors = intArrayOf(Color.parseColor(viewModel.scorecard.teeColor), Color.parseColor(viewModel.scorecard.teeColor))
            sprs_summary_tee_indicator_image_view.background = gradientDrawable


            if(viewModel.scorecard.plusMinus == 0){
                val score = "${getString(R.string.score_)} ${getString(R.string.even)}"
                sprs_score_text_view.text = score
            } else if (viewModel.scorecard.plusMinus > 0){
                val score = "${getString(R.string.score_)} +${viewModel.scorecard.plusMinus}"
                sprs_score_text_view.text = score
            } else {
                val score = "${getString(R.string.score_)} ${viewModel.scorecard.plusMinus}"
                sprs_score_text_view.text = score
            }

            val strokes = "${getString(R.string.strokes)} ${viewModel.scorecard.getTotalStrokes()}"
            sprs_strokes_text_view.text = strokes

            val scoreNameTotals = GolfUtils.scoreNameTotals(viewModel.scorecard)
            sprs_aces_text_view.text = "${Integer.toString(scoreNameTotals[0])}"
            sprs_albatross_text_view.text = "${Integer.toString(scoreNameTotals[1])}"
            sprs_eagles_text_view.text = "${Integer.toString(scoreNameTotals[2])}"
            sprs_birdies_text_view.text = "${Integer.toString(scoreNameTotals[3])}"
            sprs_pars_text_view.text = "${Integer.toString(scoreNameTotals[4])}"
            sprs_bogeys_text_view.text = "${Integer.toString(scoreNameTotals[5])}"
            sprs_dbl_bogeys_text_view.text = "${Integer.toString(scoreNameTotals[6])}"
            sprs_tri_bogeys_text_view.text = "${Integer.toString(scoreNameTotals[7])}"





    }
}
