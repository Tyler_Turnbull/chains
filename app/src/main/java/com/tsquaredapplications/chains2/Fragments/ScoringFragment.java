package com.tsquaredapplications.chains2.Fragments;


import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.tsquaredapplications.chains2.Enums.IntentExtraNames;
import com.tsquaredapplications.chains2.Enums.RoomDbNames;
import com.tsquaredapplications.chains2.MainActivity;
import com.tsquaredapplications.chains2.Objects.Scorecard;
import com.tsquaredapplications.chains2.R;
import com.tsquaredapplications.chains2.RoomDB.SavedScorecardDatabase;
import com.tsquaredapplications.chains2.SpScoringActivity;
import com.tsquaredapplications.chains2.SpScoringViewModel;
import com.tsquaredapplications.chains2.SpRsActivity;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScoringFragment extends Fragment  {

    private static final String LOG_TAG = ScoringFragment.class.getName();

    private SpScoringActivity mActivity;

    ChangeHoleListener mChangeHoleCallback;

    private SpScoringViewModel viewModel;


    @BindView(R.id.text_view_SP_hole_number)
    TextView holeNumberTextView;

    @BindView(R.id.text_view_SP_par)
    TextView parTextView;

    @BindView(R.id.text_view_SP_score)
    TextView scoreTextView;

    @BindView(R.id.tv_SP_current_hole_strokes)
    TextView strokesForHoleTextView;

    @BindView(R.id.next_hole_button)
    Button nextHoleButton;

    @BindView(R.id.previous_hole_button)
    Button previousHoleButton;


    @BindView(R.id.fab_SP_decrease_score)
    Button decreaseStrokesButton;

    @BindView(R.id.fab_SP_increase_score)
    Button increaseStrokesButton;


    public ScoringFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_scoring, container, false);
        ButterKnife.bind(this, view);
        mActivity = ((SpScoringActivity) getActivity());
        viewModel = ViewModelProviders.of(getActivity()).get(SpScoringViewModel.class);
        updateUI();
        setClickListeners();


        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mChangeHoleCallback = (ChangeHoleListener) context;
        } catch(ClassCastException e){
            e.printStackTrace();
        }
    }

    public void backPressed(){


        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setNeutralButton(getString(R.string.cancel), null)
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mActivity.mIsFinishing = true;
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                })
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SavedScorecardDatabase db = Room.databaseBuilder(getContext(),
                                SavedScorecardDatabase.class, RoomDbNames.SAVED_SCORECARDS.toString())
                                .allowMainThreadQueries()
                                .build();

                        db.savedScorecardDao().insertScorecard(viewModel.getScorecard());
                        db.close();

                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    }
                })
                .setMessage("Going back will exit the round.\n\nWould you like to save this round?")
                .create();
        builder.show();
    }

    private void updateUI() {
        holeNumberTextView.setText(
                getString(R.string.hole_identifier,
                        (viewModel.getScorecard()
                                .getCurrentHole() + 1)
                )
        );

        parTextView.setText(getString(R.string.par_identifier,
                (viewModel.getScorecard().getHolePar((viewModel.getScorecard().getCurrentHole())))));

        if (viewModel.getScorecard().getPlusMinus() == 0) {
            scoreTextView.setText(getString(R.string.even));
        } else if (viewModel.getScorecard().getPlusMinus() > 0) {
            scoreTextView.setText(getString(R.string.above_par_format,
                    viewModel.getScorecard().getPlusMinus()));
        } else {
            scoreTextView.setText(String.valueOf(viewModel.getScorecard().getPlusMinus()));
        }


        strokesForHoleTextView.setText(String.valueOf((
                viewModel.getScorecard().getHoleScore(
                        (viewModel.getScorecard().getCurrentHole())
                )
        )));

        if ( viewModel.getScorecard().getCurrentHole() == 0) {
            previousHoleButton.setEnabled(false);
        } else if (viewModel.getScorecard().getCurrentHole() == viewModel.getScorecard().getNumHoles() - 1) {
            nextHoleButton.setText(getString(R.string.finish));
        } else {
            previousHoleButton.setEnabled(true);
        }


    }

    private void setClickListeners() {
        previousHoleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewModel.getScorecard().getCurrentHole() > 0) {
                   viewModel.getScorecard().setCurrentHole(viewModel.getScorecard().getCurrentHole() - 1);
                    updateUI();
                    mActivity.changeHole(viewModel.getScorecard().getCurrentHole());
                    if(nextHoleButton.getText().equals(getString(R.string.finish)))
                        nextHoleButton.setText(getString(R.string.next));
                }

            }
        });

        nextHoleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewModel.getScorecard().getCurrentHole() < viewModel.getScorecard().getNumHoles() - 1) {
                    viewModel.getScorecard().setCurrentHole(viewModel.getScorecard().getCurrentHole() + 1);
                    updateUI();
                    mActivity.changeHole(viewModel.getScorecard().getCurrentHole());

                } else {

                    // User clicked finish
                    mActivity.mIsFinishing = true;

                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setNegativeButton("Cancel", null)
                            .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent finishIntent = new Intent(getActivity(), SpRsActivity.class);
                                    if (viewModel.isFirstTime())
                                        finishIntent.putExtra(IntentExtraNames.FIRST_TIME.toString(), true);
                                    else
                                        finishIntent.putExtra(IntentExtraNames.FIRST_TIME.toString(), false);

                                    // Remove possible saved scorecard from the ongoing rounds
                                    RemoveSavedScorecardTask task = new RemoveSavedScorecardTask((SpScoringActivity)getActivity());
                                    task.execute(viewModel.getScorecard());

                                    finishIntent.putExtra(IntentExtraNames.SCORECARD.toString(), viewModel.getScorecard());

                                   startActivity(finishIntent);
                                }
                            })
                            .setMessage("Once you submit you will not be able to return\n\n" +
                                    "Do you want to submit?")
                            .create();
                    builder.show();
                }
            }
        });

        increaseStrokesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.getScorecard().incrementHoleScore(viewModel.getScorecard().getCurrentHole());
                updateUI();
            }
        });

        decreaseStrokesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewModel.getScorecard().getHoleScore(viewModel.getScorecard().getCurrentHole()) > 1) {
                    viewModel.getScorecard().decrementHoleScore(viewModel.getScorecard().getCurrentHole());
                    updateUI();
                }
            }
        });
    }

    public interface ChangeHoleListener{
        void changeHole(int holeNum);
    }



    private static class RemoveSavedScorecardTask extends AsyncTask<Scorecard, Void, Void> {

        private WeakReference<SpScoringActivity> activityReference;


        RemoveSavedScorecardTask(SpScoringActivity context) {
            activityReference = new WeakReference<SpScoringActivity>(context);

        }

        @Override
        protected Void doInBackground(Scorecard... scorecards) {

            SavedScorecardDatabase db = Room.databaseBuilder(activityReference.get(),
                    SavedScorecardDatabase.class, RoomDbNames.SAVED_SCORECARDS.toString())
                    .build();

            db.savedScorecardDao().deleteScorecards(scorecards);
            db.close();
            return null;

        }

    }
}
