package com.tsquaredapplications.chains2.Fragments


import android.arch.persistence.room.Room
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tsquaredapplications.chains2.Enums.PreferenceNames
import com.tsquaredapplications.chains2.Enums.RoomDbNames
import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerStats
import com.tsquaredapplications.chains2.R
import com.tsquaredapplications.chains2.RoomDB.PlayerOverallStatsDatabase
import com.tsquaredapplications.chains2.Utilities.ProfilePicUtil
import kotlinx.android.synthetic.main.fragment_home.*


/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {

    var playerOverallStats: PlayerStats? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater!!.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val username = prefs.getString(PreferenceNames.USERNAME.toString(), "username")
        home_username_text_view.text = username

        // get player data from room db
        val db = Room.databaseBuilder(context!!, PlayerOverallStatsDatabase::class.java,
                RoomDbNames.USER_OVERALL_STATS_DATABASE.toString())
                .build()

        getProfilePic()
        val handler = Handler()
        Thread {
            var data = db.playerOverallStatsDao().loadPlayerStats()
            while (data == null || data.size == 0) {
                data = db.playerOverallStatsDao().loadPlayerStats()
            }
            handler.post {
                if (data != null && data.size > 0) {
                    playerOverallStats = data[0]
                    updateStatsUi()
                }
            }
        }.start()
    }

    private fun updateStatsUi() {
        playerOverallStats?.let {
            home_rounds_played_value_text_view?.text = it.numRoundsPlayed.toString()
            home_total_strokes_value_text_view?.text = it.totalStrokes.toString()
            var text: String
            if (it.lifetimePlusMinus > 0) {
                text = "+" + it.lifetimePlusMinus.toString()
            } else if (it.lifetimePlusMinus == 0) {
                text = getString(R.string.even)
            } else {
                text = it.lifetimePlusMinus.toString()
            }

            home_lpm_value_text_view?.text = text
        }

    }

    private fun getProfilePic() {
        val bitmap = ProfilePicUtil.loadPhotoFromInternalStorage(context!!)
        if (bitmap != null)
            profile_picture_image_view.setImageBitmap(bitmap)
    }
}
