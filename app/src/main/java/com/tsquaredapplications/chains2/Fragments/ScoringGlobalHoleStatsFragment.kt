package com.tsquaredapplications.chains2.Fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.tsquaredapplications.chains2.R


/**
 * A simple [Fragment] subclass.
 */
class ScoringGlobalHoleStatsFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_scoring_global_hole_stats, container, false)
    }

}// Required empty public constructor
