package com.tsquaredapplications.chains2

import android.app.Activity
import android.arch.persistence.room.Room
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.MediaStore
import android.util.Log
import android.view.View
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.tsquaredapplications.chains2.Enums.FirebaseDbNames
import com.tsquaredapplications.chains2.Enums.FirebaseStorageNames
import com.tsquaredapplications.chains2.Enums.PreferenceNames
import com.tsquaredapplications.chains2.Enums.RoomDbNames
import com.tsquaredapplications.chains2.Fragments.CreateUsernameFragment
import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerStats
import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerTeeStats
import com.tsquaredapplications.chains2.RoomDB.PlayerOverallStatsDatabase
import com.tsquaredapplications.chains2.RoomDB.PlayerTeeStatsDatabase
import com.tsquaredapplications.chains2.Utilities.ProfilePicUtil
import kotlinx.android.synthetic.main.activity_login.*
import java.io.File
import java.util.*

class LoginActivity : AppCompatActivity() {

    val RC_SIGN_IN = 123
    var mAuth: FirebaseAuth? = null
    lateinit var mPrefs: SharedPreferences
    lateinit var mPrefEditor: SharedPreferences.Editor

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_login)
        super.onCreate(savedInstanceState)

        mPrefs = PreferenceManager.getDefaultSharedPreferences(this)
        mAuth = FirebaseAuth.getInstance()
        firebaseAuth()

        mPrefs = PreferenceManager.getDefaultSharedPreferences(this)
    }

    override fun onResume() {
        if (mAuth?.currentUser != null) {
            usernameCheck()
        }
        super.onResume()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK) {
                // user signed in

                // check for stats
                val db = FirebaseDatabase.getInstance().getReference()
                        .child(FirebaseDbNames.USER_TEE_STATS.toString())
                        .child(FirebaseAuth.getInstance().uid)

                db.addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        if (dataSnapshot.exists()) {

                            val playerTeeStatsDB = Room.databaseBuilder(getApplicationContext(),
                                    PlayerTeeStatsDatabase::class.java, RoomDbNames.USER_TEE_STATS_DATABASE.toString())
                                    .allowMainThreadQueries()
                                    .build()
                            val courses = dataSnapshot.getChildren()
                            for (d in courses) {
                                val tees = d.getChildren()
                                for (t in tees) {
                                    val teeStats = t.getValue(PlayerTeeStats::class.java)
                                    playerTeeStatsDB.playerTeeStatsDao().insertCourseStats(teeStats)
                                }
                            }
                            playerTeeStatsDB.close()
                            // Now get player overall stats
                            val playerStatsDB = FirebaseDatabase.getInstance().getReference()
                                    .child(FirebaseDbNames.USER_OVERALL_STATS.toString())
                                    .child(FirebaseAuth.getInstance().getUid())
                            playerStatsDB.addListenerForSingleValueEvent(object : ValueEventListener {
                                override fun onDataChange(dataSnapshot: DataSnapshot) {
                                    var stats: PlayerStats?
                                    stats = dataSnapshot.getValue(PlayerStats::class.java)
                                    if (stats == null) {

                                        // No player stats, must be a new user
                                        stats = PlayerStats()
                                    }
                                    val playerStatsDB = Room.databaseBuilder(getApplicationContext(),
                                            PlayerOverallStatsDatabase::class.java, RoomDbNames.USER_OVERALL_STATS_DATABASE.toString())
                                            .allowMainThreadQueries()
                                            .build()
                                    playerStatsDB.playerOverallStatsDao().insertPlayerStats(stats)
                                    playerStatsDB.close()
                                }

                                override fun onCancelled(databaseError: DatabaseError) {
                                }
                            })
                        } else {
                            // No player tee stats yet
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                    }
                })

                // Check for profile picture
                val storageRef = FirebaseStorage.getInstance().reference
                        .child(FirebaseStorageNames.PROFILE_PICTURES.toString())
                        .child(FirebaseAuth.getInstance().uid!!)

                val contextWrapper = ContextWrapper(applicationContext)
                val fileDirectory = contextWrapper.getDir(ProfilePicUtil.PHOTO_DIRECTORY, Context.MODE_PRIVATE)
                val filePath = File(fileDirectory, ProfilePicUtil.FILE_NAME)

                storageRef.getFile(filePath).addOnSuccessListener {
                    val bitmap = BitmapFactory.decodeFile(filePath.toString())
                    val savedFilePath = ProfilePicUtil.savePhoto(applicationContext, bitmap)
                    val prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext).edit()
                    prefs.putString(PreferenceNames.PROFILE_PICTURE.toString(), savedFilePath).apply()
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    fun firebaseAuth() {
        if (mAuth?.currentUser != null) {
            // already signed in
            usernameCheck()
        } else {
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setLogo(R.drawable.chains_logo_v1)
                            .setAvailableProviders(
                                    Arrays.asList(
                                            AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                                            AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()
                                    )
                            )
                            .build(),
                    RC_SIGN_IN
            )
        }
    }

    fun usernameCheck() {


        if (!mPrefs.getBoolean(PreferenceNames.HAS_USERNAME.toString(), false)) {

            // Check to see if user has registered username on firebase in case switching phones
            val db = FirebaseDatabase.getInstance().getReference()
                    .child(FirebaseDbNames.USERNAMES.toString())
                    .child(FirebaseAuth.getInstance().uid)

            db.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(datasnapshot: DataSnapshot) {
                    if (datasnapshot.exists()) {
                        val username = datasnapshot.getValue(String::class.java)

                        mPrefEditor = mPrefs?.edit()
                        mPrefEditor.apply { putBoolean(PreferenceNames.HAS_USERNAME.toString(), true) }
                        mPrefEditor.apply { putString(PreferenceNames.USERNAME.toString(), username) }
                        mPrefEditor.commit()

                        // username set
                        startActivity(Intent(applicationContext, MainActivity::class.java))

                    } else {

                        // no username found, launching username fragment
                        progress_bar.visibility = View.GONE
                        val fragment = CreateUsernameFragment()
                        val transaction = supportFragmentManager.beginTransaction()
                        transaction.replace(R.id.content_frame, fragment)
                        transaction.commit()
                    }
                }

                override fun onCancelled(p0: DatabaseError?) {

                }
            })
        } else {
            // has username already within prefs
            startActivity(Intent(this, MainActivity::class.java))
        }

    }
}
