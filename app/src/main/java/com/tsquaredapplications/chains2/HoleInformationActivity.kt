package com.tsquaredapplications.chains2


import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.tsquaredapplications.chains2.Adapters.HoleInfoAdapter
import com.tsquaredapplications.chains2.Enums.IntentExtraNames
import com.tsquaredapplications.chains2.Objects.Course
import kotlinx.android.synthetic.main.activity_hole_information.*
import android.view.animation.AnimationUtils
import android.support.constraint.ConstraintSet
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Button


var currentTeeSelected = 0

class HoleInformationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hole_information)
        val course = intent.getParcelableExtra<Course>(IntentExtraNames.COURSE.toString())

        Log.i(this.toString(), course.toString())

        // Remove tees not in use and set tee colors
        if (!course.teeFourExists) tee_indicator_four.visibility = View.GONE
        else {

            val gradientDrawable = getDrawable(R.drawable.tee_indicator) as GradientDrawable
            gradientDrawable.mutate()
            gradientDrawable.colors = intArrayOf(Color.parseColor(course.teeFourColor), Color.parseColor(course.teeFourColor))
            tee_indicator_four.background = gradientDrawable
        }
        if (!course.teeThreeExists) tee_indicator_three.visibility = View.GONE
        else {
            val gradientDrawable = getDrawable(R.drawable.tee_indicator) as GradientDrawable
            gradientDrawable.mutate()
            gradientDrawable.colors = intArrayOf(Color.parseColor(course.teeThreeColor), Color.parseColor(course.teeThreeColor))
            tee_indicator_three.background = gradientDrawable
        }
        if (!course.teeTwoExists) tee_indicator_two.visibility = View.GONE
        else {
            val gradientDrawable = getDrawable(R.drawable.tee_indicator) as GradientDrawable
            gradientDrawable.mutate()
            gradientDrawable.colors = intArrayOf(Color.parseColor(course.teeTwoColor), Color.parseColor(course.teeTwoColor))
            tee_indicator_two.background = gradientDrawable
        }
        if (!course.teeOneExists) {
            var constraintSet = ConstraintSet()
            constraintSet.clone(hole_information_constraint_layout)
            constraintSet.connect(tee_indicator_two.id, ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START, 0)
            val styledAttributes = theme.obtainStyledAttributes(intArrayOf(android.R.attr.actionBarSize))
            val actionBarSize = styledAttributes.getDimension(0, 0f).toInt()
            styledAttributes.recycle()
            constraintSet.connect(tee_indicator_two.id, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, 24 + actionBarSize)
            constraintSet.applyTo(hole_information_constraint_layout)
            tee_indicator_one.visibility = View.GONE

        } else {
            val gradientDrawable = getDrawable(R.drawable.tee_indicator) as GradientDrawable
            gradientDrawable.mutate()
            gradientDrawable.colors = intArrayOf(Color.parseColor(course.teeOneColor), Color.parseColor(course.teeOneColor))
            tee_indicator_one.background = gradientDrawable

        }


        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        hole_info_recycler_view.layoutManager = layoutManager
        hole_info_recycler_view.setHasFixedSize(true)
        val dividerItemDecoration = DividerItemDecoration(this, layoutManager.orientation)
        hole_info_recycler_view.addItemDecoration(dividerItemDecoration)



        tee_indicator_one.setOnClickListener {
            runAnimation(hole_info_recycler_view, tee_indicator_one, course, 1)
            currentTeeSelected = 1

        }

        tee_indicator_two.setOnClickListener {
            runAnimation(hole_info_recycler_view, tee_indicator_two, course, 2)
            currentTeeSelected = 2

        }

        tee_indicator_three.setOnClickListener {
            runAnimation(hole_info_recycler_view, tee_indicator_three, course, 3)
            currentTeeSelected = 3

        }

        tee_indicator_four.setOnClickListener {
            runAnimation(hole_info_recycler_view, tee_indicator_four, course, 4)
            currentTeeSelected = 4
        }

    }


    fun runAnimation(recyclerView: RecyclerView, teeButton: Button, course: Course, teeNumber: Int) {

        if (currentTeeSelected > 0) {
            // then a tee is already selected and needs to have alpha changed back to .25

            when (currentTeeSelected) {
                1 -> runReduceAlphaAnimation(tee_indicator_one)
                2 -> runReduceAlphaAnimation(tee_indicator_two)
                3 -> runReduceAlphaAnimation(tee_indicator_three)
                4 -> runReduceAlphaAnimation(tee_indicator_four)
            }
        }

        teeButton.animate().alpha(1f).setDuration(resources.getInteger(R.integer.animation_dur_medium).toLong()).start()

        hole_info_how_to.visibility = View.GONE
        val controller = AnimationUtils.loadLayoutAnimation(recyclerView.context, R.anim.layout_fall_down)
        val adapter = HoleInfoAdapter(course, teeNumber, recyclerView.context)
        recyclerView.adapter = adapter
        recyclerView.layoutAnimation = controller
        adapter.notifyDataSetChanged()
        recyclerView.scheduleLayoutAnimation()
    }

    fun runReduceAlphaAnimation(button: Button) {
        button.animate().alpha(.25f).setDuration(resources.getInteger(R.integer.animation_dur_medium).toLong()).start()

    }

}
