package com.tsquaredapplications.chains2.Adapters

import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tsquaredapplications.chains2.CourseDetailsActivity
import com.tsquaredapplications.chains2.Enums.IntentExtraNames
import com.tsquaredapplications.chains2.Objects.CourseInformation
import com.tsquaredapplications.chains2.R
import kotlinx.android.synthetic.main.course_selection_list_item.view.*

class CourseInformationAdapter(courseList: MutableList<CourseInformation>):
        RecyclerView.Adapter<CourseInformationAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.course_selection_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.let {
            val currentCourse = mCourseList[position]
            it.courseNameTV.text = currentCourse.getmName()
            val location = if (currentCourse.getmState() == "null") currentCourse.getmCity()
            else  "${currentCourse.getmCity()} ${currentCourse.getmState()}"
            it.locationTV.text = location
            val numHoles = "${currentCourse.getmNumHoles()} Holes"
            it.numberHolesTV.text = numHoles
            val reviewCount = "Reviews: ${currentCourse.getmReviewCount()}"
            it.reviewCountTV.text = reviewCount
            val courseRating = currentCourse.getmDgcrRating()
            if (courseRating < 0.25) {
                it.ratingImageView.setImageResource(R.drawable.disc_0)
            } else if (courseRating < 0.75) {
                it.ratingImageView.setImageResource(R.drawable.disc_0half)
            } else if (courseRating < 1.25) {
                it.ratingImageView.setImageResource(R.drawable.disc_1)
            } else if (courseRating < 1.75) {
                it.ratingImageView.setImageResource(R.drawable.disc_1half)
            } else if (courseRating < 2.25) {
                it.ratingImageView.setImageResource(R.drawable.disc_2)
            } else if (courseRating < 2.75) {
                it.ratingImageView.setImageResource(R.drawable.disc_2half)
            } else if (courseRating < 3.25) {
                it.ratingImageView.setImageResource(R.drawable.disc_3)
            } else if (courseRating < 3.75) {
                it.ratingImageView.setImageResource(R.drawable.disc_3half)
            } else if (courseRating < 4.25) {
                it.ratingImageView.setImageResource(R.drawable.disc_4)
            } else if (courseRating < 4.75) {
                it.ratingImageView.setImageResource(R.drawable.disc_4half)
            } else {
                it.ratingImageView.setImageResource(R.drawable.disc_5)
            }
        }
    }

    val mCourseList = courseList





    override fun getItemCount(): Int {
        return mCourseList.size
    }



    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        val courseNameTV = itemView.course_name_text_view
        val locationTV = itemView.location_text_view
        val numberHolesTV = itemView.num_holes_text_view
        val reviewCountTV = itemView.ratings_count_text_view
        val ratingImageView = itemView.disc_rating_image_view

        init {
            itemView.setOnClickListener {
                val courseInformation = mCourseList[adapterPosition]
                val intent = Intent(it.context, CourseDetailsActivity::class.java)
                intent.putExtra(IntentExtraNames.COURSE_INFORMATION.toString(), courseInformation)
                startActivity(it.context, intent, null)
            }
        }

    }
}