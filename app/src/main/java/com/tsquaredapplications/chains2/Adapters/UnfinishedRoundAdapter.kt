package com.tsquaredapplications.chains2.Adapters

import android.app.AlertDialog
import android.arch.persistence.room.Room
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tsquaredapplications.chains2.Enums.IntentExtraNames
import com.tsquaredapplications.chains2.Enums.RoomDbNames
import com.tsquaredapplications.chains2.Objects.Scorecard

import com.tsquaredapplications.chains2.R
import com.tsquaredapplications.chains2.RoomDB.SavedScorecardDatabase
import com.tsquaredapplications.chains2.SpScoringActivity
import com.tsquaredapplications.chains2.Utilities.GolfUtils
import kotlinx.android.synthetic.main.unfinished_round_list_item.view.*

class UnfinishedRoundAdapter(roundsList: MutableList<Scorecard>, context: Context)
    : RecyclerView.Adapter<UnfinishedRoundAdapter.ViewHolder>() {

    private var mRoundList = roundsList
    private val mContext = context


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent?.context)
        val view = inflater.inflate(R.layout.unfinished_round_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentScorecard = mRoundList[position]
        holder.courseName.text = currentScorecard.courseName
        val currentHole = "${mContext.getString(R.string.current_hole)} ${currentScorecard.currentHole}"
        holder.currentHole.text = currentHole
        val score = "${mContext.getString(R.string.score_)} ${GolfUtils.formatScore(currentScorecard.plusMinus)}"
        holder.score.text = score

        val drawable = mContext.getDrawable(R.drawable.tee_indicator)
        val gradientDrawable = drawable as GradientDrawable
        gradientDrawable.mutate()
        gradientDrawable.colors = intArrayOf(Color.parseColor(currentScorecard.teeColor), Color.parseColor(currentScorecard.teeColor))
        holder.teeIndicator.background = gradientDrawable
    }







    override fun getItemCount(): Int {
        return mRoundList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val courseName = itemView.unfinished_round_course_name_text_view
        val currentHole = itemView.current_hole_text_view
        val score = itemView.score_text_view
        val teeIndicator = itemView.tee_indicator


        init {
            itemView.setOnClickListener {


                val builder = AlertDialog.Builder(it.context)
                builder.setNegativeButton(it.context.getString(R.string.delete)) { dialog, which ->
                            val db = Room.databaseBuilder(it.context,
                                    SavedScorecardDatabase::class.java, RoomDbNames.SAVED_SCORECARDS.toString())
                                    .allowMainThreadQueries()
                                    .build()

                            db.savedScorecardDao().removeScorecard(mRoundList[adapterPosition].courseId)
                            db.close()

                            mRoundList.removeAt(adapterPosition)
                            notifyDataSetChanged()
                        }
                        .setPositiveButton(it.context.getString(R.string.resume)) { dialog, which ->
                            val intent = Intent(it.context, SpScoringActivity::class.java)
                            intent.putExtra(IntentExtraNames.IS_ONGOING_GAME.toString(), true)
                            intent.putExtra(IntentExtraNames.SCORECARD.toString(), mRoundList[adapterPosition])
                            startActivity(it.context, intent, null)

                        }
                        .create()
                builder.show()
            }
        }
    }

}