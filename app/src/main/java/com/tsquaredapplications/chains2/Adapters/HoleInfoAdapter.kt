package com.tsquaredapplications.chains2.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import com.tsquaredapplications.chains2.Objects.Course
import android.view.View
import android.view.ViewGroup
import com.tsquaredapplications.chains2.R
import kotlinx.android.synthetic.main.hole_info_list_item.view.*

class HoleInfoAdapter(val course: Course, val teeNumber: Int, var context: Context):
    RecyclerView.Adapter<HoleInfoAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.hole_info_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return course.numHoles + 1
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(position == 0){
            holder.hole.text = context.getString(R.string.hole)
            holder.par.text = context.getString(R.string.par)
            holder.distance.text = context.getString(R.string.distance)
        } else {

            holder.hole.text = Integer.toString(position)
            when (teeNumber) {
                1 -> {
                    holder.par.text = course.teeOneHoles[position - 1].par.toString()
                    holder.distance.text = course.teeOneHoles[position - 1].distance.toString()
                }
                2 -> {
                    holder.par.text = course.teeTwoHoles[position - 1].par.toString()
                    holder.distance.text = course.teeTwoHoles[position - 1].distance.toString()
                }
                3 -> {
                    holder.par.text = course.teeThreeHoles[position - 1].par.toString()
                    holder.distance.text = course.teeThreeHoles[position - 1].distance.toString()
                }
                4 -> {
                    holder.par.text = course.teeFourHoles[position - 1].par.toString()
                    holder.distance.text = course.teeFourHoles[position - 1].distance.toString()
                }
            }
        }
    }


    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val hole = itemView.hole_info_hole
        val par = itemView.hole_info_par
        val distance = itemView.hole_info_distance
    }
}