package com.tsquaredapplications.chains2.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tsquaredapplications.chains2.Objects.Scorecard
import com.tsquaredapplications.chains2.R
import kotlinx.android.synthetic.main.sp_scorecard_list_item.view.*
import kotlin.coroutines.experimental.coroutineContext

class ScorecardListAdapter(val scorecard: Scorecard, val context: Context):
        RecyclerView.Adapter<ScorecardListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate((R.layout.sp_scorecard_list_item), parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return scorecard.getNumHoles() + 1
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // for first position do nothing. Leaves headers in text views
        if(position > 0 ) {
            holder.holeTextView.text = Integer.toString(position)
            holder.parTextView.text = Integer.toString(scorecard.getHolePar(position - 1))
            holder.strokesTextView.text = Integer.toString(scorecard.getHoleScore(position - 1))
            holder.scoreTextView.text = scorecard.getFormattedScore(scorecard.getScoreAtHole(position - 1))
        } else {
            holder.holeTextView.text = context.getString(R.string.hole)
            holder.parTextView.text = context.getString(R.string.par)
            holder.strokesTextView.text = context.getString(R.string.strokes)
            holder.scoreTextView.text = context.getString(R.string.score)
        }
    }


    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val holeTextView = itemView.sp_scorecard_hole_text_view
        val parTextView = itemView.sp_scorecard_par_text_view
        val scoreTextView = itemView.sp_scorecard_score_text_view
        val strokesTextView = itemView.sp_scorecard_strokes_text_view


    }
}