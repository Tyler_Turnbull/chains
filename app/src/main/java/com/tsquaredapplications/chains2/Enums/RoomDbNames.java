package com.tsquaredapplications.chains2.Enums;

/**
 * Created by tylerturnbull on 2/10/18.
 */

public enum RoomDbNames {
    COURSES("course_database"),
    USER_TEE_STATS_DATABASE("user_course_tee_stats_database"),
    USER_OVERALL_STATS_DATABASE("user_overall_stats_database"),
    RECENT_COURSES("recenet_course_database"),
    SAVED_SCORECARDS("saved_scorecards");

    private final String name;

    RoomDbNames(final String name){
        this.name = name;
    }


    @Override
    public String toString() {
        return name;
    }
}
