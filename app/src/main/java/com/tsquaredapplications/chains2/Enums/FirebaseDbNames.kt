package com.tsquaredapplications.chains2.Enums

/**
 * Created by Tyler on 2/25/2018.
 */

enum class FirebaseDbNames(val string: String) {
    // stats
    COURSE_TEE_STATS("course_tee_stats"),
    COURSE_OVERALL_STATS("course_overall_stats"),
    USER_OVERALL_STATS("user_overall_stats"),
    USER_TEE_STATS("user_tee_stats"),


    // usernames -> uid -> username
    USERNAMES("usernames"),

    // user_directory -> username -> uid
    USER_DIRECTORY("user_directory"),

    // friend_requests -> uid -> uids + username of requester
    // send in String[] [0] = uid [1] = username
    FRIEND_REQUESTS("friend_requests"),


    // friends -> uid -> list of friend objects
    FRIENDS("friends"),

    // friend_feed -> uid -> list of friend feeds using push
    FRIEND_FEED("friend_feed"),

    // access name for top ten scores within COURSE_TEE_STATS
    TOP_TEN_SCORES("topTenScores");


    override fun toString(): String {
        return string
    }
}
