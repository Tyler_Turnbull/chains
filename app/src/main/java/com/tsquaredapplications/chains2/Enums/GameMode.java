package com.tsquaredapplications.chains2.Enums;

/**
 * Created by tylerturnbull on 2/5/18.
 */

public enum GameMode {
    Single,
    Multi;
}
