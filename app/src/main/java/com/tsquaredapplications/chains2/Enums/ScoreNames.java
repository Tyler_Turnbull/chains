package com.tsquaredapplications.chains2.Enums;

/**
 * Created by tylerturnbull on 2/7/18.
 */

public enum ScoreNames {
    ACE,
    ALBATROSS,
    EAGLE,
    BIRDIE,
    PAR,
    BOGEY,
    DBOGEY,
    TBOGEYPLUS

}
