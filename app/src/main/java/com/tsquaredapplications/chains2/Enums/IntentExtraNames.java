package com.tsquaredapplications.chains2.Enums;

/**
 * Created by tylerturnbull on 2/10/18.
 */

public enum IntentExtraNames {

    COURSE_STATS("course-stats"),
    COURSE("course-for-round"),
    TEE_CHOICE("tee-choice"),
    FIRST_TIME("is-first-time"),
    SCORECARD("scorecard"),
    COURSE_ID("course_id"),
    COURSE_LIST("course_list"),
    USER_STATS("user_stats"),
    BEST_SCORE("best_score"),
    WORST_SCORE("worst_score"),
    FRIEND_LIST("friend_list"),
    FRIEND_STAT_LIST("friends_stats"),
    HAS_NETWORK_CONNECTION("has_network_connection"),
    LEADERBOARD_LIST("leaderboard_list"),
    LEADERBOARD_USERNAMES("leaderboard_usernames"),
    STATS_BEFORE("stats_before"),
    STATS_AFTER("stats_after"),
    HOLE_NUMBER("hole_number"),
    IS_ONGOING_GAME("is_ongoing_game"),
    BUNDLE("bundle"),
    IS_SAVED_COURSE("is_saved_course"),
    IS_RECENT_COURSES("is_recent_courses"),
    LATITUDE("lat"),
    LONGITUDE("lon"),
    PAR("par"),
    COURSE_SAVE_TYPE("course_save_type"), // 0 new, 1 saved, 2 recent
    COURSE_INFORMATION("course_information"),
    MAIN_TO_ROUNDS("main_to_rounds");

    private final String name;

    IntentExtraNames (final String name){
        this.name = name;
    }


    @Override
    public String toString() {
        return name;
    }
}
