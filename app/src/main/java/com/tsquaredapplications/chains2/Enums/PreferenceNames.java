package com.tsquaredapplications.chains2.Enums;

/**
 * Created by tylerturnbull on 2/28/18.
 */

public enum PreferenceNames {

    HAS_USERNAME("has_username"),
    USERNAME("username"),
    PROFILE_PICTURE("profile_picture");

    private final String name;

    PreferenceNames(final String name){
        this.name = name;
    }


    @Override
    public String toString() {
        return name;
    }
}
