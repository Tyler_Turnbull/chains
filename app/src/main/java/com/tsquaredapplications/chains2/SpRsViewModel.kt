package com.tsquaredapplications.chains2

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.util.Log
import com.tsquaredapplications.chains2.CourseStatsObjects.CourseTeeStats
import com.tsquaredapplications.chains2.Friends.Friend
import com.tsquaredapplications.chains2.Objects.Scorecard
import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerStats
import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerTeeStats
import com.tsquaredapplications.chains2.Utilities.*

class SpRsViewModel(application: Application) : AndroidViewModel(application) {


    var isNetworkAvailable = false
    var firstTime = true
    var username: String? = null
    var playerStatsBefore: PlayerStats? = null
    var playerStatsAfter: PlayerStats? = null
    var playerTeeStatsBefore: PlayerTeeStats? = null
    var playerTeeStatsAfter: PlayerTeeStats? = null
    var courseTeeStatsBefore: CourseTeeStats? = null
    var courseTeeStatsAfter: CourseTeeStats? = null
    var friendsList = arrayListOf<Friend>()
    var friendsStatList = arrayListOf<PlayerTeeStats>()
    lateinit var scorecard: Scorecard


    fun initSetup(scorecard: Scorecard, isFirstTime: Boolean) {
        this.scorecard = scorecard
        this.firstTime = isFirstTime
        isNetworkAvailable = NetworkUtils.isNetworkAvailable(getApplication())
        updatePlayerTeeStats()
        updatePlayerOverallStats()
        FirebaseUtils.pushToFriendFeed(getApplication(), scorecard)
        // get friends data
        if (isNetworkAvailable) getFriendsList()

    }



    fun updatePlayerTeeStats() {

        val playerTeeStatsHandler = PlayerTeeStatsHandler()
        playerTeeStatsHandler.updateStats(getApplication(), GeneralUtils.getUsername(getApplication()),
                scorecard, scorecard.courseId, scorecard.teeChoice)
    }

    fun updatePlayerOverallStats() {
        val playerOverallStatsHandler = PlayerOverallStatsHandler()
        playerOverallStatsHandler.updateStats(getApplication(), scorecard)
    }

    fun getFriendsList() {
        val friendsListGetter = FriendListGetter()
        friendsListGetter.getFriendsList()
    }

    fun updateCourseStats() {
        val courseTeeStatsHandler = CourseTeeStatsHandler()
        courseTeeStatsHandler.updateCourseTeeStats(getApplication(), scorecard, playerTeeStatsAfter!!)
    }


    inner class PlayerTeeStatsHandler : UpdatePlayerTeeStatsUtil() {
        override fun playerTeeStatsUpdated(teeStatsBefore: PlayerTeeStats?, teeStatsAfter: PlayerTeeStats) {
            playerTeeStatsBefore = teeStatsBefore
            playerTeeStatsAfter = teeStatsAfter
            updateCourseStats()
        }
    }

    inner class PlayerOverallStatsHandler : UpdatePlayerOverallStatsUtil() {
        override fun playerOverallStatsUpdated(statsBefore: PlayerStats, statsAfter: PlayerStats) {
            playerStatsBefore = statsBefore
            playerStatsAfter = statsAfter
        }

    }

    inner class CourseTeeStatsHandler : UpdateCourseStatsUtil() {
        override fun courseStatsUpdated(statsBefore: CourseTeeStats?, statsAfter: CourseTeeStats?) {
            courseTeeStatsBefore = statsBefore
            courseTeeStatsAfter = statsAfter
            Log.i(this.toString(), courseTeeStatsAfter.toString())
        }

    }

    inner class FriendListGetter : FirebaseFriendsListUtil() {
        override fun onFriendsFound(fList: ArrayList<Friend>) {
            friendsList = fList
            val friendStatsGetter = FriendsStatsGetter()
            for (i in 0 until friendsList.size) {
                friendStatsGetter.getFriendsData(friendsList[i].uid,
                        scorecard.courseId, scorecard.teeChoice)
            }

        }

    }

    inner class FriendsStatsGetter : FirebaseFriendsTeeStatsUtil() {
        override fun onFriendsDataFound(friendsStats: PlayerTeeStats) {
            friendsStatList.add(friendsStats)

        }

    }

}