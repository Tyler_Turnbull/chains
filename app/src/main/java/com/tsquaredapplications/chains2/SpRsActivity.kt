package com.tsquaredapplications.chains2

import android.arch.lifecycle.ViewModelProviders
import android.arch.persistence.room.Room
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.Menu
import android.view.MenuItem
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.tsquaredapplications.chains2.CourseStatsObjects.CourseTeeStats
import com.tsquaredapplications.chains2.Enums.FirebaseDbNames
import com.tsquaredapplications.chains2.Enums.IntentExtraNames
import com.tsquaredapplications.chains2.Enums.PreferenceNames
import com.tsquaredapplications.chains2.Enums.RoomDbNames
import com.tsquaredapplications.chains2.Fragments.*
import com.tsquaredapplications.chains2.Friends.Friend
import com.tsquaredapplications.chains2.Friends.FriendFeedItem
import com.tsquaredapplications.chains2.Objects.Scorecard
import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerStats
import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerTeeStats
import com.tsquaredapplications.chains2.RoomDB.PlayerOverallStatsDatabase
import com.tsquaredapplications.chains2.RoomDB.PlayerTeeStatsDatabase
import com.tsquaredapplications.chains2.Utilities.FirebaseUtils
import com.tsquaredapplications.chains2.Utilities.GolfUtils
import com.tsquaredapplications.chains2.Utilities.NetworkUtils
import com.tsquaredapplications.chains2.Utilities.TrendUtil
import kotlinx.android.synthetic.main.activity_sp_round_summary_.*
import java.util.*

class SpRsActivity : AppCompatActivity() {

    lateinit var scorecard: Scorecard
   lateinit var viewModel: SpRsViewModel
    var firstTime: Boolean = true



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sp_round_summary_)
        scorecard = intent.getParcelableExtra(IntentExtraNames.SCORECARD.toString())
        firstTime = intent.getBooleanExtra(IntentExtraNames.FIRST_TIME.toString(), true)
        viewModel = ViewModelProviders.of(this).get(SpRsViewModel::class.java)
        viewModel.initSetup(scorecard, firstTime)


        sp_round_summary_view_pager.adapter = ScreenSlidePagerAdapter(supportFragmentManager)
        activity_sp_round_summary_tab_layout.setupWithViewPager(sp_round_summary_view_pager)


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.round_summary_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        when (id) {
            R.id.action_sprs_finish -> {
                finishAffinity()
                startActivity(Intent(this, MainActivity::class.java))
            }
        }

        return super.onOptionsItemSelected(item)
    }



    private inner class ScreenSlidePagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment? {
            if(viewModel.firstTime){
                when (position) {
                    0 -> {
                        return SpRsFragment()
                    }
                    1 -> {
                        return SpRsCardFragment()
                    }
                    2 -> {
                        return RsFriendsStatsFragment()
                    }
                    3 -> {
                        return RsCourseLeaderboardFragment()
                    }
                }
                return null
            } else {
                when (position) {
                    0 -> {
                        return SpRsFragment()
                    }
                    1 -> {
                        return SpRsCardFragment()
                    }
                    2 -> {
                        return RsFriendsStatsFragment()
                    }
                    3 -> {
                        return RsCourseLeaderboardFragment()
                    }
                    4 -> {
                        return SpRsProgress()
                    }
                }
                return null
            }

        }


        override fun getCount(): Int {
            if(viewModel.firstTime)
                return 4
            else
                return 5
        }

        override fun getPageTitle(position: Int): CharSequence? {
            if(viewModel.firstTime){
                when (position) {
                    0 -> return getString(R.string.summary)
                    1 -> return getString(R.string.card)
                    2 -> return getString(R.string.friends)
                    3 -> return getString(R.string.leaderboard)
                }
                return null
            } else {
                when (position) {
                    0 -> return getString(R.string.summary)
                    1 -> return getString(R.string.card)
                    2 -> return getString(R.string.friends)
                    3 -> return getString(R.string.leaderboard)
                    4 -> return getString(R.string.progress)
                }
                return null
            }
        }
    }
}
