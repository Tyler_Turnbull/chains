package com.tsquaredapplications.chains2

import android.Manifest
import android.arch.persistence.room.Room
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.tsquaredapplications.chains2.Dialogs.CourseNameDialogFragment
import com.tsquaredapplications.chains2.Dialogs.ZipCodeDialogFragment
import com.tsquaredapplications.chains2.Enums.IntentExtraNames
import com.tsquaredapplications.chains2.Enums.RoomDbNames
import com.tsquaredapplications.chains2.RoomDB.SavedCourseDatabase
import com.tsquaredapplications.chains2.Utilities.DiscGolfCourseReviewUtil
import kotlinx.android.synthetic.main.activity_find_course.*
import org.json.JSONArray

class FindCourseActivity : AppCompatActivity(),
    ZipCodeDialogFragment.ZipCodeDialogListener,
    CourseNameDialogFragment.CourseNameDialogListener{

    private val ACCESS_COARSE_LOCATION_CODE = 1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_find_course)
        supportActionBar?.hide()

        checkForSavedCourses(RoomDbNames.COURSES.toString())
        checkForSavedCourses(RoomDbNames.RECENT_COURSES.toString())

        setOnClickListeners()
    }

    fun setOnClickListeners() {
        recent_courses_button.setOnClickListener {
            intent = Intent(this, CourseSelectionActivity::class.java)
            intent.putExtra(IntentExtraNames.IS_RECENT_COURSES.toString(), true)
            startActivity(intent)
        }

        saved_courses_button.setOnClickListener {
            intent = Intent(this, CourseSelectionActivity::class.java)
            intent.putExtra(IntentExtraNames.IS_SAVED_COURSE.toString(), true)
            startActivity(intent)
        }

        find_nearby_courses_button.setOnClickListener {
            findNearbyCourses()
        }

        find_courses_by_name_button.setOnClickListener {
            val dialog = CourseNameDialogFragment()
            dialog.show(fragmentManager, "CourseNameDialogFragment")
        }

        find_courses_by_zip_button.setOnClickListener {
            val dialog = ZipCodeDialogFragment()
            dialog.show(fragmentManager, "ZipCodeDialogFragment")
        }

        dgcr_logo_image_view.setOnClickListener {
            intent = Intent(Intent.ACTION_VIEW)
            intent.setData(Uri.parse(DiscGolfCourseReviewUtil.Companion.getDgcrUrl()))
            startActivity(intent)
        }


    }

    fun checkForSavedCourses(databaseName: String) {
        val db = Room.databaseBuilder(applicationContext, SavedCourseDatabase::class.java,
                databaseName).build()

        val handler = Handler()
        Thread({
            val courseList = db.savedCourseDao().loadAllCourses()
            db.close()
            handler.post {
                if (courseList.isEmpty() && databaseName == RoomDbNames.COURSES.toString()) {
                    saved_courses_button.isEnabled = false
                } else {
                    recent_courses_button.isEnabled = false
                }
            }
        })
    }

    /**
     * Checks for location permissions and if does not have permission request it
     *
     * Then, if has permission,
     */
    fun findNearbyCourses() {
        // Check location permission
        if (ContextCompat.checkSelfPermission(applicationContext,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                    ACCESS_COARSE_LOCATION_CODE)

            if (ContextCompat.checkSelfPermission(applicationContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                // User granted access to location
                val fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
                fusedLocationProviderClient.lastLocation.addOnSuccessListener {
                    if (it != null) {
                        makeDgcrRequest(DiscGolfCourseReviewUtil.Companion.makeLocationSearchString(it))
                    } else {
                        Snackbar.make(saved_courses_button,
                                "Can not find location", Snackbar.LENGTH_SHORT).show()
                    }
                }

            }
        }
    }


    fun makeDgcrRequest(searchString: String) {
        val requestQueue = Volley.newRequestQueue(applicationContext)
        val jsonArrayRequest = JsonArrayRequest(
                Request.Method.GET, searchString, null,
                Response.Listener<JSONArray> {
                    val courseList = DiscGolfCourseReviewUtil.Companion
                            .parseJsonToCourseInformationList(it)

                    if(!courseList.isEmpty()) {
                        intent = Intent(applicationContext, CourseSelectionActivity::class.java)
                        intent.putParcelableArrayListExtra(IntentExtraNames.COURSE_LIST.toString(), courseList)
                        startActivity(intent)
                    } else {
                        Snackbar.make(find_nearby_courses_button, "No courses found with that search",
                                Snackbar.LENGTH_SHORT).show()
                    }

                }, Response.ErrorListener {
            println(Snackbar.make(find_nearby_courses_button,
                    "No Courses Found", Snackbar.LENGTH_SHORT))
        }
        )

        requestQueue.add(jsonArrayRequest)
    }

    override fun onZipCodeDialogPositiveClick(zip: String?) {
        makeDgcrRequest(DiscGolfCourseReviewUtil.Companion.makeZipCodeSearchString(zip!!))
    }

    override fun onDialogNegativeClick() {
        // Do nothing.. user clicked cancel
    }

    override fun onCourseNameDialogPositiveClick(name: String?) {
        makeDgcrRequest(DiscGolfCourseReviewUtil.Companion.makeCourseNameSearchString(name!!))
    }



}
