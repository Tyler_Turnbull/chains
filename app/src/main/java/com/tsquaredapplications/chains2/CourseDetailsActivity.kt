package com.tsquaredapplications.chains2

import android.app.Activity
import android.app.AlertDialog
import android.arch.persistence.room.Room
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.constraint.ConstraintSet
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.tsquaredapplications.chains2.Enums.IntentExtraNames
import com.tsquaredapplications.chains2.Enums.RoomDbNames
import com.tsquaredapplications.chains2.Objects.Course

import com.tsquaredapplications.chains2.Objects.CourseInformation
import com.tsquaredapplications.chains2.RoomDB.SavedCourseDatabase
import com.tsquaredapplications.chains2.RoomDB.SavedScorecardDatabase
import com.tsquaredapplications.chains2.Utilities.DiscGolfCourseReviewUtil
import com.tsquaredapplications.chains2.Utilities.NetworkUtils
import kotlinx.android.synthetic.main.activity_course_details.*

class CourseDetailsActivity : AppCompatActivity(), OnMapReadyCallback {

    lateinit var mCourse: Course
    lateinit var mCourseInformation: CourseInformation
    private var mIsSavedCourse: Boolean = false
    private var mIsRecentCourse: Boolean = false
    private var mWasCourseRemoved = false
    lateinit var map: MapView
    var mapDone = false
    var uiDone = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_course_details)

        supportActionBar?.hide()

        map = mapView
        map.onCreate(savedInstanceState)



        if (intent.hasExtra(IntentExtraNames.COURSE.toString())) {
            mCourse = intent.getParcelableExtra(IntentExtraNames.COURSE.toString())
            isCourseSaved()
            if (!mIsSavedCourse && intent.hasExtra(IntentExtraNames.COURSE.toString())) mIsRecentCourse = true

            // if connected to network, try to update the course
            if (NetworkUtils.isNetworkAvailable(this)) {
                val courseRequestString = DiscGolfCourseReviewUtil.makeCourseRequestString(mCourse.courseId)
                makeDgcrCourseUpdateRequest(courseRequestString)


            }
        } else {
            // This is not a saved course, so retrieve rest of needed data from DGCR

            mCourseInformation = intent.getParcelableExtra(IntentExtraNames.COURSE_INFORMATION.toString())
            val courseDetailsRequestString = DiscGolfCourseReviewUtil.makeCourseRequestString(mCourseInformation.getmCourseId())
            makeDgcrCourseRequest(courseDetailsRequestString)
        }



        setClickListeners()
    }

    override fun onBackPressed() {
        /*
        * If course was removed we need to regather data for list on course selection activity.
        *
        * If coming from recent courses then we dont want to do this because removing courses will
        * not affect that list. Does not apply to if course was saved, as you can not save a new course
        * and return to the course selection activity with saved courses..
        * */
        if (mWasCourseRemoved && !mIsRecentCourse) {
            val handler = Handler()
            Thread({
                val db = Room.databaseBuilder(this,
                        SavedCourseDatabase::class.java,
                        RoomDbNames.COURSES.toString())
                        .build()

                val courseList = db.savedCourseDao().loadAllCourses()
                db.close()

                handler.post {
                    if (courseList.isNotEmpty()) {
                        intent = Intent(applicationContext, CourseSelectionActivity::class.java)
                        intent.putExtra(IntentExtraNames.IS_SAVED_COURSE.toString(), true)
                        intent.putExtra(IntentExtraNames.COURSE_LIST.toString(), courseList)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                    } else {
                        intent = Intent(applicationContext, FindCourseActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                    }
                }
            }).start()

        } else {
            super.onBackPressed()
        }
    }

    private fun makeDgcrCourseRequest(searchString: String) {
        val requestQueue = Volley.newRequestQueue(this)

        val jsonArrayRequest = JsonArrayRequest(Request.Method.GET, searchString,
                null, Response.Listener {
            mCourse = DiscGolfCourseReviewUtil.parseJsonToCourse(it, mCourseInformation.getmLatitude(), mCourseInformation.getmLongitude())
            updateUI()
            progress_bar_constraint_layout.visibility = View.GONE
            content_constraint_layout.visibility = View.VISIBLE


        }, Response.ErrorListener {
            Toast.makeText(this,
                    "Can not load course, check data connection,"
                            + "consider saving course before going out to play",
                    Toast.LENGTH_SHORT).show()
        })

        requestQueue.add(jsonArrayRequest)
    }

    private fun makeDgcrCourseUpdateRequest(searchString: String) {
        val requestQueue = Volley.newRequestQueue(this)

        val jsonArrayRequest = JsonArrayRequest(Request.Method.GET, searchString,
                null, Response.Listener {
            mCourse = DiscGolfCourseReviewUtil.parseJsonToCourse(it, mCourse.latitude, mCourse.longitude)
            updateUI()
            progress_bar_constraint_layout.visibility = View.GONE
            content_constraint_layout.visibility = View.VISIBLE


        }, Response.ErrorListener {
            updateUI()
            progress_bar_constraint_layout.visibility = View.GONE
            content_constraint_layout.visibility = View.VISIBLE
        })

        requestQueue.add(jsonArrayRequest)
    }

    private fun updateUI() {

        map.getMapAsync(this)

        course_name_text_view.text = mCourse.name
        var location = mCourse.city
        if (!mCourse.state.equals("null")) location += " ${mCourse.state}"
        location_text_view.text = location

        // Set image
        val courseRating = mCourse.dgcrRating
        if (courseRating < .25)
            disc_rating_image_view.setImageResource(R.drawable.disc_0)
        else if (courseRating < .75)
            disc_rating_image_view.setImageResource(R.drawable.disc_0half)
        else if (courseRating < 1.25)
            disc_rating_image_view.setImageResource(R.drawable.disc_1)
        else if (courseRating < 1.75)
            disc_rating_image_view.setImageResource(R.drawable.disc_1half)
        else if (courseRating < 2.25)
            disc_rating_image_view.setImageResource(R.drawable.disc_2)
        else if (courseRating < 2.75)
            disc_rating_image_view.setImageResource(R.drawable.disc_2half)
        else if (courseRating < 3.25)
            disc_rating_image_view.setImageResource(R.drawable.disc_3)
        else if (courseRating < 3.75)
            disc_rating_image_view.setImageResource(R.drawable.disc_3half)
        else if (courseRating < 4.25)
            disc_rating_image_view.setImageResource(R.drawable.disc_4)
        else if (courseRating < 4.75)
            disc_rating_image_view.setImageResource(R.drawable.disc_4half)
        else
            disc_rating_image_view.setImageResource(R.drawable.disc_5)

        if(mCourse.teeOneExists){
            val drawable = getDrawable(R.drawable.tee_indicator)
            val gradientDrawable = drawable as GradientDrawable
            gradientDrawable.mutate()
            gradientDrawable.colors = intArrayOf(Color.parseColor(mCourse.teeOneColor), Color.parseColor(mCourse.teeOneColor))
            tee_indicator_one.background = gradientDrawable.mutate()
        } else {
           tee_indicator_one.visibility = View.GONE


        }

        if(mCourse.teeTwoExists){
            val drawable = getDrawable(R.drawable.tee_indicator)
            val gradientDrawable = drawable as GradientDrawable
            gradientDrawable.mutate()
            gradientDrawable.colors = intArrayOf(Color.parseColor(mCourse.teeTwoColor), Color.parseColor(mCourse.teeTwoColor))
            tee_indicator_two.background = gradientDrawable
        } else {
            tee_indicator_two.visibility = View.GONE
        }

        if(mCourse.teeThreeExists){
            val drawable = getDrawable(R.drawable.tee_indicator)
            val gradientDrawable = drawable as GradientDrawable
            gradientDrawable.mutate()
            gradientDrawable.colors = intArrayOf(Color.parseColor(mCourse.teeThreeColor), Color.parseColor(mCourse.teeThreeColor))
            tee_indicator_three.background = gradientDrawable
        } else {
            tee_indicator_three.visibility = View.GONE
        }

        if(mCourse.teeFourExists){
            val drawable = getDrawable(R.drawable.tee_indicator)
            val gradientDrawable = drawable as GradientDrawable
            gradientDrawable.mutate()
            gradientDrawable.colors = intArrayOf(Color.parseColor(mCourse.teeFourColor), Color.parseColor(mCourse.teeFourColor))
            tee_indicator_four.background = gradientDrawable
        } else {
            tee_indicator_four.visibility = View.GONE
        }




        uiDone = true
        progressBarHelper()

    }

    private fun saveCourse() {
        val db = Room.databaseBuilder(this,
                SavedCourseDatabase::class.java, RoomDbNames.COURSES.toString())
                .build()

        val handler = Handler()
        Thread({
            db.savedCourseDao().insertCourses(mCourse)
            db.close()
            handler.post({
                Snackbar.make(save_course_button, getString(R.string.course_saved), Snackbar.LENGTH_SHORT).show()
            })
        }).start()
    }

    private fun removeCourse() {
        val db = Room.databaseBuilder(this,
                SavedCourseDatabase::class.java, RoomDbNames.COURSES.toString())
                .build()

        val handler = Handler()
        Thread({
            db.savedCourseDao().deleteCourses(mCourse)
            db.close()
            handler.post {
                Snackbar.make(save_course_button, getString(R.string.course_removed), Snackbar.LENGTH_SHORT).show()
            }
        }).start()
    }

    private fun isCourseSaved() {
        val db = Room.databaseBuilder(this,
                SavedCourseDatabase::class.java,
                RoomDbNames.COURSES.toString()).build()


        val handler = Handler()
        Thread({
            val course = db.savedCourseDao().loadCourse(mCourse.courseId)
            db.close()
            handler.post {

                if (course != null) {
                    save_course_button.text = getString(R.string.remove_course)
                    mIsSavedCourse = true
                }
            }
        }).start()
    }


    private fun setClickListeners() {
        dgcr_page_button.setOnClickListener {
            intent = Intent(Intent.ACTION_VIEW)
            intent.setData(Uri.parse(mCourse.dgcrMobileLink))
            startActivity(intent)
        }

        directions_button.setOnClickListener {
            val geoLocationString = mCourse.createGeoLocationString()
            val mapIntent = Intent(Intent.ACTION_VIEW, Uri.parse(geoLocationString))
            if (mapIntent.resolveActivity(packageManager) != null) {
                // has map application
                startActivity(mapIntent)
            } else {
                Toast.makeText(this, "No map application found", Toast.LENGTH_LONG).show()
            }
        }

        save_course_button.setOnClickListener {
            if (mIsSavedCourse) {
                removeCourse()
                mIsSavedCourse = false
                save_course_button.text = getString(R.string.save_course)
                mWasCourseRemoved = true
            } else {
                saveCourse()
                mIsSavedCourse = true
                mWasCourseRemoved = false
                save_course_button.text = getString(R.string.remove_course)
            }
        }

        hole_information_button.setOnClickListener {
            val holeInfoIntent = Intent(this, HoleInformationActivity::class.java)
            holeInfoIntent.putExtra(IntentExtraNames.COURSE.toString(), mCourse)
            startActivity(holeInfoIntent)
        }

        play_course_button.setOnClickListener {

            // Check to see if ongoing round at this course exists
            // if so prompt user that starting a new round will erase the old round

            val db = Room.databaseBuilder(applicationContext,
                    SavedScorecardDatabase::class.java, RoomDbNames.SAVED_SCORECARDS.toString())
                    .allowMainThreadQueries()
                    .build()

            val ongoingRound = db.savedScorecardDao().findScorecard(mCourse.courseId)
            db.close()

            if(ongoingRound == null){
                // There is no ongoing round for this course
                intent = Intent(this, RoundSetupActivity::class.java)
                intent.putExtra(IntentExtraNames.COURSE.toString(), mCourse)
                startActivity(intent)
            } else {
                // There is an ongoing round make a prompt
                val builder = AlertDialog.Builder(this)
                builder.setNeutralButton(getString(R.string.cancel), null)
                        .setNegativeButton(getString(R.string.no)) { dialog, which ->
                            val intent = Intent(this, MainActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                            // Extra is checked in main activity and will have rounds fragment brought up
                            intent.putExtra(IntentExtraNames.MAIN_TO_ROUNDS.toString(), true)
                            startActivity(intent)
                        }
                        .setPositiveButton(getString(R.string.yes)) { dialog, which ->
                            intent = Intent(this, RoundSetupActivity::class.java)
                            intent.putExtra(IntentExtraNames.COURSE.toString(), mCourse)
                            startActivity(intent)

                        }
                        .setMessage("You already have an ongoing round for this course.\n\nStarting " +
                                "a new round will overwrite the previous.\n\nDo you still want to start a " +
                                "new round on this course?")
                        .create()
                builder.show()
            }

        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        if(googleMap != null){

            val coordinates = LatLng(mCourse.latitude, mCourse.longitude)
            googleMap.addMarker(MarkerOptions()
                    .position(coordinates)
                    .title(mCourse.name)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                    .draggable(false)
                    .visible(true))

            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 10f))

            map.onResume()
            mapDone = true
            progressBarHelper()
        }
    }

    fun progressBarHelper(){
        if(mapDone && uiDone) {
            progress_bar_constraint_layout.visibility = View.INVISIBLE
            content_constraint_layout.visibility = View.VISIBLE
        }
    }
}
