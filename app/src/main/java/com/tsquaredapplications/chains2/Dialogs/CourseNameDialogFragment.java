package com.tsquaredapplications.chains2.Dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.EditText;

import com.tsquaredapplications.chains2.R;

/**
 * Created by tylerturnbull on 1/17/18.
 *
 * Dialog class for displaying an AlertDialog to user for input of desired course name
 * for searching the DGCR DB
 * .
 * Calling activity must implement {@link CourseNameDialogListener}. The listener is used to
 * transport data back to the activity.
 */

public class CourseNameDialogFragment extends DialogFragment {


    public interface CourseNameDialogListener{
        public void onCourseNameDialogPositiveClick(String name);
        public void onDialogNegativeClick();
    }

    CourseNameDialogFragment.CourseNameDialogListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            mListener = (CourseNameDialogFragment.CourseNameDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ZipCodeDialogListener");
        }

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.dialog_course_name_chooser, null))
                .setPositiveButton(R.string.submit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText mEditText = (EditText)getDialog().findViewById(R.id.course_name_edit_text);
                        mListener.onCourseNameDialogPositiveClick(mEditText.getText().toString().toLowerCase().replace(" ", "_"));
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onDialogNegativeClick();
                    }
                });

        return builder.create();

    }
}

