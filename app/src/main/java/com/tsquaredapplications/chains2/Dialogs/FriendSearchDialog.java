package com.tsquaredapplications.chains2.Dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.EditText;

import com.tsquaredapplications.chains2.R;

/**
 * Created by Tyler on 3/1/2018.
 */

public class FriendSearchDialog extends DialogFragment {

    public interface FriendSearchDialogListener{
        public void onDialogPositiveClick(String username);
    }

    FriendSearchDialogListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            mListener = (FriendSearchDialogListener) activity;
        } catch (ClassCastException e){
            throw new ClassCastException(activity.toString()
                    + " must implement FriendSearchDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.dialog_friend_search, null))
                .setPositiveButton(getString(R.string.search), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        EditText mEditText = (EditText)getDialog().findViewById(R.id.friend_seach_username_edit_text);
                        mListener.onDialogPositiveClick(mEditText.getText().toString().trim());
                    }
                })
                .setNegativeButton(getString(R.string.cancel), null);

        return builder.create();
    }
}
