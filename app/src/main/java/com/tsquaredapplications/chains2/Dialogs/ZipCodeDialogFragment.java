package com.tsquaredapplications.chains2.Dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.EditText;

import com.tsquaredapplications.chains2.R;

/**
 * Created by tylerturnbull on 1/17/18.
 *
 * Dialog class for displaying an AlertDialog to user for input of desired zip code for
 * searching the DGCR DB
 * .
 * Calling activity must implement {@link ZipCodeDialogListener}. The listener is used to
 * transport data back to the activity.
 */

public class ZipCodeDialogFragment extends DialogFragment {


    public interface ZipCodeDialogListener{
        public void onZipCodeDialogPositiveClick(String zip);
        public void onDialogNegativeClick();
    }

    ZipCodeDialogListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            mListener = (ZipCodeDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
            + " must implement ZipCodeDialogListener");
        }

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.dialog_zip_code_chooser, null))
                .setPositiveButton(R.string.submit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText mEditText = (EditText)getDialog().findViewById(R.id.zip_code_edit_text);
                        mListener.onZipCodeDialogPositiveClick( mEditText.getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onDialogNegativeClick();
                    }
                });

        return builder.create();

    }
}
