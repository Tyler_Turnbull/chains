package com.tsquaredapplications.chains2.Utilities

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.tsquaredapplications.chains2.Enums.FirebaseDbNames
import com.tsquaredapplications.chains2.Friends.Friend


abstract class FirebaseFriendsListUtil {
    protected abstract fun onFriendsFound(friendsList: ArrayList<Friend>)

    fun getFriendsList() {
        val friendsList = arrayListOf<Friend>()
        val friendsDb = FirebaseDatabase.getInstance().reference
                .child(FirebaseDbNames.FRIENDS.toString())
                .child(FirebaseAuth.getInstance().uid!!)

        friendsDb.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val data = dataSnapshot.children
                for (currentSnapshot in data) {
                    val friend = currentSnapshot.getValue(Friend::class.java)
                    if(friend != null) friendsList.add(friend)
                }

                onFriendsFound(friendsList)
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })
    }
}
