package com.tsquaredapplications.chains2.Utilities;

import com.tsquaredapplications.chains2.CourseStatsObjects.LeaderboardEntry;

import java.util.Comparator;

/**
 * Created by Tyler on 2/25/2018.
 */

public class LeadboardEntryComparator implements Comparator<LeaderboardEntry> {


    @Override
    public int compare(LeaderboardEntry entry1, LeaderboardEntry entry2) {
        return entry1.getScore() - entry2.getScore();
    }
}
