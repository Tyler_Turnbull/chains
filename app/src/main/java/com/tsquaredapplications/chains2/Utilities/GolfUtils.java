package com.tsquaredapplications.chains2.Utilities;

import com.tsquaredapplications.chains2.Enums.ScoreNames;
import com.tsquaredapplications.chains2.Objects.Scorecard;

/**
 * Created by tylerturnbull on 2/7/18.
 *
 * Determines what the golf score name for a hole is
 */

public class GolfUtils {
    public static ScoreNames determineScoreName(int score, int par) {
        switch (par) {
            case 2:
                switch (score) {
                    case 1:
                        return ScoreNames.ACE;
                    case 2:
                        return ScoreNames.PAR;
                    case 3:
                        return ScoreNames.BOGEY;
                    case 4:
                        return ScoreNames.DBOGEY;
                    default:
                        return ScoreNames.TBOGEYPLUS;
                }
            case 3:
                switch (score){
                    case 1:
                        return ScoreNames.ACE;
                    case 2:
                        return ScoreNames.BIRDIE;
                    case 3:
                        return ScoreNames.PAR;
                    case 4:
                        return ScoreNames.BOGEY;
                    case 5:
                        return ScoreNames.DBOGEY;
                    default:
                        return ScoreNames.TBOGEYPLUS;
                }
            case 4:
                switch (score){
                    case 1:
                        return ScoreNames.ACE;
                    case 2:
                        return ScoreNames.EAGLE;
                    case 3:
                        return ScoreNames.BIRDIE;
                    case 4:
                        return ScoreNames.PAR;
                    case 5:
                        return ScoreNames.BOGEY;
                    case 6:
                        return ScoreNames.DBOGEY;
                    default:
                        return ScoreNames.TBOGEYPLUS;
                }
            case 5:
                switch(score){
                    case 1:
                        return ScoreNames.ACE;
                    case 2:
                        return ScoreNames.ALBATROSS;
                    case 3:
                        return ScoreNames.EAGLE;
                    case 4:
                        return ScoreNames.BIRDIE;
                    case 5:
                        return ScoreNames.PAR;
                    case 6:
                        return ScoreNames.BOGEY;
                    case 7:
                        return ScoreNames.DBOGEY;
                    default:
                        return ScoreNames.TBOGEYPLUS;
                }
            case 6:
                switch (score){
                    case 1:
                        return ScoreNames.ACE;
                    case 2:
                        return ScoreNames.ALBATROSS;
                    case 3:
                        return ScoreNames.ALBATROSS;
                    case 4:
                        return ScoreNames.EAGLE;
                    case 5:
                        return ScoreNames.BIRDIE;
                    case 6:
                        return ScoreNames.PAR;
                    case 7:
                        return ScoreNames.BOGEY;
                    case 8:
                        return ScoreNames.DBOGEY;
                    default:
                        return ScoreNames.TBOGEYPLUS;
                }
        }
        throw new IllegalArgumentException("Can not determine score name with given inputs");
    }

    /***
     * Returns array of integers, which represent the number of
     * aces, albatross, eagles...
     *
     * Ordering of int array aligns with score names
     *
     * [0] = aces
     * [1] = albatross
     * [2] = eagles
     * [3] = birdies
     * [4] = pars
     * [5] = bogeys
     * [6] = dbl. bogeys
     * [7] = 3x bogeys +
     * @param scorecard
     * @return
     */
    public static int[] scoreNameTotals(Scorecard scorecard){
        int numHoles = scorecard.getNumHoles();
        int[] scoreNameTotals = new int[8];
        for(int i = 0; i < 8; i++){
            scoreNameTotals[i] = 0;
        }
        for(int i = 0; i < numHoles; i++){
            switch (GolfUtils.determineScoreName(scorecard.getHoleScore(i), scorecard.getHolePar(i))){
                case ACE:
                    scoreNameTotals[0]++;
                    break;
                case ALBATROSS:
                    scoreNameTotals[1]++;
                    break;
                case EAGLE:
                    scoreNameTotals[2]++;
                    break;
                case BIRDIE:
                    scoreNameTotals[3]++;
                    break;
                case PAR:
                    scoreNameTotals[4]++;
                    break;
                case BOGEY:
                    scoreNameTotals[5]++;
                    break;
                case DBOGEY:
                    scoreNameTotals[6]++;
                    break;
                case TBOGEYPLUS:
                    scoreNameTotals[7]++;
                    break;
            }
        }

        return scoreNameTotals;
    }

    public static double calculateAvgStrokes(int totalStrokes, int roundsPlayed){
        return (double) totalStrokes / (double)roundsPlayed;
    }

    public static String formatScore(int score){
        if(score > 0){
            return "+" + Integer.toString(score);
        } else if (score == 0){
            return "Even";
        } else {
            return Integer.toString(score);
        }
    }

    public static String formatScore(double score){
        if(score > 0){
            return "+" + Double.toString(score);
        } else if (score == 0){
            return "Even";
        } else {
            return Double.toString(score);
        }
    }


}
