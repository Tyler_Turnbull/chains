package com.tsquaredapplications.chains2.Utilities;

/**
 * Created by tylerturnbull on 1/18/18.
 *
 * Collection of static methods for use on username operations
 */

public class UsernameUtils {


    /**
     * Tests whether a username is valid for Firebase DB
     * @param username: Given username
     * @return : booelan, if username is valid
     */
    public static boolean isValidUsername(String username){
//        return !(username.contains(".") || username.contains("#") || username.contains("$")
//                || username.contains("[") || username.contains("]") || username.toLowerCase().equals("you"));

        for(int i = 0; i < username.length(); i++){
            if(!(username.charAt(i) <= 'Z' && username.charAt(i) >= 'A' ||
                    username.charAt(i) <= 'z' && username.charAt(i) >='a' ||
            username.charAt(i) <='9' && username.charAt(i) >= '0')){
                if(!(username.charAt(i) == '-' || username.charAt(i) == '_' || username.charAt(i) == ' ')){
                    return false;
                }
            }
        }

        return true;
    }

    public static boolean isUsernameToLong(String username){
        return (username.length() > 20);
    }

    public static boolean isUsernameToShort(String username){return (username.length() < 3);}
}
