package com.tsquaredapplications.chains2.Utilities

import android.arch.persistence.room.Room
import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.tsquaredapplications.chains2.Enums.RoomDbNames
import com.tsquaredapplications.chains2.Objects.Scorecard
import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerStats
import com.tsquaredapplications.chains2.RoomDB.PlayerOverallStatsDatabase
import com.tsquaredapplications.chains2.RoomDB.PlayerTeeStatsDatabase

abstract class UpdatePlayerOverallStatsUtil {
    abstract fun playerOverallStatsUpdated(statsBefore: PlayerStats, statsAfter: PlayerStats)

    fun updateStats(context: Context, scorecard: Scorecard){
        var playerStatsAfter: PlayerStats
        var playerStatsBefore: PlayerStats? = null

        Thread{
            val db = Room.databaseBuilder(context, PlayerOverallStatsDatabase::class.java,
                    RoomDbNames.USER_OVERALL_STATS_DATABASE.toString()).build()

            if(db.playerOverallStatsDao().loadPlayerStats().isEmpty()){
                playerStatsAfter = PlayerStats.firstTime(scorecard, FirebaseAuth.getInstance().uid!!)
            }else{
                playerStatsBefore = db.playerOverallStatsDao().loadPlayerStats()[0]
                playerStatsAfter = PlayerStats.mergeStats(scorecard, playerStatsBefore!!,
                        TrendUtil.calculateTrendValue(scorecard.getTotalStrokes(),
                                GolfUtils.calculateAvgStrokes(playerStatsBefore!!.totalStrokes,
                                        playerStatsBefore!!.numRoundsPlayed)))

            }

            db.playerOverallStatsDao().insertPlayerStats(playerStatsAfter)
            db.close()

            FirebaseUtils.savePlayerOverallStats(playerStatsAfter)
        }.start()

    }
}