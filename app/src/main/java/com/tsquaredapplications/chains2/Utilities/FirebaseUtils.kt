package com.tsquaredapplications.chains2.Utilities

import android.app.ProgressDialog
import android.content.Context
import android.net.Uri
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.tsquaredapplications.chains2.CourseStatsObjects.CourseTeeStats
import com.tsquaredapplications.chains2.Enums.FirebaseDbNames
import com.tsquaredapplications.chains2.Enums.FirebaseStorageNames
import com.tsquaredapplications.chains2.Friends.Friend
import com.tsquaredapplications.chains2.Friends.FriendFeedItem
import com.tsquaredapplications.chains2.Objects.Scorecard
import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerStats
import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerTeeStats
import java.util.*

/**
 * Created by Tyler on 2/25/2018.
 */

object FirebaseUtils {

    private val LOG_TAG = FirebaseUtils::class.java.name

    fun savePlayerTeeStats(courseId: Int, teeChoice: Int, stats: PlayerTeeStats) {
        val database: DatabaseReference
        database = FirebaseDatabase.getInstance().reference
        database.child(FirebaseDbNames.USER_TEE_STATS.toString())
                .child(FirebaseAuth.getInstance().uid!!)
                .child(courseId.toString())
                .child(teeChoice.toString())
                .setValue(stats)

    }


    fun savePlayerOverallStats(stats: PlayerStats) {
        val db = FirebaseDatabase.getInstance().reference

        db.child(FirebaseDbNames.USER_OVERALL_STATS.toString())
                .child(FirebaseAuth.getInstance().uid!!)
                .setValue(stats)
    }

    fun pushToFriendFeed(context: Context, scorecard: Scorecard) {
        val db = FirebaseDatabase.getInstance().reference
                .child(FirebaseDbNames.FRIENDS.toString())
                .child(FirebaseAuth.getInstance().uid!!)

        db.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                // Do Nothing
            }

            override fun onDataChange(p0: DataSnapshot?) {
                p0?.let {
                    val cal = Calendar.getInstance()
                    val date = "${cal.get(Calendar.MONTH)}/${cal.get(Calendar.DAY_OF_MONTH)}/${cal.get(Calendar.YEAR)}"
                    val friends = mutableListOf<Friend?>()
                    val data = it.children
                    for (d: DataSnapshot in data) {
                        friends.add(d.getValue(Friend::class.java))
                    }

                    val friendFeedItem = FriendFeedItem(
                            GeneralUtils.getUsername(context),
                            FirebaseAuth.getInstance().uid,
                            scorecard.courseName,
                            scorecard.getTotalStrokes(),
                            scorecard.plusMinus,
                            date)

                    for (friend in friends) {
                        FirebaseDatabase.getInstance().reference
                                .child(FirebaseDbNames.FRIEND_FEED.toString())
                                .child(friend?.uid)
                                .push()
                                .setValue(friendFeedItem)
                    }
                }
            }

        })

    }

    fun eraseUserStats() {
        val userTeeStatsDb = FirebaseDatabase.getInstance().reference
                .child(FirebaseDbNames.USER_TEE_STATS.toString())
                .child(FirebaseAuth.getInstance().uid)

        userTeeStatsDb.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                // Do Nothing
            }

            override fun onDataChange(data: DataSnapshot?) {
                if (data != null) {
                    if (data.exists()) {
                        val data = data.children
                        for (currentSnapshot in data) {
                            Log.i(this.toString(), "Course id is ${currentSnapshot.key}")
                            val teesPlayed = currentSnapshot.children
                            for (tee in teesPlayed) {
                                Log.i(this.toString(), "Played tee ${tee.key}")
                                val teeStats = tee.getValue(PlayerTeeStats::class.java)
                                Log.i(this.toString(), "Total strokes are ${teeStats?.totalStrokes}")
                                if (teeStats != null)
                                    FirebaseUtils.eraseUserStatsFromCourseStats(teeStats, currentSnapshot.key, tee.key)

                            }


                        }
                    }
                }
            }

        })
    }

    fun eraseUserStatsFromCourseStats(userStats: PlayerTeeStats, courseId: String, teeChoice: String) {
        val courseStatsDb = FirebaseDatabase.getInstance().reference
                .child(FirebaseDbNames.COURSE_TEE_STATS.toString())
                .child(courseId)
                .child(teeChoice)

        courseStatsDb.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                // Do nothing
            }

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                if (dataSnapshot != null)
                    if (dataSnapshot.exists()) {
                        val courseTeeStats = dataSnapshot.getValue(CourseTeeStats::class.java)
                        Log.i(this.toString(), "Course id is ${courseId}")
                        Log.i(this.toString(), "Total course strokes ${courseTeeStats?.totalStrokes}")

                    }
            }

        })
    }



}
