package com.tsquaredapplications.chains2.Utilities

import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.tsquaredapplications.chains2.Enums.FirebaseDbNames

class FirebaseDbRefUtil {
    companion object {
        fun getUserOverallStatsDbRef(userId: String): DatabaseReference{
            return FirebaseDatabase.getInstance().reference
                    .child(FirebaseDbNames.USER_OVERALL_STATS.toString())
                    .child(userId)
        }
    }
}