package com.tsquaredapplications.chains2.Utilities

class TrendUtil {
    companion object {
        fun calculateTrendValue(roundStrokes: Int, overallAvgStrokes: Double): Int {
            if(roundStrokes.toDouble() - overallAvgStrokes > 2)
                return 1
            else if (roundStrokes.toDouble() - overallAvgStrokes < -2)
                return -1
            else
                return 0
        }
    }
}