package com.tsquaredapplications.chains2.Utilities

import android.location.Location
import android.util.Log
import com.tsquaredapplications.chains2.Objects.Course
import com.tsquaredapplications.chains2.Objects.CourseInformation
import com.tsquaredapplications.chains2.Objects.Hole
import org.json.JSONArray
import org.json.JSONException
import java.math.BigInteger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.ArrayList

class DiscGolfCourseReviewUtil {


    companion object {
        // DGCR api params
        private const val BASE_URL_STRING = "https://www.dgcoursereview.com/api_test/index.php/?key="
        private const val API_KEY = "bx7cuueiirzwq828v39j6qcw"
        private const val API_SECRET = "kml6g3f28yqs"
        private const val DGCR_URL_STRING = "https://www.dgcoursereview.com/"

        // Max number of courses returned in results
        // min is 1, max is 25
        val COURSE_LIMIT = 20

        // Maximum radius for results
        // min is 1 max is 25
        val RADIUS = 20

        fun convertPassMd5(input: String): String {
            var password: String? = null
            try {
                var mdEnc = MessageDigest.getInstance("MD5")
                mdEnc.update(input.toByteArray(), 0, input.length)
                password = BigInteger(1, mdEnc.digest()).toString(16)
                password.let {
                    while (it.length < 32) {
                        password = "0" + password
                    }
                }


            } catch (e: NoSuchAlgorithmException) {
                e.printStackTrace()
            }

            return password.toString()
        }


        fun parseJsonToCourseInformationList(jsonArray: JSONArray): ArrayList<CourseInformation> {

            val courseList = arrayListOf<CourseInformation>()

            for (i in 0 until jsonArray.length()) {
                try {
                    val currentObject = jsonArray.getJSONObject(i)

                    val course = CourseInformation(
                            currentObject.getString("name"),
                            currentObject.getString("city"),
                            currentObject.getString("state"),
                            currentObject.getInt("holes"),
                            currentObject.getInt("course_id"),
                            currentObject.getString("dgcr_mobile_url"),
                            currentObject.getInt("reviews"),
                            currentObject.getDouble("rating"),
                            currentObject.getDouble("latitude"),
                            currentObject.getDouble("longitude")

                    )

                    courseList.add(course)
                } catch (e: JSONException) {
                    Log.e(this::class.java.name, "Problem parsing JSON to Course Information")
                    e.printStackTrace()
                }
            }

            return courseList
        }

        fun parseJsonToCourse(jsonArray: JSONArray, lat: Double, lon: Double): Course {
            val course = Course()
            try {

                val courseDetails = jsonArray.getJSONObject(0)
                course.teeOneColor = courseDetails.getString("tee_1_clr")
                if (course.teeOneColor != "") course.teeOneColor = "#${course.teeOneColor}"
                course.teeTwoColor = courseDetails.getString("tee_2_clr")
                if (course.teeTwoColor != "") course.teeTwoColor = "#${course.teeTwoColor}"
                course.teeThreeColor = courseDetails.getString("tee_3_clr")
                if (course.teeThreeColor != "") course.teeThreeColor = "#${course.teeThreeColor}"
                course.teeFourColor = courseDetails.getString("tee_4_clr")
                if (course.teeFourColor != "") course.teeFourColor = "#${course.teeFourColor}"


                val teeOneHoles = mutableListOf<Hole>()
                val teeTwoHoles = mutableListOf<Hole>()
                val teeThreeHoles = mutableListOf<Hole>()
                val teeFourHoles = mutableListOf<Hole>()

                for (i in 1 until jsonArray.length()) {
                    val currentHole = jsonArray.getJSONObject(i)
                    if (course.teeOneColor != "") {
                        teeOneHoles.add(Hole(currentHole.getInt("hole_num"),
                                currentHole.getInt("tee_1_par"), currentHole.getInt("tee_1_len")))
                    }

                    if (course.teeTwoColor != "") {
                        teeTwoHoles.add(Hole(currentHole.getInt("hole_num"),
                                currentHole.getInt("tee_2_par"), currentHole.getInt("tee_2_len")))
                    }

                    if (course.teeThreeColor != "") {
                        teeThreeHoles.add(Hole(currentHole.getInt("hole_num"),
                                currentHole.getInt("tee_3_par"), currentHole.getInt("tee_3_len")))
                    }

                    if (course.teeFourColor != "") {
                        teeFourHoles.add(Hole(currentHole.getInt("hole_num"),
                                currentHole.getInt("tee_4_par"), currentHole.getInt("tee_4_len")))
                    }

                }

                course.name = courseDetails.getString("name")


                course.city = courseDetails.getString("city")
                course.state = courseDetails.getString("state")
                course.numHoles = jsonArray.length() - 1
                course.courseId = courseDetails.getInt("course_id")
                course.dgcrMobileLink = courseDetails.getString("dgcr_mobile_url")
                course.reviewCount = courseDetails.getInt("reviews")
                course.dgcrRating = courseDetails.getDouble("rating")
                course.latitude = lat
                course.longitude = lon
                course.teeOneHoles = teeOneHoles
                course.teeTwoHoles = teeTwoHoles
                course.teeThreeHoles = teeThreeHoles
                course.teeFourHoles = teeFourHoles
                course.teeOneExists = course.teeOneColor != "" && course.teeOneHoles[0].getPar() != 0
                course.teeTwoExists = course.teeTwoColor != "" && course.teeTwoHoles[0].getPar() != 0
                course.teeThreeExists = course.teeThreeColor != "" && course.teeThreeHoles[0].getPar() != 0
                course.teeFourExists = course.teeFourColor != "" && course.teeFourHoles[0].getPar() != 0



            } catch (e: JSONException) {
                Log.e(this::class.java.name, "Problem parsing JSON to Course")
                e.printStackTrace()
            }

            return course

        }

        fun makeCourseRequestString(courseId: Int): String {
            return "$BASE_URL_STRING$API_KEY&mode=holeinfo&id=$courseId&sig=" +
                    convertPassMd5("$API_KEY${API_SECRET}holeinfo")
        }

        fun makeLocationSearchString(location: Location): String{
            return "$BASE_URL_STRING$API_KEY&mode=near_rad&lat=${location.latitude}" +
                    "&lon=${location.longitude}&limit=$COURSE_LIMIT&rad=$RADIUS" +
                    "&sig=${ convertPassMd5("$API_KEY${API_SECRET}near_rad")}"
        }

        fun makeZipCodeSearchString(zip: String): String{
            return "$BASE_URL_STRING$API_KEY&mode=findzip&zip=$zip&limit=$COURSE_LIMIT" +
                    "&rad=$RADIUS&sig=${convertPassMd5("$API_KEY${API_SECRET}findzip")}"
        }

        fun makeCourseNameSearchString(name: String): String {
            return "$BASE_URL_STRING$API_KEY&mode=findname&name=$name&sig=" +
                    convertPassMd5("$API_KEY${API_SECRET}findname")
        }

        fun getDgcrUrl(): String =  DGCR_URL_STRING

    }
}