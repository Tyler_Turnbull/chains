package com.tsquaredapplications.chains2.Utilities

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.tsquaredapplications.chains2.Enums.FirebaseDbNames
import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerTeeStats

abstract class FirebaseFriendsTeeStatsUtil {
    protected abstract fun onFriendsDataFound(friendsStats: PlayerTeeStats)

    fun getFriendsData(uid: String, courseId: Int, teeChoice: Int){
        val statsDb = FirebaseDatabase.getInstance().reference
                .child(FirebaseDbNames.USER_TEE_STATS.toString())
                .child(uid)
                .child(courseId.toString())
                .child(teeChoice.toString())

        statsDb.addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onDataChange(dataSnapshot: DataSnapshot?) {
               dataSnapshot.let {
                   val playerStats = it?.getValue(PlayerTeeStats::class.java)
                   if(playerStats != null) onFriendsDataFound(playerStats)

               }
            }

            override fun onCancelled(p0: DatabaseError?) {
                // Do nothing
            }

        })
    }
}