package com.tsquaredapplications.chains2.Utilities

import com.github.mikephil.charting.data.Entry
import com.tsquaredapplications.chains2.Objects.Scorecard

class MPChartUtil {
    companion object {
        fun createEntriesForScoreLineChart(scorecard: Scorecard): List<Entry> {

            var entries = ArrayList<Entry>()
            var score = 0
            for(hole in 0 until scorecard.getNumHoles()){
                score += scorecard.getHoleScore(hole) - scorecard.getHolePar(hole)
                entries.add(Entry((hole + 1).toFloat(), score.toFloat()))
            }

            return entries
        }
    }
}