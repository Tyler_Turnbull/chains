package com.tsquaredapplications.chains2.Utilities

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

import com.tsquaredapplications.chains2.Enums.PreferenceNames

import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.ArrayList
import java.util.prefs.Preferences

/**
 * Created by tylerturnbull on 2/7/18.
 *
 * For general utilities
 */

object GeneralUtils {


    fun roundTwoPlacesString(num: Double): String {
        val df = DecimalFormat("#.00")
        df.roundingMode = RoundingMode.HALF_EVEN
        return df.format(num)
    }

    fun roundTwoPlaces(num: Double): Double{
        val df = DecimalFormat("#.00")
        df.roundingMode = RoundingMode.HALF_EVEN
        return df.format(num).toDouble()
    }




    fun getUsername(context: Context): String {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        return prefs.getString(PreferenceNames.USERNAME.toString(), "")

    }
}
