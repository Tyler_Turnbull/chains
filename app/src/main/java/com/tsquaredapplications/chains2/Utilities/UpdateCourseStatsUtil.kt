package com.tsquaredapplications.chains2.Utilities

import android.content.Context
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.tsquaredapplications.chains2.CourseStatsObjects.CourseTeeStats
import com.tsquaredapplications.chains2.Enums.FirebaseDbNames
import com.tsquaredapplications.chains2.Objects.Course
import com.tsquaredapplications.chains2.Objects.Scorecard
import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerTeeStats

abstract class UpdateCourseStatsUtil {
    abstract fun courseStatsUpdated(statsBefore: CourseTeeStats?, statsAfter: CourseTeeStats?)

    fun updateCourseTeeStats(context: Context, scorecard: Scorecard, playerTeeStatsAfter: PlayerTeeStats){

        var courseTeeStatsBefore: CourseTeeStats? = null
        var courseTeeStatsAfter: CourseTeeStats? = null

        Log.i(this.toString(), FirebaseDbNames.COURSE_TEE_STATS.toString())

        val db = FirebaseDatabase.getInstance().reference
                .child(FirebaseDbNames.COURSE_TEE_STATS.toString())
                .child(scorecard.courseId.toString())
                .child(scorecard.teeChoice.toString())

        db.runTransaction(object : Transaction.Handler{
            override fun onComplete(p0: DatabaseError?, p1: Boolean, p2: DataSnapshot?) {
                courseStatsUpdated(courseTeeStatsBefore, courseTeeStatsAfter)
            }

            override fun doTransaction(p0: MutableData?): Transaction.Result {
                courseTeeStatsBefore = p0?.getValue(CourseTeeStats::class.java)
                if(courseTeeStatsBefore == null){
                    // First time course played
                    courseTeeStatsAfter = CourseTeeStats.firstTime(scorecard,
                            GeneralUtils.getUsername(context), FirebaseAuth.getInstance().uid!!)
                } else {
                    // Not first time course has been played
                    courseTeeStatsAfter = CourseTeeStats.mergeStats(scorecard,
                            courseTeeStatsBefore!!, playerTeeStatsAfter,
                            FirebaseAuth.getInstance().uid!!)
                }

                p0?.value = courseTeeStatsAfter
                return Transaction.success(p0)

            }

        })

    }
}