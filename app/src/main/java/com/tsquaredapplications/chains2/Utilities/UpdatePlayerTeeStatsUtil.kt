package com.tsquaredapplications.chains2.Utilities

import android.arch.persistence.room.Room
import android.content.Context
import com.tsquaredapplications.chains2.Enums.RoomDbNames
import com.tsquaredapplications.chains2.Objects.Scorecard
import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerTeeStats
import com.tsquaredapplications.chains2.RoomDB.PlayerTeeStatsDatabase

abstract class UpdatePlayerTeeStatsUtil {
    abstract fun playerTeeStatsUpdated(teeStatsBefore: PlayerTeeStats?, teeStatsAfter: PlayerTeeStats)

    fun updateStats(context: Context,username: String, scorecard: Scorecard, courseId: Int, teeChoice: Int){
        Thread{

            var playerTeeStatsBefore: PlayerTeeStats? = null
            var playerTeeStatsAfter: PlayerTeeStats

            val db = Room.databaseBuilder(context, PlayerTeeStatsDatabase::class.java,
                    RoomDbNames.USER_TEE_STATS_DATABASE.toString()).build()

            if(db.playerTeeStatsDao().findCourseStats(courseId, teeChoice) == null){
                playerTeeStatsAfter = PlayerTeeStats.firstTime(scorecard, courseId, username)
                db.playerTeeStatsDao().insertCourseStats(playerTeeStatsAfter)

            }else{
                playerTeeStatsBefore = db.playerTeeStatsDao().findCourseStats(courseId, teeChoice)
                playerTeeStatsAfter = PlayerTeeStats.mergeStats(scorecard, playerTeeStatsBefore, username)
                db.playerTeeStatsDao().insertCourseStats(playerTeeStatsAfter)
            }

            db.close()

            FirebaseUtils.savePlayerTeeStats(courseId, teeChoice, playerTeeStatsAfter)

            playerTeeStatsUpdated(playerTeeStatsBefore, playerTeeStatsAfter)
        }.start()
    }
}