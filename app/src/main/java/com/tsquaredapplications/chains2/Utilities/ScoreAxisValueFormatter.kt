package com.tsquaredapplications.chains2.Utilities

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import java.text.DecimalFormat

class ScoreAxisValueFormatter: IAxisValueFormatter {
    private  var mFormat = DecimalFormat("###,###,##0") // use one decimal

    override fun getFormattedValue(value: Float, axis: AxisBase?): String {
        when{
            (value.toInt() == 0) -> return "Even"
            (value.toInt() > 0) -> return "+${mFormat.format(value)}"
            else -> return mFormat.format(value)
        }
    }
}