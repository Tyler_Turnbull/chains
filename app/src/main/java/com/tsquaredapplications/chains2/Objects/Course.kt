package com.tsquaredapplications.chains2.Objects

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import java.util.ArrayList

/**
 * Created by tylerturnbull on 1/16/18.
 *
 *  Object for storing complete details on a given course from
 *  DGCR API.
 */
@Entity(tableName = "saved_courses")
data class Course(
        var name: String = "",
        var city: String = "",
        var state: String = "",
        var numHoles: Int = 0,
        @PrimaryKey var courseId: Int = 0,
        var dgcrMobileLink: String = "",
        var reviewCount: Int = 0,
        var dgcrRating: Double = 0.0,
        var latitude: Double = 0.0,
        var longitude: Double = 0.0,
        var teeOneColor: String = " ",
        var teeOneExists: Boolean = false,
        var teeOneHoles: MutableList<Hole> = mutableListOf(),
        var teeTwoColor: String = " ",
        var teeTwoExists: Boolean = false,
        var teeTwoHoles: MutableList<Hole> = mutableListOf(),
        var teeThreeColor: String = " ",
        var teeThreeExists: Boolean = false,
        var teeThreeHoles: MutableList<Hole> = mutableListOf(),
        var teeFourColor: String = " ",
        var teeFourExists: Boolean = false,
        var teeFourHoles: MutableList<Hole> = mutableListOf()
        ) : Parcelable {




    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(city)
        parcel.writeString(state)
        parcel.writeInt(numHoles)
        parcel.writeInt(courseId)
        parcel.writeString(dgcrMobileLink)
        parcel.writeInt(reviewCount)
        parcel.writeDouble(dgcrRating)
        parcel.writeDouble(latitude)
        parcel.writeDouble(longitude)
        parcel.writeString(teeOneColor)
        parcel.writeByte(if (teeOneExists) 1 else 0)
        parcel.writeTypedList(teeOneHoles)
        parcel.writeString(teeTwoColor)
        parcel.writeByte(if (teeTwoExists) 1 else 0)
        parcel.writeTypedList(teeTwoHoles)
        parcel.writeString(teeThreeColor)
        parcel.writeByte(if (teeThreeExists) 1 else 0)
        parcel.writeTypedList(teeThreeHoles)
        parcel.writeString(teeFourColor)
        parcel.writeByte(if (teeFourExists) 1 else 0)
        parcel.writeTypedList(teeFourHoles)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Course> {
        override fun createFromParcel(parcel: Parcel): Course {
            val name = parcel.readString()
            val city = parcel.readString()
            val state = parcel.readString()
            val numHoles = parcel.readInt()
            val courseId = parcel.readInt()
            val dgcrMobileLink = parcel.readString()
            val reviewCount = parcel.readInt()
            val dgcrRating = parcel.readDouble()
            val latitude = parcel.readDouble()
            val longitude = parcel.readDouble()
            val teeOneColor = parcel.readString()
            val teeOneExists = parcel.readByte() != 0.toByte()
            var teeOneHoles = mutableListOf<Hole>()
            parcel.readTypedList(teeOneHoles, Hole.CREATOR)
            val teeTwoColor = parcel.readString()
            val teeTwoExists = parcel.readByte() != 0.toByte()
            var teeTwoHoles = mutableListOf<Hole>()
            parcel.readTypedList(teeTwoHoles, Hole.CREATOR)
            val teeThreeColor = parcel.readString()
            val teeThreeExists = parcel.readByte() != 0.toByte()
            var teeThreeHoles = mutableListOf<Hole>()
            parcel.readTypedList(teeThreeHoles, Hole.CREATOR)
            val teeFourColor = parcel.readString()
            val teeFourExists = parcel.readByte() != 0.toByte()
            var teeFourHoles = mutableListOf<Hole>()
            parcel.readTypedList(teeFourHoles, Hole.CREATOR)
            return Course(name, city, state, numHoles, courseId, dgcrMobileLink,
                    reviewCount, dgcrRating, latitude, longitude, teeOneColor,
                    teeOneExists, teeOneHoles, teeTwoColor, teeTwoExists,
                    teeTwoHoles, teeThreeColor, teeThreeExists, teeThreeHoles,
                    teeFourColor, teeFourExists, teeFourHoles)
        }

        override fun newArray(size: Int): Array<Course?> {
            return arrayOfNulls(size)
        }

        private val HOLE_CREATOR = object : Parcelable.Creator<Hole> {
            override fun createFromParcel(source: Parcel): Hole {
                return Hole(source)
            }

            override fun newArray(size: Int): Array<Hole?> {
                return arrayOfNulls(size)
            }
        }
    }


    fun createGeoLocationString(): String {
        return "geo:$latitude, $longitude?q=$latitude, $longitude ($name)"
    }



}