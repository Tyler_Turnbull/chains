package com.tsquaredapplications.chains2.Objects

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

@Entity(tableName = "saved_scorecards")
data class Scorecard(
        var scores: ArrayList<Int> = arrayListOf(),
        var holeList: MutableList<Hole> = mutableListOf(),
        var plusMinus: Int = 0,
        var currentHole: Int = 0,
        var teeChoice: Int = 0,
        @PrimaryKey var courseId: Int = 0,
        var courseName: String = "",
        var teeColor: String = ""
) : Parcelable {

    constructor(scorecard: Scorecard): this(scorecard.scores, scorecard.holeList,
            scorecard.plusMinus, scorecard.currentHole, scorecard.teeChoice,
            scorecard.courseId, scorecard.courseName, scorecard.teeColor)


    constructor(course: Course, teeChoice: Int) : this() {
        this.plusMinus = 0
        this.currentHole = 0
        this.teeChoice = teeChoice
        this.courseId = course.courseId
        this.courseName = course.name

        when (teeChoice) {
            1 -> {
                this.holeList = course.teeOneHoles
                this.teeColor = course.teeOneColor
            }
            2 -> {
                this.holeList = course.teeTwoHoles
                this.teeColor = course.teeTwoColor
            }
            3 -> {
                this.holeList = course.teeThreeHoles
                this.teeColor = course.teeThreeColor
            }
            else -> {
                this.holeList = course.teeFourHoles
                this.teeColor = course.teeFourColor
            }
        }



        for(i in 0 until holeList.size){
            this.scores.add(holeList.get(i).getPar())
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(plusMinus)
        parcel.writeInt(currentHole)
        parcel.writeInt(teeChoice)
        parcel.writeInt(courseId)
        parcel.writeString(courseName)
        parcel.writeString(teeColor)
        parcel.writeTypedList(holeList)
        parcel.writeList(scores)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Scorecard> {
        override fun createFromParcel(parcel: Parcel): Scorecard {

            val scorecard = Scorecard()
            scorecard.plusMinus = parcel.readInt()
            scorecard.currentHole = parcel.readInt()
            scorecard.teeChoice = parcel.readInt()
            scorecard.courseId = parcel.readInt()
            scorecard.courseName = parcel.readString()
            scorecard.teeColor = parcel.readString()
            val holeList = mutableListOf<Hole>()
            parcel.readTypedList(holeList, Hole.CREATOR)
            scorecard.holeList = holeList
            val scores: ArrayList<Int> = arrayListOf()
            parcel.readList(scores, Int::class.java.classLoader)
            scorecard.scores = scores

            return scorecard

        }

        override fun newArray(size: Int): Array<Scorecard?> {
            return arrayOfNulls(size)
        }
    }

    fun getNumHoles() = holeList.size

    fun incrementHoleScore(holeNumber: Int){
        scores[holeNumber] += 1
        plusMinus++
    }

    fun decrementHoleScore(holeNumber: Int){
        scores[holeNumber] -= 1
        plusMinus--
    }

    fun getTotalStrokes(): Int{
        var strokes = 0
        for(i in 0 until holeList.size){
            strokes += scores[i]
        }

        return strokes
    }

    fun getHoleScore(holeNumber: Int): Int {
        return scores[holeNumber]
    }

    fun getHolePar(holeNumber: Int): Int {
        return holeList[holeNumber].getPar()
    }

    fun getScoreAtHole(holeNumber: Int): Int {
        var score = 0
        for (hole in 0..holeNumber){
            score += getHoleScore(hole) - getHolePar(hole)
        }

        return score
    }



    fun getFormattedScore(score: Int): String =
            when{
                (score == 0) -> "Even"
                (score > 0) -> "+$score"
                else -> "$score"
            }

    fun getCoursePar(): Int {
        var par = 0
        for( i in 0 until getNumHoles()) par += holeList[i].getPar()
        return par
    }

}