package com.tsquaredapplications.chains2.Objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by tylerturnbull on 1/16/18.
 *
 * Models a hole on a {@link Course}
 */

public class Hole implements Parcelable{

    private int mNumber;
    private int mPar;
    private int mDistance;

    public Hole(int number, int par, int distance){
        mNumber = number;
        mPar = par;
        mDistance = distance;
    }

    public Hole(Parcel in){
        mNumber = in.readInt();
        mPar = in.readInt();
        mDistance = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i){
        parcel.writeInt(mNumber);
        parcel.writeInt(mPar);
        parcel.writeInt(mDistance);
    }

    public static final Creator<Hole> CREATOR = new Creator<Hole>() {
        @Override
        public Hole createFromParcel(Parcel source) {
            return new Hole(source);
        }

        @Override
        public Hole[] newArray(int size) {
            return new Hole[size];
        }
    };



    public int getNumber() {
        return mNumber;
    }

    public void setNumber(int mNumber) {
        this.mNumber = mNumber;
    }

    public int getPar() {
        return mPar;
    }

    public void setPar(int mPar) {
        this.mPar = mPar;
    }

    public int getDistance() {
        return mDistance;
    }

    public void setDistance(int mDistance) {
        this.mDistance = mDistance;
    }

    @Override
    public String toString() {
        return "Hole{" +
                "mNumber=" + mNumber +
                ", mPar=" + mPar +
                ", mDistance=" + mDistance +
                '}';
    }
}
