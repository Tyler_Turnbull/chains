package com.tsquaredapplications.chains2.Objects;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by tylerturnbull on 1/17/18.
 *
 * Similar to a {@link Course} object except this has no information on the holes
 * of the course. This is due to the way thay DGCR API works. When searching for
 * courses it returns a list of details on each course in this manner. As where when calling
 * the API for a specific courses full information it also returns the details for holes.
 */

public class CourseInformation implements Parcelable {

    private String mName;
    private String mCity;
    private String mState;
    private int mNumHoles;
    private int mCourseId;
    private String mDgcrMobileLink;
    private int mReviewCount;
    private double mDgcrRating;
    private double mLatitude;
    private double mLongitude;




    public CourseInformation(String mName, String mCity, String mState,
                             int mNumHoles, int mCourseId, String mDgcrMobileLink,
                             int mReviewCount, double mDgcrRating, double mLatitude,
                             double mLongitude) {
        this.mName = mName;
        this.mCity = mCity;
        this.mState = mState;
        this.mNumHoles = mNumHoles;
        this.mCourseId = mCourseId;
        this.mDgcrMobileLink = mDgcrMobileLink;
        this.mReviewCount = mReviewCount;
        this.mDgcrRating = mDgcrRating;
        this.mLatitude = mLatitude;
        this.mLongitude = mLongitude;
    }

    @Override
    public int describeContents(){return 0;}


    @Override
    public void writeToParcel(Parcel parcel, int i){
        parcel.writeString(mName);
        parcel.writeString(mCity);
        parcel.writeString(mState);
        parcel.writeInt(mNumHoles);
        parcel.writeInt(mCourseId);
        parcel.writeString(mDgcrMobileLink);
        parcel.writeInt(mReviewCount);
        parcel.writeDouble(mDgcrRating);
        parcel.writeDouble(mLatitude);
        parcel.writeDouble(mLongitude);


    }

    public static final Parcelable.Creator<CourseInformation> CREATOR = new Parcelable.Creator<CourseInformation>() {
        @Override
        public CourseInformation createFromParcel(Parcel source) {
            return new CourseInformation(source);
        }

        @Override
        public CourseInformation[] newArray(int size) {
            return new CourseInformation[size];
        }
    };

    private static final Parcelable.Creator<Hole> HOLE_CREATOR = new Parcelable.Creator<Hole>() {
        @Override
        public Hole createFromParcel(Parcel source) {
            return new Hole(source);
        }

        @Override
        public Hole[] newArray(int size) {
            return new Hole[size];
        }
    };

    private CourseInformation(Parcel in) {

        ArrayList<Hole> tempList = new ArrayList<>();
        this.mName = in.readString();
        this.mCity = in.readString();
        this.mState = in.readString();
        this.mNumHoles = in.readInt();
        this.mCourseId = in.readInt();
        this.mDgcrMobileLink = in.readString();
        this.mReviewCount = in.readInt();
        this.mDgcrRating = in.readDouble();
        this.mLatitude = in.readDouble();
        this.mLongitude = in.readDouble();

    }

    @Override
    public String toString() {
        return "Course{" +
                "mName='" + mName + '\'' +
                ", mCity='" + mCity + '\'' +
                ", mState='" + mState + '\'' +
                ", mNumHoles=" + mNumHoles +
                ", mCourseId=" + mCourseId +
                ", mDgcrMobileLink='" + mDgcrMobileLink + '\'' +
                ", mReviewCount=" + mReviewCount +
                ", mDgcrRating=" + mDgcrRating +
                ", mLatitude=" + mLatitude +
                ", mLongitude=" + mLongitude +
                '}';
    }


    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmCity() {
        return mCity;
    }

    public void setmCity(String mCity) {
        this.mCity = mCity;
    }

    public String getmState() {
        return mState;
    }

    public void setmState(String mState) {
        this.mState = mState;
    }

    public int getmNumHoles() {
        return mNumHoles;
    }

    public void setmNumHoles(int mNumHoles) {
        this.mNumHoles = mNumHoles;
    }

    public int getmCourseId() {
        return mCourseId;
    }

    public void setmCourseId(int mCourseId) {
        this.mCourseId = mCourseId;
    }

    public String getmDgcrMobileLink() {
        return mDgcrMobileLink;
    }

    public void setmDgcrMobileLink(String mDgcrMobileLink) {
        this.mDgcrMobileLink = mDgcrMobileLink;
    }

    public int getmReviewCount() {
        return mReviewCount;
    }

    public void setmReviewCount(int mReviewCount) {
        this.mReviewCount = mReviewCount;
    }

    public double getmDgcrRating() {
        return mDgcrRating;
    }

    public void setmDgcrRating(double mDgcrRating) {
        this.mDgcrRating = mDgcrRating;
    }

    public double getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public double getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(double mLongitude) {
        this.mLongitude = mLongitude;
    }


}
