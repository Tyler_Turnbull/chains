package com.tsquaredapplications.chains2.RoomDB;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.tsquaredapplications.chains2.Objects.Scorecard;

/**
 * Created by tylerturnbull on 3/29/18.
 */
@Dao
public interface SavedScorecardDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertScorecard(Scorecard... scorecards);

    @Delete
    public void deleteScorecards(Scorecard... scorecards);

    @Query("SELECT * FROM saved_scorecards")
    public Scorecard[] loadAllScorecards();

    @Query("SELECT * FROM saved_scorecards WHERE courseId == :courseId")
    public Scorecard findScorecard(int courseId);

    @Query("DELETE FROM saved_scorecards")
    public void deleteAllScorecards();

    @Query("DELETE FROM saved_scorecards WHERE courseId == :courseId")
    public void removeScorecard(int courseId);
}
