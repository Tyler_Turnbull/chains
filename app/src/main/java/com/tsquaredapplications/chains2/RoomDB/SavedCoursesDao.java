package com.tsquaredapplications.chains2.RoomDB;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.tsquaredapplications.chains2.Objects.Course;

/**
 * Created by tylerturnbull on 1/18/18.
 */

@Dao
public interface SavedCoursesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertCourses(Course... courses);

    @Delete
    public void deleteCourses(Course... courses);

    @Query("SELECT * FROM saved_courses")
    public Course[] loadAllCourses();

    @Query("DELETE FROM saved_courses")
    public void deleteAllCourses();

    @Query("SELECT * FROM saved_courses WHERE courseId == :courseId")
    public Course loadCourse(int courseId);

}