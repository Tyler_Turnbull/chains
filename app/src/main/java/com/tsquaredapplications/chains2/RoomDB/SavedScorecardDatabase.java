package com.tsquaredapplications.chains2.RoomDB;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.tsquaredapplications.chains2.Objects.Scorecard;

/**
 * Created by tylerturnbull on 3/29/18.
 */

@Database(entities = {Scorecard.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class SavedScorecardDatabase extends RoomDatabase {
    public abstract SavedScorecardDao savedScorecardDao();
}
