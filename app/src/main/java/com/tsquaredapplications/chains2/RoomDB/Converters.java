package com.tsquaredapplications.chains2.RoomDB;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tsquaredapplications.chains2.Objects.Hole;
import com.tsquaredapplications.chains2.PlayerStatsObjects.HoleStats;
import com.tsquaredapplications.chains2.PlayerStatsObjects.RoundStats;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tylerturnbull on 1/18/18.
 */

public class Converters {

    @TypeConverter
    public static ArrayList<Hole> holeArrayListFromString(String value){
        Type listType = new TypeToken<ArrayList<Hole>>(){}.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String fromHoleArrayList(ArrayList<Hole> list){
        return new Gson().toJson(list);

    }

    @TypeConverter
    public static List<Hole> holeListFromString(String value){
        Type listType = new TypeToken<ArrayList<Hole>>(){}.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String fromHoleList(List<Hole> list){
        return new Gson().toJson(list);

    }

    @TypeConverter
    public static ArrayList<RoundStats> roundStatsArrayListFromString(String value){
        Type listType = new TypeToken<ArrayList<RoundStats>>(){}.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String fromRoundStatsArrayList(ArrayList<RoundStats> list){
        return new Gson().toJson(list);
    }

    @TypeConverter
    public static ArrayList<HoleStats> holeStatsArrayListFromString(String value){
        Type listType = new TypeToken<ArrayList<HoleStats>>(){}.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String fromHoleStatsArrayList(ArrayList<HoleStats> list){
        return new Gson().toJson(list);
    }

    @TypeConverter
    public static ArrayList<Integer> integerArrayListFromString (String value){
        Type listType = new TypeToken<ArrayList<Integer>>(){}.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String fromIntegerArrayList(ArrayList<Integer> list){
        return new Gson().toJson(list);
    }

    @TypeConverter
    public static int[] intArrayFromString(String value){
        Type listType = new TypeToken<int[]>(){}.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String fromIntArray(int[] list){
        return new Gson().toJson(list);
    }



}