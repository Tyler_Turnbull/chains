package com.tsquaredapplications.chains2.RoomDB;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerTeeStats;

/**
 * Created by tylerturnbull on 2/8/18.
 */

@Dao
public interface PlayerTeeStatsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertCourseStats(PlayerTeeStats... playerTeeStats);

    @Delete
    public void deleteCourseStats(PlayerTeeStats... playerTeeStats);

    @Update
    public void updateCourseStats(PlayerTeeStats... playerTeeStats);


    @Query("SELECT * FROM user_course_tee_stats WHERE courseId == :courseId AND teeNumber == :teeNumber")
    public PlayerTeeStats findCourseStats(int courseId, int teeNumber);

   @Query("SELECT * FROM user_course_tee_stats")
    public PlayerTeeStats[] loadAllPlayerTeeStats();

   @Query("DELETE FROM user_course_tee_stats")
    public void deleteAllPlayerTeeStats();
}
