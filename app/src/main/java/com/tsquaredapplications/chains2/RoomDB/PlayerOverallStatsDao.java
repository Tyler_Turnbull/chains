package com.tsquaredapplications.chains2.RoomDB;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerStats;

/**
 * Created by Tyler on 2/25/2018.
 */

@Dao
public interface PlayerOverallStatsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertPlayerStats(PlayerStats... stats);

    @Delete
    public void deletePlayerStats(PlayerStats... stats);

    @Update
    public void updatePlayerStats(PlayerStats... stats);

    @Query("SELECT * FROM user_overall_stats")
    public PlayerStats[] loadPlayerStats();

    @Query("DELETE FROM user_overall_stats")
    public void deleteAllOverallStats();
}
