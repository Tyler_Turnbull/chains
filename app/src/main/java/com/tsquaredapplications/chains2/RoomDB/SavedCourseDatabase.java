package com.tsquaredapplications.chains2.RoomDB;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.tsquaredapplications.chains2.Objects.Course;

/**
 * Created by tylerturnbull on 1/18/18.
 */

@Database(entities = {Course.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class SavedCourseDatabase extends RoomDatabase {
    public abstract SavedCoursesDao savedCourseDao();
}