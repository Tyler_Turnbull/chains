package com.tsquaredapplications.chains2.RoomDB;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerTeeStats;

/**
 * Created by tylerturnbull on 2/8/18.
 */

@Database(entities = {PlayerTeeStats.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class PlayerTeeStatsDatabase extends RoomDatabase {
    public abstract PlayerTeeStatsDao playerTeeStatsDao();
}
