package com.tsquaredapplications.chains2.RoomDB;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.tsquaredapplications.chains2.PlayerStatsObjects.PlayerStats;

/**
 * Created by Tyler on 2/25/2018.
 */

@Database(entities = {PlayerStats.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class PlayerOverallStatsDatabase extends RoomDatabase{
    public abstract PlayerOverallStatsDao playerOverallStatsDao();
}
