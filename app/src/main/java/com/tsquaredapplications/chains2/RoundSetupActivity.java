package com.tsquaredapplications.chains2;


import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.tsquaredapplications.chains2.Enums.GameMode;
import com.tsquaredapplications.chains2.Enums.IntentExtraNames;
import com.tsquaredapplications.chains2.Enums.RoomDbNames;
import com.tsquaredapplications.chains2.Fragments.GameModeSelectionFragment;
import com.tsquaredapplications.chains2.Fragments.TeeSelectionFragment;
import com.tsquaredapplications.chains2.Objects.Course;
import com.tsquaredapplications.chains2.Objects.Scorecard;
import com.tsquaredapplications.chains2.RoomDB.SavedCourseDatabase;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RoundSetupActivity extends AppCompatActivity
                        implements TeeSelectionFragment.onTeeSelectedListener,
        GameModeSelectionFragment.OnGameModeSelectedListener{

    private static final String LOG_TAG = RoundSetupActivity.class.getName();

    private Course mCourse;
    private int mTeeChoice;
    private Fragment mCurrentFragment;
    private Context mContext;

    @BindView(R.id.round_setup_frame_layout)
    FrameLayout contentFrameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_round_setup);

        ButterKnife.bind(this);

        mContext = getApplicationContext();
        Intent incomingIntent = getIntent();
        if(incomingIntent != null){
            mCourse = incomingIntent.getParcelableExtra(IntentExtraNames.COURSE.toString());
        } else {
            // If course did not come through, can not setup round.
            finish();
        }

        SaveCourseTask task = new SaveCourseTask(this);
        task.execute(mCourse);

        // Ready Bundle for teeSelectionFragment
        Bundle bundle = new Bundle();
        bundle.putParcelable(IntentExtraNames.COURSE.toString(), mCourse);

        // Start tee selection fragment
        mCurrentFragment = new TeeSelectionFragment();
        mCurrentFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.round_setup_frame_layout, mCurrentFragment);
        transaction.commit();



    }

    @Override
    public void onTeeSelected(int teeChoice) {
        // Tee chosen, now prompt for game style ( single / multi )
        mTeeChoice = teeChoice;
        mCurrentFragment = new GameModeSelectionFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.round_setup_frame_layout, mCurrentFragment);
        transaction.commit();

    }

    @Override
    public void onModeSelected(GameMode mode) {



       switch(mode){
           case Single: {
               Intent intent = new Intent(this, SpScoringActivity.class);
               Scorecard scorecard = new Scorecard(mCourse, mTeeChoice);
               intent.putExtra(IntentExtraNames.SCORECARD.toString(), new Scorecard(mCourse, mTeeChoice));
               startActivity(intent);
               break;
           }
           case Multi: {

           }


       }
    }


    private static class SaveCourseTask extends AsyncTask<Course, Void, Boolean> {

        private WeakReference<RoundSetupActivity> activityReference;


        SaveCourseTask(RoundSetupActivity context) {
            activityReference = new WeakReference<RoundSetupActivity>(context);

        }

        @Override
        protected Boolean doInBackground(Course... courses) {

            SavedCourseDatabase db = Room.databaseBuilder(activityReference.get(),
                    SavedCourseDatabase.class, RoomDbNames.RECENT_COURSES.toString()).build();

            Course[] courseList = db.savedCourseDao().loadAllCourses();
            ArrayList<Course> dbCourseList = new ArrayList<Course>(Arrays.asList(courseList));

            if (dbCourseList.size() < 10) {
                if (!dbCourseList.contains(courses[0])) {
                    dbCourseList.add(0, courses[0]);
                } else {
                    dbCourseList.remove(dbCourseList.indexOf(courses[0]));
                    dbCourseList.add(0, courses[0]);
                }
            } else {
                if (!dbCourseList.contains(courses[0])) {
                    dbCourseList.remove(dbCourseList.size() - 1);
                    dbCourseList.add(0, courses[0]);
                } else {
                    dbCourseList.remove(dbCourseList.indexOf(courses[0]));
                    dbCourseList.add(courses[0]);
                }
            }

            Course[] outputList = new Course[dbCourseList.size()];
            for (int i = 0; i < dbCourseList.size(); i++) {
                outputList[i] = dbCourseList.get(i);
            }
            db.savedCourseDao().deleteAllCourses();

            db.savedCourseDao().insertCourses(outputList);
            db.close();
            return true;
        }

    }
}
